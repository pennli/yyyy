package com.sandking.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @UserName : SandKing
 * @DataTime : 2013年11月27日 下午12:03:31
 * @Description ：Please describe this document
 */
public class TestMain {
	static final List<String> saveDelete = new CopyOnWriteArrayList<String>();
	public static void main(String[] args) throws Exception {
		saveDelete.add("1");
		saveDelete.add("2");
		saveDelete.add("3");
		saveDelete.add("4");
		saveDelete.add("5");
		saveDelete.add("6");
		
		List list = new ArrayList<>();
		list.add("10");
		list.add("6");
		System.out.println(saveDelete.removeAll(list));
		System.out.println(saveDelete.remove("1"));
		System.out.println(saveDelete.remove("5"));
		
		
	}
}
