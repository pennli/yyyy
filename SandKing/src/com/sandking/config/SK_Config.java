package com.sandking.config;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import redis.clients.jedis.JedisPool;

import com.alibaba.druid.pool.DruidDataSource;

/**
 * @UserName : SandKing
 * @DataTime : 2013年11月30日 下午5:09:04
 * @Description ：服务器配置
 */
public class SK_Config {
	static DruidDataSource ds;
	public static final String URL = "jdbc:mysql://127.0.0.1:3306/hoc?autoReconnect=true&characterEncoding=utf-8";
	public static final String USERNAME = "root";
	public static final String PASSWORD = "";
	public static final String DRIVER = "com.mysql.jdbc.Driver";

	public static DataSource getDataSource() {
		if (ds == null) {
			ds = new DruidDataSource();
			ds.setUrl(URL);
			ds.setUsername(USERNAME);
			ds.setPassword(PASSWORD);
			ds.setDriverClassName(DRIVER);
		}
		return ds;
	}

	public static JedisPool getJedisPool() {
		return null;
	}

	public static SK_ServerCfg getServerConfig() {
		return null;
	}

	public static Connection getConnection() {
		if (ds != null) {
			try {
				return ds.getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			getDataSource();
			try {
				return ds.getConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
