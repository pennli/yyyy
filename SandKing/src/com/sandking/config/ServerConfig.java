package com.sandking.config;

import javax.sql.DataSource;

import redis.clients.jedis.JedisPool;

public interface ServerConfig {
	/**
	 * 逻辑库数据库连接池
	 * 
	 * @return
	 */
	public DataSource getLogicDataSource();

	/**
	 * 设计库数据库连接池
	 * 
	 * @return
	 */
	public DataSource getDesignDataSource();

	/**
	 * 日志库数据库连接池
	 * 
	 * @return
	 */
	public DataSource getLogDataSource();

	/**
	 * 配置库数据库连接池
	 * 
	 * @return
	 */
	public DataSource getConfigDataSource();

	/**
	 * Jedis数据库连接池
	 * 
	 * @return
	 */
	public JedisPool getJedisPool();
}
