package com.sandking.net.imp;
/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午4:55:24
 * @Description ：网络传输对象
 */
public class NetObject {
	@NetAnnotation(remark = "返回状态")
	public class ReturnStatus {
		@NetAnnotation(remark = "编号")
		int code;
		@NetAnnotation(remark = "消息")
		String msg;
	}
	
	@NetAnnotation(remark = "int类型")
	public class N_Int {
		@NetAnnotation(remark = "值")
		int val;
	}
	
	@NetAnnotation(remark = "string类型")
	public class N_Str {
		@NetAnnotation(remark = "值")
		String val;
	}
	
	@NetAnnotation(remark = "Bool类型")
	public class N_Bool {
		@NetAnnotation(remark = "值")
		boolean val;
	}
}
