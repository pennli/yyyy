package com.sandking.net.test;

import com.sandking.net.NetConfig;
import com.sandking.net.Net_Generate;
import com.sandking.tools.SK_Plus;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午5:09:03
 * @Description ：Please describe this document
 */
public class NetTest {
	public static void main(String[] args) throws Exception {
		NetConfig netCfg = new NetConfig();
		netCfg.setFilePath("src/com/sandking/sgNet");
		netCfg.setClientImpPath(SK_Plus.b(netCfg.getFilePath(),"/clientImp").e());
		netCfg.setServerImpPath(SK_Plus.b(netCfg.getFilePath(),"/serverImp").e());
		netCfg.setNetObjectPath(SK_Plus.b(netCfg.getFilePath(),"/netObject").e());
		netCfg.setConstantPath(SK_Plus.b(netCfg.getFilePath(),"/constant").e());
		netCfg.setServerImpName("SgServerNetImp");
		netCfg.setClientImpName("SgClientNetImp");
		netCfg.setClientImp(SgClientImp.class);
		netCfg.setServerImp(SgServerImp.class);
		netCfg.setConstant(SgConstant.class);
		netCfg.setNetObject(SgNetObject.class);
		netCfg.setConfiguration(Net_Generate.getConfiguration());
		Net_Generate.run(netCfg);
	}
}
