package com.sandking.net.test;

import com.sandking.net.imp.Constant;
import com.sandking.net.imp.NetAnnotation;
import com.sandking.net.pojo.NetType;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午5:07:59
 * @Description ：Please describe this document
 */
public class SgConstant extends Constant {

	@NetAnnotation(remark = "用户类型")
	class UserType {

		@NetAnnotation(remark = "客服")
		public static final int GM = 1111111;

		@NetAnnotation(remark = "系统")
		public static final String SYS = "ASD";
	}
	
	@NetAnnotation(remark = "用户类型")
	class User {

		@NetAnnotation(remark = "客服")
		public static final int GM = 1111111;

		@NetAnnotation(remark = "系统")
		public static final String SYS = "ASD";
	}
}
