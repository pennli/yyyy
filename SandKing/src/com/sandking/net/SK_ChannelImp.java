package com.sandking.net;

import java.util.Map;

/**
 * @UserName : SandKing
 * @DataTime : 2014年6月26日 上午10:15:40
 * @Description ：Please describe this document
 */
public interface SK_ChannelImp {
	
	public void send(Map<Object, Object> map) throws Exception;

	public void send(byte[] buf) throws Exception;
}
