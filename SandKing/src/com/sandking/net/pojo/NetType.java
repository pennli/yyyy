package com.sandking.net.pojo;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午5:03:26
 * @Description ：Please describe this document
 */
public class NetType {
	public static final int Server = 1;
	public static final int Client = 2;
	public static final int NetObject = 3;
	public static final int ConstantObject = 4;
}
