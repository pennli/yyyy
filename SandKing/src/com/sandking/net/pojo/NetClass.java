package com.sandking.net.pojo;

import java.util.List;
import java.util.Set;

/**
 * @UserName : SandKing
 * @DataTime : 2014年5月29日 下午4:58:48
 * @Description ：Please describe this document
 */
public class NetClass {
	private String packageName;
	private String className;
	private String dclassName;
	private String xclassName;
	private String remark;
	private List<NetMethod> methods;
	private List<NetField> fields;
	private Set<String> serverImports;
	private Set<String> clientImports;
	private Set<String> netObjImports;
	private Set<String> constantImports;

	public NetClass(String packageName, String className, String dclassName,
			String xclassName, String remark, List<NetMethod> methods,
			List<NetField> fields, Set<String> serverImports,
			Set<String> clientImports, Set<String> netObjImports,
			Set<String> constantImports) {
		super();
		this.packageName = packageName;
		this.className = className;
		this.dclassName = dclassName;
		this.xclassName = xclassName;
		this.remark = remark;
		this.methods = methods;
		this.fields = fields;
		this.serverImports = serverImports;
		this.clientImports = clientImports;
		this.netObjImports = netObjImports;
		this.constantImports = constantImports;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getDclassName() {
		return dclassName;
	}

	public void setDclassName(String dclassName) {
		this.dclassName = dclassName;
	}

	public String getXclassName() {
		return xclassName;
	}

	public void setXclassName(String xclassName) {
		this.xclassName = xclassName;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public List<NetMethod> getMethods() {
		return methods;
	}

	public void setMethods(List<NetMethod> methods) {
		this.methods = methods;
	}

	public List<NetField> getFields() {
		return fields;
	}

	public void setFields(List<NetField> fields) {
		this.fields = fields;
	}

	public Set<String> getServerImports() {
		return serverImports;
	}

	public void setServerImports(Set<String> serverImports) {
		this.serverImports = serverImports;
	}

	public Set<String> getClientImports() {
		return clientImports;
	}

	public void setClientImports(Set<String> clientImports) {
		this.clientImports = clientImports;
	}

	public Set<String> getNetObjImports() {
		return netObjImports;
	}

	public void setNetObjImports(Set<String> netObjImports) {
		this.netObjImports = netObjImports;
	}

	public Set<String> getConstantImports() {
		return constantImports;
	}

	public void setConstantImports(Set<String> constantImports) {
		this.constantImports = constantImports;
	}

}
