package com.sandking.sgNet.netObject;

import java.util.Map;
import java.util.HashMap;
 

/**
 * int类型
 */
public class N_Int{
	/** 值 */
	private int val;
	
	public int getVal(){
		return val;
	}
	
	public void setVal(int val){
		this.val = val;
	}
	
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("val",val);
        return result;
    }
}