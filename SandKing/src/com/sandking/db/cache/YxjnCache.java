package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.db.bean.Yxjn;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.jedis.YxjnJedis;
import java.util.Set;
import com.sandking.db.dao.YxjnDao;
/**
 * 英雄技能
 */
public class YxjnCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YxjnCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yxjn> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yhyx_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yxjn> saveDelete = new CopyOnWriteArrayList<Yxjn>();
	
	static final List<Yxjn> saveInsert = new CopyOnWriteArrayList<Yxjn>();
	
	static final List<Yxjn> saveUpdate = new CopyOnWriteArrayList<Yxjn>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yxjn getById(int id){
		Yxjn yxjn = null;
		String key = SK_Plus.b(id).e();
		yxjn = idCache.get(key);
		
		if(yxjn==null){
			//查询数据库
			yxjn = YxjnJedis.getById(id);
			if(yxjn!=null){
				//加入缓存
				loadCache(yxjn);
			}
		}
		return yxjn;
	}
	
	/**
	 * 根据( 用户英雄_id ) 查询
	 */
	public static List<Yxjn> getByYhyx_id(int yhyx_id){
		List<Yxjn> yxjns = new ArrayList<Yxjn>();
		String key = SK_Plus.b(yhyx_id).e();
		Set<String> keys = yhyx_idCache.get(key);
		if(keys != null){
			Yxjn yxjn = null;
			for (String k : keys) {
				yxjn = getById(Integer.valueOf(k));
				if (yxjn == null) continue;
					yxjns.add(yxjn);
			}
		}else{
			yxjns = YxjnJedis.getByYhyx_id(yhyx_id);
			if(!yxjns.isEmpty()){
				loadCaches(yxjns);
			}
		}
		return yxjns;
	}
	
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,int page,int size,Integer pageCount){
		List<Yxjn> yxjns = getByYhyx_id(yhyx_id);
		yxjns = SK_List.getPage(yxjns, page, size, pageCount);
		return yxjns;
	}
	
	public static List<Yxjn> getCacheAll(){
		return new ArrayList<Yxjn>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yxjn> getAll(){
		List<Yxjn> yxjns = new ArrayList<Yxjn>();
		if(!isLoadAll){
			yxjns = YxjnJedis.getAll();
			loadCaches(yxjns);
			isLoadAll = true;
		}else{
			yxjns = new ArrayList<Yxjn>(idCache.values());
		}
		return yxjns;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yxjn> getAllByPage(int page,int size,Integer pageCount){
		List<Yxjn> yxjns = getAll();
		yxjns = SK_List.getPage(yxjns, page, size, pageCount);
		return yxjns;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yxjn> yxjns){
		for(Yxjn yxjn : yxjns){
			loadCache(yxjn);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yxjn yxjn){
		idCache.put(SK_Plus.b(yxjn.getId()).e(),yxjn);
		Set<String> yhyx_idset = yhyx_idCache.get(String.valueOf(SK_Plus.b(yxjn.getYhyx_id()).e()));
		if(yhyx_idset == null){
			yhyx_idset = new HashSet<String>();
		}
		yhyx_idset.add(String.valueOf(yxjn.getId()));
		yhyx_idCache.put(SK_Plus.b(yxjn.getYhyx_id()).e(),yhyx_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yxjn yxjn,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yxjn.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yxjn.getId()).e());
		}
		Set<String> yhyx_idset = yhyx_idCache.get(String.valueOf(SK_Plus.b(yxjn.getYhyx_id()).e()));
		if(yhyx_idset == null){
			yhyx_idset = new HashSet<String>();
		}
		if (yhyx_idset.contains(String.valueOf(yxjn.getId()))) {
			yhyx_idset.remove(String.valueOf(yxjn.getId()));
		}
		yhyx_idCache.put(SK_Plus.b(yxjn.getYhyx_id()).e(),yhyx_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yxjn);
			break;
		case 2:
			saveRedis(yxjn);
			break;
		case 3:
			saveAll(yxjn);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yxjn> yxjns,int clearType,int saveType){
		for(Yxjn yxjn : yxjns){
			clearCache(yxjn,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yxjns);
			break;
		case 2:
			saveRedis(yxjns);
			break;
		case 3:
			saveAll(yxjns);
			break;
		default:
			break;
		}
	}
	
	public static Yxjn insert(Yxjn yxjn){
		return insert(yxjn,false);
    }
    
    public static Yxjn update(Yxjn yxjn){
    	return update(yxjn,false);
    }
    
    public static boolean delete(Yxjn yxjn){
    	return delete(yxjn,false);
    }
    
    private static Yxjn insert(Yxjn yxjn,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yxjn = YxjnJedis.insert(yxjn);
    		LASTID.set(yxjn.getId());
    	}else{
    		int _id = yxjn.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yxjn.getId() > id) {
				LASTID.set(yxjn.getId());
			}
    		//加入定时器
    	}
    	loadCache(yxjn);
    	if(!isFlush){
	    	if (!saveInsert.contains(yxjn)) {
				saveInsert.add(yxjn);
			}
		}
    	return yxjn;
    }
    
    private static Yxjn update(Yxjn yxjn,boolean isFlush){
    	clearCache(yxjn,1,0);
    	loadCache(yxjn);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yxjn)) {
				saveUpdate.add(yxjn);
			}
		}else{
			saveUpdate.remove(yxjn);
		}
    	return yxjn;
    }
    
    private static boolean delete(Yxjn yxjn,boolean isFlush){
    	clearCache(yxjn,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yxjn)) {
				saveUpdate.remove(yxjn);
				saveInsert.remove(yxjn);
				saveDelete.add(yxjn);
			}
    	}else{
    		saveUpdate.remove(yxjn);
			saveInsert.remove(yxjn);
			saveDelete.remove(yxjn);
    	}
    	return false;
    }
    
    public static Yxjn updateAndFlush(Yxjn yxjn){
    	update(yxjn,true);
    	return YxjnJedis.update(yxjn);
    }
    
    public static Yxjn insertAndFlush(Yxjn yxjn){
    	int id = LASTID.get();
    	insert(yxjn,true);
    	if(id > 0){
    		yxjn = YxjnJedis.insert(yxjn);
    	}
    	return yxjn;
    }
    
    public static boolean deleteAndFlush(Yxjn yxjn){
    	delete(yxjn,true);
    	return YxjnJedis.delete(yxjn);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YxjnDao.deleteBatch(saveDelete);
    	YxjnDao.insertBatch(saveInsert);
    	YxjnDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yxjn yxjn){
    	if (saveDelete.remove(yxjn))
			YxjnDao.delete(yxjn);
		if (saveInsert.remove(yxjn))
			YxjnDao.insert(yxjn);
		if (saveUpdate.remove(yxjn))
			YxjnDao.update(yxjn);
    }
    
    public static void saveDatabase(List<Yxjn> yxjns){
    	if (saveDelete.removeAll(yxjns))
			YxjnDao.deleteBatch(yxjns);
		if (saveInsert.removeAll(yxjns))
			YxjnDao.insertBatch(yxjns);
		if (saveUpdate.removeAll(yxjns))
			YxjnDao.updateBatch(yxjns);
    }
    
    public static void saveRedis(){
    	YxjnJedis.clearCaches(saveDelete);
    	YxjnJedis.loadCaches(saveInsert);
    	YxjnJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yxjn yxjn){
    	if (saveDelete.remove(yxjn))
			YxjnJedis.clearCache(yxjn);
		if (saveInsert.remove(yxjn))
			YxjnJedis.loadCache(yxjn);
		if (saveUpdate.remove(yxjn))
			YxjnJedis.loadCache(yxjn);
    }
    
    public static void saveRedis(List<Yxjn> yxjns){
    	if (saveDelete.removeAll(yxjns))
			YxjnJedis.clearCaches(yxjns);
		if (saveInsert.removeAll(yxjns))
			YxjnJedis.loadCaches(yxjns);
		if (saveUpdate.removeAll(yxjns))
			YxjnJedis.loadCaches(yxjns);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yxjn yxjn){
   		saveDatabase(yxjn);
   		saveRedis(yxjn);
    }
    
    public static void saveAll(List<Yxjn> yxjns){
   		saveDatabase(yxjns);
   		saveRedis(yxjns);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yxjn yxjn){
	
		loadCache(yxjn);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yxjn> yxjns){
		for(Yxjn yxjn : yxjns){
			loadCacheCascade(yxjn);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yxjn yxjn,int saveType){
	
		clearCache(yxjn,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yxjn> yxjns,int saveType){
		for(Yxjn yxjn : yxjns){
			clearCacheCascade(yxjn,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yxjns);
			break;
		case 2:
			saveRedis(yxjns);
			break;
		case 3:
			saveAll(yxjns);
			break;
		default:
			break;
		}
	}
}