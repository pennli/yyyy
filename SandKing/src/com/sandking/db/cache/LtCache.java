package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.jedis.LtJedis;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import com.sandking.db.bean.Lt;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.dao.LtDao;
import java.util.Set;
/**
 * 聊天
 */
public class LtCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LtCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Lt> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> ltlx_idCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> lm_idCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> jsridCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> fyridCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Lt> saveDelete = new CopyOnWriteArrayList<Lt>();
	
	static final List<Lt> saveInsert = new CopyOnWriteArrayList<Lt>();
	
	static final List<Lt> saveUpdate = new CopyOnWriteArrayList<Lt>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Lt getById(int id){
		Lt lt = null;
		String key = SK_Plus.b(id).e();
		lt = idCache.get(key);
		
		if(lt==null){
			//查询数据库
			lt = LtJedis.getById(id);
			if(lt!=null){
				//加入缓存
				loadCache(lt);
			}
		}
		return lt;
	}
	
	/**
	 * 根据( 聊天类型_id ) 查询
	 */
	public static List<Lt> getByLtlx_id(int ltlx_id){
		List<Lt> lts = new ArrayList<Lt>();
		String key = SK_Plus.b(ltlx_id).e();
		Set<String> keys = ltlx_idCache.get(key);
		if(keys != null){
			Lt lt = null;
			for (String k : keys) {
				lt = getById(Integer.valueOf(k));
				if (lt == null) continue;
					lts.add(lt);
			}
		}else{
			lts = LtJedis.getByLtlx_id(ltlx_id);
			if(!lts.isEmpty()){
				loadCaches(lts);
			}
		}
		return lts;
	}
	
	public static List<Lt> getByPageLtlx_id(int ltlx_id,int page,int size,Integer pageCount){
		List<Lt> lts = getByLtlx_id(ltlx_id);
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Lt> getByLm_id(int lm_id){
		List<Lt> lts = new ArrayList<Lt>();
		String key = SK_Plus.b(lm_id).e();
		Set<String> keys = lm_idCache.get(key);
		if(keys != null){
			Lt lt = null;
			for (String k : keys) {
				lt = getById(Integer.valueOf(k));
				if (lt == null) continue;
					lts.add(lt);
			}
		}else{
			lts = LtJedis.getByLm_id(lm_id);
			if(!lts.isEmpty()){
				loadCaches(lts);
			}
		}
		return lts;
	}
	
	public static List<Lt> getByPageLm_id(int lm_id,int page,int size,Integer pageCount){
		List<Lt> lts = getByLm_id(lm_id);
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Lt> getByJsrid(int jsrid){
		List<Lt> lts = new ArrayList<Lt>();
		String key = SK_Plus.b(jsrid).e();
		Set<String> keys = jsridCache.get(key);
		if(keys != null){
			Lt lt = null;
			for (String k : keys) {
				lt = getById(Integer.valueOf(k));
				if (lt == null) continue;
					lts.add(lt);
			}
		}else{
			lts = LtJedis.getByJsrid(jsrid);
			if(!lts.isEmpty()){
				loadCaches(lts);
			}
		}
		return lts;
	}
	
	public static List<Lt> getByPageJsrid(int jsrid,int page,int size,Integer pageCount){
		List<Lt> lts = getByJsrid(jsrid);
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	/**
	 * 根据( 发言人id ) 查询
	 */
	public static List<Lt> getByFyrid(int fyrid){
		List<Lt> lts = new ArrayList<Lt>();
		String key = SK_Plus.b(fyrid).e();
		Set<String> keys = fyridCache.get(key);
		if(keys != null){
			Lt lt = null;
			for (String k : keys) {
				lt = getById(Integer.valueOf(k));
				if (lt == null) continue;
					lts.add(lt);
			}
		}else{
			lts = LtJedis.getByFyrid(fyrid);
			if(!lts.isEmpty()){
				loadCaches(lts);
			}
		}
		return lts;
	}
	
	public static List<Lt> getByPageFyrid(int fyrid,int page,int size,Integer pageCount){
		List<Lt> lts = getByFyrid(fyrid);
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	
	public static List<Lt> getCacheAll(){
		return new ArrayList<Lt>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Lt> getAll(){
		List<Lt> lts = new ArrayList<Lt>();
		if(!isLoadAll){
			lts = LtJedis.getAll();
			loadCaches(lts);
			isLoadAll = true;
		}else{
			lts = new ArrayList<Lt>(idCache.values());
		}
		return lts;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Lt> getAllByPage(int page,int size,Integer pageCount){
		List<Lt> lts = getAll();
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Lt> lts){
		for(Lt lt : lts){
			loadCache(lt);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Lt lt){
		idCache.put(SK_Plus.b(lt.getId()).e(),lt);
		Set<String> ltlx_idset = ltlx_idCache.get(String.valueOf(SK_Plus.b(lt.getLtlx_id()).e()));
		if(ltlx_idset == null){
			ltlx_idset = new HashSet<String>();
		}
		ltlx_idset.add(String.valueOf(lt.getId()));
		ltlx_idCache.put(SK_Plus.b(lt.getLtlx_id()).e(),ltlx_idset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(lt.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		lm_idset.add(String.valueOf(lt.getId()));
		lm_idCache.put(SK_Plus.b(lt.getLm_id()).e(),lm_idset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(lt.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		jsridset.add(String.valueOf(lt.getId()));
		jsridCache.put(SK_Plus.b(lt.getJsrid()).e(),jsridset);
		Set<String> fyridset = fyridCache.get(String.valueOf(SK_Plus.b(lt.getFyrid()).e()));
		if(fyridset == null){
			fyridset = new HashSet<String>();
		}
		fyridset.add(String.valueOf(lt.getId()));
		fyridCache.put(SK_Plus.b(lt.getFyrid()).e(),fyridset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Lt lt,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(lt.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(lt.getId()).e());
		}
		Set<String> ltlx_idset = ltlx_idCache.get(String.valueOf(SK_Plus.b(lt.getLtlx_id()).e()));
		if(ltlx_idset == null){
			ltlx_idset = new HashSet<String>();
		}
		if (ltlx_idset.contains(String.valueOf(lt.getId()))) {
			ltlx_idset.remove(String.valueOf(lt.getId()));
		}
		ltlx_idCache.put(SK_Plus.b(lt.getLtlx_id()).e(),ltlx_idset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(lt.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		if (lm_idset.contains(String.valueOf(lt.getId()))) {
			lm_idset.remove(String.valueOf(lt.getId()));
		}
		lm_idCache.put(SK_Plus.b(lt.getLm_id()).e(),lm_idset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(lt.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		if (jsridset.contains(String.valueOf(lt.getId()))) {
			jsridset.remove(String.valueOf(lt.getId()));
		}
		jsridCache.put(SK_Plus.b(lt.getJsrid()).e(),jsridset);
		Set<String> fyridset = fyridCache.get(String.valueOf(SK_Plus.b(lt.getFyrid()).e()));
		if(fyridset == null){
			fyridset = new HashSet<String>();
		}
		if (fyridset.contains(String.valueOf(lt.getId()))) {
			fyridset.remove(String.valueOf(lt.getId()));
		}
		fyridCache.put(SK_Plus.b(lt.getFyrid()).e(),fyridset);
		switch (saveType) {
		case 1:
			saveDatabase(lt);
			break;
		case 2:
			saveRedis(lt);
			break;
		case 3:
			saveAll(lt);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Lt> lts,int clearType,int saveType){
		for(Lt lt : lts){
			clearCache(lt,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(lts);
			break;
		case 2:
			saveRedis(lts);
			break;
		case 3:
			saveAll(lts);
			break;
		default:
			break;
		}
	}
	
	public static Lt insert(Lt lt){
		return insert(lt,false);
    }
    
    public static Lt update(Lt lt){
    	return update(lt,false);
    }
    
    public static boolean delete(Lt lt){
    	return delete(lt,false);
    }
    
    private static Lt insert(Lt lt,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		lt = LtJedis.insert(lt);
    		LASTID.set(lt.getId());
    	}else{
    		int _id = lt.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (lt.getId() > id) {
				LASTID.set(lt.getId());
			}
    		//加入定时器
    	}
    	loadCache(lt);
    	if(!isFlush){
	    	if (!saveInsert.contains(lt)) {
				saveInsert.add(lt);
			}
		}
    	return lt;
    }
    
    private static Lt update(Lt lt,boolean isFlush){
    	clearCache(lt,1,0);
    	loadCache(lt);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(lt)) {
				saveUpdate.add(lt);
			}
		}else{
			saveUpdate.remove(lt);
		}
    	return lt;
    }
    
    private static boolean delete(Lt lt,boolean isFlush){
    	clearCache(lt,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(lt)) {
				saveUpdate.remove(lt);
				saveInsert.remove(lt);
				saveDelete.add(lt);
			}
    	}else{
    		saveUpdate.remove(lt);
			saveInsert.remove(lt);
			saveDelete.remove(lt);
    	}
    	return false;
    }
    
    public static Lt updateAndFlush(Lt lt){
    	update(lt,true);
    	return LtJedis.update(lt);
    }
    
    public static Lt insertAndFlush(Lt lt){
    	int id = LASTID.get();
    	insert(lt,true);
    	if(id > 0){
    		lt = LtJedis.insert(lt);
    	}
    	return lt;
    }
    
    public static boolean deleteAndFlush(Lt lt){
    	delete(lt,true);
    	return LtJedis.delete(lt);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	LtDao.deleteBatch(saveDelete);
    	LtDao.insertBatch(saveInsert);
    	LtDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Lt lt){
    	if (saveDelete.remove(lt))
			LtDao.delete(lt);
		if (saveInsert.remove(lt))
			LtDao.insert(lt);
		if (saveUpdate.remove(lt))
			LtDao.update(lt);
    }
    
    public static void saveDatabase(List<Lt> lts){
    	if (saveDelete.removeAll(lts))
			LtDao.deleteBatch(lts);
		if (saveInsert.removeAll(lts))
			LtDao.insertBatch(lts);
		if (saveUpdate.removeAll(lts))
			LtDao.updateBatch(lts);
    }
    
    public static void saveRedis(){
    	LtJedis.clearCaches(saveDelete);
    	LtJedis.loadCaches(saveInsert);
    	LtJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Lt lt){
    	if (saveDelete.remove(lt))
			LtJedis.clearCache(lt);
		if (saveInsert.remove(lt))
			LtJedis.loadCache(lt);
		if (saveUpdate.remove(lt))
			LtJedis.loadCache(lt);
    }
    
    public static void saveRedis(List<Lt> lts){
    	if (saveDelete.removeAll(lts))
			LtJedis.clearCaches(lts);
		if (saveInsert.removeAll(lts))
			LtJedis.loadCaches(lts);
		if (saveUpdate.removeAll(lts))
			LtJedis.loadCaches(lts);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Lt lt){
   		saveDatabase(lt);
   		saveRedis(lt);
    }
    
    public static void saveAll(List<Lt> lts){
   		saveDatabase(lts);
   		saveRedis(lts);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Lt lt){
	
		loadCache(lt);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Lt> lts){
		for(Lt lt : lts){
			loadCacheCascade(lt);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Lt lt,int saveType){
	
		clearCache(lt,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Lt> lts,int saveType){
		for(Lt lt : lts){
			clearCacheCascade(lt,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(lts);
			break;
		case 2:
			saveRedis(lts);
			break;
		case 3:
			saveAll(lts);
			break;
		default:
			break;
		}
	}
}