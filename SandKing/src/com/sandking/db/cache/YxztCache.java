package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.dao.YxztDao;
import com.sandking.db.bean.Yxzt;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.jedis.YxztJedis;
import com.sandking.tools.SK_List;
/**
 * 英雄状态
 */
public class YxztCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YxztCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yxzt> idCache = SK_Collections.newSortedMap();
	
	static final List<Yxzt> saveDelete = new CopyOnWriteArrayList<Yxzt>();
	
	static final List<Yxzt> saveInsert = new CopyOnWriteArrayList<Yxzt>();
	
	static final List<Yxzt> saveUpdate = new CopyOnWriteArrayList<Yxzt>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yxzt getById(int id){
		Yxzt yxzt = null;
		String key = SK_Plus.b(id).e();
		yxzt = idCache.get(key);
		
		if(yxzt==null){
			//查询数据库
			yxzt = YxztJedis.getById(id);
			if(yxzt!=null){
				//加入缓存
				loadCache(yxzt);
			}
		}
		return yxzt;
	}
	
	
	public static List<Yxzt> getCacheAll(){
		return new ArrayList<Yxzt>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yxzt> getAll(){
		List<Yxzt> yxzts = new ArrayList<Yxzt>();
		if(!isLoadAll){
			yxzts = YxztJedis.getAll();
			loadCaches(yxzts);
			isLoadAll = true;
		}else{
			yxzts = new ArrayList<Yxzt>(idCache.values());
		}
		return yxzts;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yxzt> getAllByPage(int page,int size,Integer pageCount){
		List<Yxzt> yxzts = getAll();
		yxzts = SK_List.getPage(yxzts, page, size, pageCount);
		return yxzts;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yxzt> yxzts){
		for(Yxzt yxzt : yxzts){
			loadCache(yxzt);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yxzt yxzt){
		idCache.put(SK_Plus.b(yxzt.getId()).e(),yxzt);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yxzt yxzt,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yxzt.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yxzt.getId()).e());
		}
		switch (saveType) {
		case 1:
			saveDatabase(yxzt);
			break;
		case 2:
			saveRedis(yxzt);
			break;
		case 3:
			saveAll(yxzt);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yxzt> yxzts,int clearType,int saveType){
		for(Yxzt yxzt : yxzts){
			clearCache(yxzt,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yxzts);
			break;
		case 2:
			saveRedis(yxzts);
			break;
		case 3:
			saveAll(yxzts);
			break;
		default:
			break;
		}
	}
	
	public static Yxzt insert(Yxzt yxzt){
		return insert(yxzt,false);
    }
    
    public static Yxzt update(Yxzt yxzt){
    	return update(yxzt,false);
    }
    
    public static boolean delete(Yxzt yxzt){
    	return delete(yxzt,false);
    }
    
    private static Yxzt insert(Yxzt yxzt,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yxzt = YxztJedis.insert(yxzt);
    		LASTID.set(yxzt.getId());
    	}else{
    		int _id = yxzt.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yxzt.getId() > id) {
				LASTID.set(yxzt.getId());
			}
    		//加入定时器
    	}
    	loadCache(yxzt);
    	if(!isFlush){
	    	if (!saveInsert.contains(yxzt)) {
				saveInsert.add(yxzt);
			}
		}
    	return yxzt;
    }
    
    private static Yxzt update(Yxzt yxzt,boolean isFlush){
    	clearCache(yxzt,1,0);
    	loadCache(yxzt);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yxzt)) {
				saveUpdate.add(yxzt);
			}
		}else{
			saveUpdate.remove(yxzt);
		}
    	return yxzt;
    }
    
    private static boolean delete(Yxzt yxzt,boolean isFlush){
    	clearCache(yxzt,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yxzt)) {
				saveUpdate.remove(yxzt);
				saveInsert.remove(yxzt);
				saveDelete.add(yxzt);
			}
    	}else{
    		saveUpdate.remove(yxzt);
			saveInsert.remove(yxzt);
			saveDelete.remove(yxzt);
    	}
    	return false;
    }
    
    public static Yxzt updateAndFlush(Yxzt yxzt){
    	update(yxzt,true);
    	return YxztJedis.update(yxzt);
    }
    
    public static Yxzt insertAndFlush(Yxzt yxzt){
    	int id = LASTID.get();
    	insert(yxzt,true);
    	if(id > 0){
    		yxzt = YxztJedis.insert(yxzt);
    	}
    	return yxzt;
    }
    
    public static boolean deleteAndFlush(Yxzt yxzt){
    	delete(yxzt,true);
    	return YxztJedis.delete(yxzt);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YxztDao.deleteBatch(saveDelete);
    	YxztDao.insertBatch(saveInsert);
    	YxztDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yxzt yxzt){
    	if (saveDelete.remove(yxzt))
			YxztDao.delete(yxzt);
		if (saveInsert.remove(yxzt))
			YxztDao.insert(yxzt);
		if (saveUpdate.remove(yxzt))
			YxztDao.update(yxzt);
    }
    
    public static void saveDatabase(List<Yxzt> yxzts){
    	if (saveDelete.removeAll(yxzts))
			YxztDao.deleteBatch(yxzts);
		if (saveInsert.removeAll(yxzts))
			YxztDao.insertBatch(yxzts);
		if (saveUpdate.removeAll(yxzts))
			YxztDao.updateBatch(yxzts);
    }
    
    public static void saveRedis(){
    	YxztJedis.clearCaches(saveDelete);
    	YxztJedis.loadCaches(saveInsert);
    	YxztJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yxzt yxzt){
    	if (saveDelete.remove(yxzt))
			YxztJedis.clearCache(yxzt);
		if (saveInsert.remove(yxzt))
			YxztJedis.loadCache(yxzt);
		if (saveUpdate.remove(yxzt))
			YxztJedis.loadCache(yxzt);
    }
    
    public static void saveRedis(List<Yxzt> yxzts){
    	if (saveDelete.removeAll(yxzts))
			YxztJedis.clearCaches(yxzts);
		if (saveInsert.removeAll(yxzts))
			YxztJedis.loadCaches(yxzts);
		if (saveUpdate.removeAll(yxzts))
			YxztJedis.loadCaches(yxzts);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yxzt yxzt){
   		saveDatabase(yxzt);
   		saveRedis(yxzt);
    }
    
    public static void saveAll(List<Yxzt> yxzts){
   		saveDatabase(yxzts);
   		saveRedis(yxzts);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yxzt yxzt){
	
		loadCache(yxzt);
		
		YhyxCache.loadCachesCascade(YhyxCache.getByYxztid(yxzt.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yxzt> yxzts){
		for(Yxzt yxzt : yxzts){
			loadCacheCascade(yxzt);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yxzt yxzt,int saveType){
	
		clearCache(yxzt,3,saveType);
		
		YhyxCache.clearCachesCascade(YhyxCache.getByYxztid(yxzt.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yxzt> yxzts,int saveType){
		for(Yxzt yxzt : yxzts){
			clearCacheCascade(yxzt,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yxzts);
			break;
		case 2:
			saveRedis(yxzts);
			break;
		case 3:
			saveAll(yxzts);
			break;
		default:
			break;
		}
	}
}