package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.jedis.LtlxJedis;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import com.sandking.db.bean.Ltlx;
import java.util.List;
import com.sandking.db.dao.LtlxDao;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.tools.SK_List;
/**
 * 聊天类型
 */
public class LtlxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LtlxCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Ltlx> idCache = SK_Collections.newSortedMap();
	
	static final List<Ltlx> saveDelete = new CopyOnWriteArrayList<Ltlx>();
	
	static final List<Ltlx> saveInsert = new CopyOnWriteArrayList<Ltlx>();
	
	static final List<Ltlx> saveUpdate = new CopyOnWriteArrayList<Ltlx>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Ltlx getById(int id){
		Ltlx ltlx = null;
		String key = SK_Plus.b(id).e();
		ltlx = idCache.get(key);
		
		if(ltlx==null){
			//查询数据库
			ltlx = LtlxJedis.getById(id);
			if(ltlx!=null){
				//加入缓存
				loadCache(ltlx);
			}
		}
		return ltlx;
	}
	
	
	public static List<Ltlx> getCacheAll(){
		return new ArrayList<Ltlx>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Ltlx> getAll(){
		List<Ltlx> ltlxs = new ArrayList<Ltlx>();
		if(!isLoadAll){
			ltlxs = LtlxJedis.getAll();
			loadCaches(ltlxs);
			isLoadAll = true;
		}else{
			ltlxs = new ArrayList<Ltlx>(idCache.values());
		}
		return ltlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Ltlx> getAllByPage(int page,int size,Integer pageCount){
		List<Ltlx> ltlxs = getAll();
		ltlxs = SK_List.getPage(ltlxs, page, size, pageCount);
		return ltlxs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Ltlx> ltlxs){
		for(Ltlx ltlx : ltlxs){
			loadCache(ltlx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Ltlx ltlx){
		idCache.put(SK_Plus.b(ltlx.getId()).e(),ltlx);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Ltlx ltlx,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(ltlx.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(ltlx.getId()).e());
		}
		switch (saveType) {
		case 1:
			saveDatabase(ltlx);
			break;
		case 2:
			saveRedis(ltlx);
			break;
		case 3:
			saveAll(ltlx);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Ltlx> ltlxs,int clearType,int saveType){
		for(Ltlx ltlx : ltlxs){
			clearCache(ltlx,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(ltlxs);
			break;
		case 2:
			saveRedis(ltlxs);
			break;
		case 3:
			saveAll(ltlxs);
			break;
		default:
			break;
		}
	}
	
	public static Ltlx insert(Ltlx ltlx){
		return insert(ltlx,false);
    }
    
    public static Ltlx update(Ltlx ltlx){
    	return update(ltlx,false);
    }
    
    public static boolean delete(Ltlx ltlx){
    	return delete(ltlx,false);
    }
    
    private static Ltlx insert(Ltlx ltlx,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		ltlx = LtlxJedis.insert(ltlx);
    		LASTID.set(ltlx.getId());
    	}else{
    		int _id = ltlx.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (ltlx.getId() > id) {
				LASTID.set(ltlx.getId());
			}
    		//加入定时器
    	}
    	loadCache(ltlx);
    	if(!isFlush){
	    	if (!saveInsert.contains(ltlx)) {
				saveInsert.add(ltlx);
			}
		}
    	return ltlx;
    }
    
    private static Ltlx update(Ltlx ltlx,boolean isFlush){
    	clearCache(ltlx,1,0);
    	loadCache(ltlx);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(ltlx)) {
				saveUpdate.add(ltlx);
			}
		}else{
			saveUpdate.remove(ltlx);
		}
    	return ltlx;
    }
    
    private static boolean delete(Ltlx ltlx,boolean isFlush){
    	clearCache(ltlx,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(ltlx)) {
				saveUpdate.remove(ltlx);
				saveInsert.remove(ltlx);
				saveDelete.add(ltlx);
			}
    	}else{
    		saveUpdate.remove(ltlx);
			saveInsert.remove(ltlx);
			saveDelete.remove(ltlx);
    	}
    	return false;
    }
    
    public static Ltlx updateAndFlush(Ltlx ltlx){
    	update(ltlx,true);
    	return LtlxJedis.update(ltlx);
    }
    
    public static Ltlx insertAndFlush(Ltlx ltlx){
    	int id = LASTID.get();
    	insert(ltlx,true);
    	if(id > 0){
    		ltlx = LtlxJedis.insert(ltlx);
    	}
    	return ltlx;
    }
    
    public static boolean deleteAndFlush(Ltlx ltlx){
    	delete(ltlx,true);
    	return LtlxJedis.delete(ltlx);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	LtlxDao.deleteBatch(saveDelete);
    	LtlxDao.insertBatch(saveInsert);
    	LtlxDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Ltlx ltlx){
    	if (saveDelete.remove(ltlx))
			LtlxDao.delete(ltlx);
		if (saveInsert.remove(ltlx))
			LtlxDao.insert(ltlx);
		if (saveUpdate.remove(ltlx))
			LtlxDao.update(ltlx);
    }
    
    public static void saveDatabase(List<Ltlx> ltlxs){
    	if (saveDelete.removeAll(ltlxs))
			LtlxDao.deleteBatch(ltlxs);
		if (saveInsert.removeAll(ltlxs))
			LtlxDao.insertBatch(ltlxs);
		if (saveUpdate.removeAll(ltlxs))
			LtlxDao.updateBatch(ltlxs);
    }
    
    public static void saveRedis(){
    	LtlxJedis.clearCaches(saveDelete);
    	LtlxJedis.loadCaches(saveInsert);
    	LtlxJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Ltlx ltlx){
    	if (saveDelete.remove(ltlx))
			LtlxJedis.clearCache(ltlx);
		if (saveInsert.remove(ltlx))
			LtlxJedis.loadCache(ltlx);
		if (saveUpdate.remove(ltlx))
			LtlxJedis.loadCache(ltlx);
    }
    
    public static void saveRedis(List<Ltlx> ltlxs){
    	if (saveDelete.removeAll(ltlxs))
			LtlxJedis.clearCaches(ltlxs);
		if (saveInsert.removeAll(ltlxs))
			LtlxJedis.loadCaches(ltlxs);
		if (saveUpdate.removeAll(ltlxs))
			LtlxJedis.loadCaches(ltlxs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Ltlx ltlx){
   		saveDatabase(ltlx);
   		saveRedis(ltlx);
    }
    
    public static void saveAll(List<Ltlx> ltlxs){
   		saveDatabase(ltlxs);
   		saveRedis(ltlxs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Ltlx ltlx){
	
		loadCache(ltlx);
		
		LtCache.loadCachesCascade(LtCache.getByLtlx_id(ltlx.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Ltlx> ltlxs){
		for(Ltlx ltlx : ltlxs){
			loadCacheCascade(ltlx);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Ltlx ltlx,int saveType){
	
		clearCache(ltlx,3,saveType);
		
		LtCache.clearCachesCascade(LtCache.getByLtlx_id(ltlx.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Ltlx> ltlxs,int saveType){
		for(Ltlx ltlx : ltlxs){
			clearCacheCascade(ltlx,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(ltlxs);
			break;
		case 2:
			saveRedis(ltlxs);
			break;
		case 3:
			saveAll(ltlxs);
			break;
		default:
			break;
		}
	}
}