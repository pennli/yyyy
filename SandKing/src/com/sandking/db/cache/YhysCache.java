package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.bean.Yhys;
import com.sandking.db.dao.YhysDao;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
import com.sandking.db.jedis.YhysJedis;
/**
 * 用户映射
 */
public class YhysCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhysCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhys> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yhys> saveDelete = new CopyOnWriteArrayList<Yhys>();
	
	static final List<Yhys> saveInsert = new CopyOnWriteArrayList<Yhys>();
	
	static final List<Yhys> saveUpdate = new CopyOnWriteArrayList<Yhys>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhys getById(int id){
		Yhys yhys = null;
		String key = SK_Plus.b(id).e();
		yhys = idCache.get(key);
		
		if(yhys==null){
			//查询数据库
			yhys = YhysJedis.getById(id);
			if(yhys!=null){
				//加入缓存
				loadCache(yhys);
			}
		}
		return yhys;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhys> getByYh_id(int yh_id){
		List<Yhys> yhyss = new ArrayList<Yhys>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			Yhys yhys = null;
			for (String k : keys) {
				yhys = getById(Integer.valueOf(k));
				if (yhys == null) continue;
					yhyss.add(yhys);
			}
		}else{
			yhyss = YhysJedis.getByYh_id(yh_id);
			if(!yhyss.isEmpty()){
				loadCaches(yhyss);
			}
		}
		return yhyss;
	}
	
	public static List<Yhys> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhys> yhyss = getByYh_id(yh_id);
		yhyss = SK_List.getPage(yhyss, page, size, pageCount);
		return yhyss;
	}
	
	public static List<Yhys> getCacheAll(){
		return new ArrayList<Yhys>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhys> getAll(){
		List<Yhys> yhyss = new ArrayList<Yhys>();
		if(!isLoadAll){
			yhyss = YhysJedis.getAll();
			loadCaches(yhyss);
			isLoadAll = true;
		}else{
			yhyss = new ArrayList<Yhys>(idCache.values());
		}
		return yhyss;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhys> getAllByPage(int page,int size,Integer pageCount){
		List<Yhys> yhyss = getAll();
		yhyss = SK_List.getPage(yhyss, page, size, pageCount);
		return yhyss;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhys> yhyss){
		for(Yhys yhys : yhyss){
			loadCache(yhys);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhys yhys){
		idCache.put(SK_Plus.b(yhys.getId()).e(),yhys);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhys.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhys.getId()));
		yh_idCache.put(SK_Plus.b(yhys.getYh_id()).e(),yh_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhys yhys,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhys.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhys.getId()).e());
		}
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhys.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhys.getId()))) {
			yh_idset.remove(String.valueOf(yhys.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhys.getYh_id()).e(),yh_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yhys);
			break;
		case 2:
			saveRedis(yhys);
			break;
		case 3:
			saveAll(yhys);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhys> yhyss,int clearType,int saveType){
		for(Yhys yhys : yhyss){
			clearCache(yhys,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhyss);
			break;
		case 2:
			saveRedis(yhyss);
			break;
		case 3:
			saveAll(yhyss);
			break;
		default:
			break;
		}
	}
	
	public static Yhys insert(Yhys yhys){
		return insert(yhys,false);
    }
    
    public static Yhys update(Yhys yhys){
    	return update(yhys,false);
    }
    
    public static boolean delete(Yhys yhys){
    	return delete(yhys,false);
    }
    
    private static Yhys insert(Yhys yhys,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhys = YhysJedis.insert(yhys);
    		LASTID.set(yhys.getId());
    	}else{
    		int _id = yhys.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhys.getId() > id) {
				LASTID.set(yhys.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhys);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhys)) {
				saveInsert.add(yhys);
			}
		}
    	return yhys;
    }
    
    private static Yhys update(Yhys yhys,boolean isFlush){
    	clearCache(yhys,1,0);
    	loadCache(yhys);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhys)) {
				saveUpdate.add(yhys);
			}
		}else{
			saveUpdate.remove(yhys);
		}
    	return yhys;
    }
    
    private static boolean delete(Yhys yhys,boolean isFlush){
    	clearCache(yhys,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhys)) {
				saveUpdate.remove(yhys);
				saveInsert.remove(yhys);
				saveDelete.add(yhys);
			}
    	}else{
    		saveUpdate.remove(yhys);
			saveInsert.remove(yhys);
			saveDelete.remove(yhys);
    	}
    	return false;
    }
    
    public static Yhys updateAndFlush(Yhys yhys){
    	update(yhys,true);
    	return YhysJedis.update(yhys);
    }
    
    public static Yhys insertAndFlush(Yhys yhys){
    	int id = LASTID.get();
    	insert(yhys,true);
    	if(id > 0){
    		yhys = YhysJedis.insert(yhys);
    	}
    	return yhys;
    }
    
    public static boolean deleteAndFlush(Yhys yhys){
    	delete(yhys,true);
    	return YhysJedis.delete(yhys);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhysDao.deleteBatch(saveDelete);
    	YhysDao.insertBatch(saveInsert);
    	YhysDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhys yhys){
    	if (saveDelete.remove(yhys))
			YhysDao.delete(yhys);
		if (saveInsert.remove(yhys))
			YhysDao.insert(yhys);
		if (saveUpdate.remove(yhys))
			YhysDao.update(yhys);
    }
    
    public static void saveDatabase(List<Yhys> yhyss){
    	if (saveDelete.removeAll(yhyss))
			YhysDao.deleteBatch(yhyss);
		if (saveInsert.removeAll(yhyss))
			YhysDao.insertBatch(yhyss);
		if (saveUpdate.removeAll(yhyss))
			YhysDao.updateBatch(yhyss);
    }
    
    public static void saveRedis(){
    	YhysJedis.clearCaches(saveDelete);
    	YhysJedis.loadCaches(saveInsert);
    	YhysJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhys yhys){
    	if (saveDelete.remove(yhys))
			YhysJedis.clearCache(yhys);
		if (saveInsert.remove(yhys))
			YhysJedis.loadCache(yhys);
		if (saveUpdate.remove(yhys))
			YhysJedis.loadCache(yhys);
    }
    
    public static void saveRedis(List<Yhys> yhyss){
    	if (saveDelete.removeAll(yhyss))
			YhysJedis.clearCaches(yhyss);
		if (saveInsert.removeAll(yhyss))
			YhysJedis.loadCaches(yhyss);
		if (saveUpdate.removeAll(yhyss))
			YhysJedis.loadCaches(yhyss);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhys yhys){
   		saveDatabase(yhys);
   		saveRedis(yhys);
    }
    
    public static void saveAll(List<Yhys> yhyss){
   		saveDatabase(yhyss);
   		saveRedis(yhyss);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhys yhys){
	
		loadCache(yhys);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhys> yhyss){
		for(Yhys yhys : yhyss){
			loadCacheCascade(yhys);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhys yhys,int saveType){
	
		clearCache(yhys,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhys> yhyss,int saveType){
		for(Yhys yhys : yhyss){
			clearCacheCascade(yhys,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhyss);
			break;
		case 2:
			saveRedis(yhyss);
			break;
		case 3:
			saveAll(yhyss);
			break;
		default:
			break;
		}
	}
}