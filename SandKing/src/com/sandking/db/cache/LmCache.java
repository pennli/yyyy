package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.jedis.LmJedis;
import com.sandking.db.dao.LmDao;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
import com.sandking.db.bean.Lm;
/**
 * 联盟
 */
public class LmCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LmCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Lm> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> cjridCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Lm> saveDelete = new CopyOnWriteArrayList<Lm>();
	
	static final List<Lm> saveInsert = new CopyOnWriteArrayList<Lm>();
	
	static final List<Lm> saveUpdate = new CopyOnWriteArrayList<Lm>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Lm getById(int id){
		Lm lm = null;
		String key = SK_Plus.b(id).e();
		lm = idCache.get(key);
		
		if(lm==null){
			//查询数据库
			lm = LmJedis.getById(id);
			if(lm!=null){
				//加入缓存
				loadCache(lm);
			}
		}
		return lm;
	}
	
	/**
	 * 根据( 创建人id ) 查询
	 */
	public static List<Lm> getByCjrid(int cjrid){
		List<Lm> lms = new ArrayList<Lm>();
		String key = SK_Plus.b(cjrid).e();
		Set<String> keys = cjridCache.get(key);
		if(keys != null){
			Lm lm = null;
			for (String k : keys) {
				lm = getById(Integer.valueOf(k));
				if (lm == null) continue;
					lms.add(lm);
			}
		}else{
			lms = LmJedis.getByCjrid(cjrid);
			if(!lms.isEmpty()){
				loadCaches(lms);
			}
		}
		return lms;
	}
	
	public static List<Lm> getByPageCjrid(int cjrid,int page,int size,Integer pageCount){
		List<Lm> lms = getByCjrid(cjrid);
		lms = SK_List.getPage(lms, page, size, pageCount);
		return lms;
	}
	
	public static List<Lm> getCacheAll(){
		return new ArrayList<Lm>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Lm> getAll(){
		List<Lm> lms = new ArrayList<Lm>();
		if(!isLoadAll){
			lms = LmJedis.getAll();
			loadCaches(lms);
			isLoadAll = true;
		}else{
			lms = new ArrayList<Lm>(idCache.values());
		}
		return lms;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Lm> getAllByPage(int page,int size,Integer pageCount){
		List<Lm> lms = getAll();
		lms = SK_List.getPage(lms, page, size, pageCount);
		return lms;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Lm> lms){
		for(Lm lm : lms){
			loadCache(lm);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Lm lm){
		idCache.put(SK_Plus.b(lm.getId()).e(),lm);
		Set<String> cjridset = cjridCache.get(String.valueOf(SK_Plus.b(lm.getCjrid()).e()));
		if(cjridset == null){
			cjridset = new HashSet<String>();
		}
		cjridset.add(String.valueOf(lm.getId()));
		cjridCache.put(SK_Plus.b(lm.getCjrid()).e(),cjridset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Lm lm,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(lm.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(lm.getId()).e());
		}
		Set<String> cjridset = cjridCache.get(String.valueOf(SK_Plus.b(lm.getCjrid()).e()));
		if(cjridset == null){
			cjridset = new HashSet<String>();
		}
		if (cjridset.contains(String.valueOf(lm.getId()))) {
			cjridset.remove(String.valueOf(lm.getId()));
		}
		cjridCache.put(SK_Plus.b(lm.getCjrid()).e(),cjridset);
		switch (saveType) {
		case 1:
			saveDatabase(lm);
			break;
		case 2:
			saveRedis(lm);
			break;
		case 3:
			saveAll(lm);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Lm> lms,int clearType,int saveType){
		for(Lm lm : lms){
			clearCache(lm,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(lms);
			break;
		case 2:
			saveRedis(lms);
			break;
		case 3:
			saveAll(lms);
			break;
		default:
			break;
		}
	}
	
	public static Lm insert(Lm lm){
		return insert(lm,false);
    }
    
    public static Lm update(Lm lm){
    	return update(lm,false);
    }
    
    public static boolean delete(Lm lm){
    	return delete(lm,false);
    }
    
    private static Lm insert(Lm lm,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		lm = LmJedis.insert(lm);
    		LASTID.set(lm.getId());
    	}else{
    		int _id = lm.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (lm.getId() > id) {
				LASTID.set(lm.getId());
			}
    		//加入定时器
    	}
    	loadCache(lm);
    	if(!isFlush){
	    	if (!saveInsert.contains(lm)) {
				saveInsert.add(lm);
			}
		}
    	return lm;
    }
    
    private static Lm update(Lm lm,boolean isFlush){
    	clearCache(lm,1,0);
    	loadCache(lm);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(lm)) {
				saveUpdate.add(lm);
			}
		}else{
			saveUpdate.remove(lm);
		}
    	return lm;
    }
    
    private static boolean delete(Lm lm,boolean isFlush){
    	clearCache(lm,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(lm)) {
				saveUpdate.remove(lm);
				saveInsert.remove(lm);
				saveDelete.add(lm);
			}
    	}else{
    		saveUpdate.remove(lm);
			saveInsert.remove(lm);
			saveDelete.remove(lm);
    	}
    	return false;
    }
    
    public static Lm updateAndFlush(Lm lm){
    	update(lm,true);
    	return LmJedis.update(lm);
    }
    
    public static Lm insertAndFlush(Lm lm){
    	int id = LASTID.get();
    	insert(lm,true);
    	if(id > 0){
    		lm = LmJedis.insert(lm);
    	}
    	return lm;
    }
    
    public static boolean deleteAndFlush(Lm lm){
    	delete(lm,true);
    	return LmJedis.delete(lm);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	LmDao.deleteBatch(saveDelete);
    	LmDao.insertBatch(saveInsert);
    	LmDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Lm lm){
    	if (saveDelete.remove(lm))
			LmDao.delete(lm);
		if (saveInsert.remove(lm))
			LmDao.insert(lm);
		if (saveUpdate.remove(lm))
			LmDao.update(lm);
    }
    
    public static void saveDatabase(List<Lm> lms){
    	if (saveDelete.removeAll(lms))
			LmDao.deleteBatch(lms);
		if (saveInsert.removeAll(lms))
			LmDao.insertBatch(lms);
		if (saveUpdate.removeAll(lms))
			LmDao.updateBatch(lms);
    }
    
    public static void saveRedis(){
    	LmJedis.clearCaches(saveDelete);
    	LmJedis.loadCaches(saveInsert);
    	LmJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Lm lm){
    	if (saveDelete.remove(lm))
			LmJedis.clearCache(lm);
		if (saveInsert.remove(lm))
			LmJedis.loadCache(lm);
		if (saveUpdate.remove(lm))
			LmJedis.loadCache(lm);
    }
    
    public static void saveRedis(List<Lm> lms){
    	if (saveDelete.removeAll(lms))
			LmJedis.clearCaches(lms);
		if (saveInsert.removeAll(lms))
			LmJedis.loadCaches(lms);
		if (saveUpdate.removeAll(lms))
			LmJedis.loadCaches(lms);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Lm lm){
   		saveDatabase(lm);
   		saveRedis(lm);
    }
    
    public static void saveAll(List<Lm> lms){
   		saveDatabase(lms);
   		saveRedis(lms);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Lm lm){
	
		loadCache(lm);
		
		YhCache.loadCachesCascade(YhCache.getByLm_id(lm.getId()));
		LtCache.loadCachesCascade(LtCache.getByLm_id(lm.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Lm> lms){
		for(Lm lm : lms){
			loadCacheCascade(lm);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Lm lm,int saveType){
	
		clearCache(lm,3,saveType);
		
		YhCache.clearCachesCascade(YhCache.getByLm_id(lm.getId()),saveType);
		LtCache.clearCachesCascade(LtCache.getByLm_id(lm.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Lm> lms,int saveType){
		for(Lm lm : lms){
			clearCacheCascade(lm,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(lms);
			break;
		case 2:
			saveRedis(lms);
			break;
		case 3:
			saveAll(lms);
			break;
		default:
			break;
		}
	}
}