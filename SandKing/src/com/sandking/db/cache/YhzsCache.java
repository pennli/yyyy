package com.sandking.db.cache;

import com.sandking.db.jedis.YhzsJedis;
import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.bean.Yhzs;
import com.sandking.db.dao.YhzsDao;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
/**
 * 用户装饰
 */
public class YhzsCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhzsCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhzs> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yhzs> saveDelete = new CopyOnWriteArrayList<Yhzs>();
	
	static final List<Yhzs> saveInsert = new CopyOnWriteArrayList<Yhzs>();
	
	static final List<Yhzs> saveUpdate = new CopyOnWriteArrayList<Yhzs>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhzs getById(int id){
		Yhzs yhzs = null;
		String key = SK_Plus.b(id).e();
		yhzs = idCache.get(key);
		
		if(yhzs==null){
			//查询数据库
			yhzs = YhzsJedis.getById(id);
			if(yhzs!=null){
				//加入缓存
				loadCache(yhzs);
			}
		}
		return yhzs;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhzs> getByYh_id(int yh_id){
		List<Yhzs> yhzss = new ArrayList<Yhzs>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			Yhzs yhzs = null;
			for (String k : keys) {
				yhzs = getById(Integer.valueOf(k));
				if (yhzs == null) continue;
					yhzss.add(yhzs);
			}
		}else{
			yhzss = YhzsJedis.getByYh_id(yh_id);
			if(!yhzss.isEmpty()){
				loadCaches(yhzss);
			}
		}
		return yhzss;
	}
	
	public static List<Yhzs> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhzs> yhzss = getByYh_id(yh_id);
		yhzss = SK_List.getPage(yhzss, page, size, pageCount);
		return yhzss;
	}
	
	public static List<Yhzs> getCacheAll(){
		return new ArrayList<Yhzs>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhzs> getAll(){
		List<Yhzs> yhzss = new ArrayList<Yhzs>();
		if(!isLoadAll){
			yhzss = YhzsJedis.getAll();
			loadCaches(yhzss);
			isLoadAll = true;
		}else{
			yhzss = new ArrayList<Yhzs>(idCache.values());
		}
		return yhzss;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhzs> getAllByPage(int page,int size,Integer pageCount){
		List<Yhzs> yhzss = getAll();
		yhzss = SK_List.getPage(yhzss, page, size, pageCount);
		return yhzss;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhzs> yhzss){
		for(Yhzs yhzs : yhzss){
			loadCache(yhzs);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhzs yhzs){
		idCache.put(SK_Plus.b(yhzs.getId()).e(),yhzs);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhzs.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhzs.getId()));
		yh_idCache.put(SK_Plus.b(yhzs.getYh_id()).e(),yh_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhzs yhzs,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhzs.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhzs.getId()).e());
		}
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhzs.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhzs.getId()))) {
			yh_idset.remove(String.valueOf(yhzs.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhzs.getYh_id()).e(),yh_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yhzs);
			break;
		case 2:
			saveRedis(yhzs);
			break;
		case 3:
			saveAll(yhzs);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhzs> yhzss,int clearType,int saveType){
		for(Yhzs yhzs : yhzss){
			clearCache(yhzs,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhzss);
			break;
		case 2:
			saveRedis(yhzss);
			break;
		case 3:
			saveAll(yhzss);
			break;
		default:
			break;
		}
	}
	
	public static Yhzs insert(Yhzs yhzs){
		return insert(yhzs,false);
    }
    
    public static Yhzs update(Yhzs yhzs){
    	return update(yhzs,false);
    }
    
    public static boolean delete(Yhzs yhzs){
    	return delete(yhzs,false);
    }
    
    private static Yhzs insert(Yhzs yhzs,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhzs = YhzsJedis.insert(yhzs);
    		LASTID.set(yhzs.getId());
    	}else{
    		int _id = yhzs.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhzs.getId() > id) {
				LASTID.set(yhzs.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhzs);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhzs)) {
				saveInsert.add(yhzs);
			}
		}
    	return yhzs;
    }
    
    private static Yhzs update(Yhzs yhzs,boolean isFlush){
    	clearCache(yhzs,1,0);
    	loadCache(yhzs);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhzs)) {
				saveUpdate.add(yhzs);
			}
		}else{
			saveUpdate.remove(yhzs);
		}
    	return yhzs;
    }
    
    private static boolean delete(Yhzs yhzs,boolean isFlush){
    	clearCache(yhzs,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhzs)) {
				saveUpdate.remove(yhzs);
				saveInsert.remove(yhzs);
				saveDelete.add(yhzs);
			}
    	}else{
    		saveUpdate.remove(yhzs);
			saveInsert.remove(yhzs);
			saveDelete.remove(yhzs);
    	}
    	return false;
    }
    
    public static Yhzs updateAndFlush(Yhzs yhzs){
    	update(yhzs,true);
    	return YhzsJedis.update(yhzs);
    }
    
    public static Yhzs insertAndFlush(Yhzs yhzs){
    	int id = LASTID.get();
    	insert(yhzs,true);
    	if(id > 0){
    		yhzs = YhzsJedis.insert(yhzs);
    	}
    	return yhzs;
    }
    
    public static boolean deleteAndFlush(Yhzs yhzs){
    	delete(yhzs,true);
    	return YhzsJedis.delete(yhzs);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhzsDao.deleteBatch(saveDelete);
    	YhzsDao.insertBatch(saveInsert);
    	YhzsDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhzs yhzs){
    	if (saveDelete.remove(yhzs))
			YhzsDao.delete(yhzs);
		if (saveInsert.remove(yhzs))
			YhzsDao.insert(yhzs);
		if (saveUpdate.remove(yhzs))
			YhzsDao.update(yhzs);
    }
    
    public static void saveDatabase(List<Yhzs> yhzss){
    	if (saveDelete.removeAll(yhzss))
			YhzsDao.deleteBatch(yhzss);
		if (saveInsert.removeAll(yhzss))
			YhzsDao.insertBatch(yhzss);
		if (saveUpdate.removeAll(yhzss))
			YhzsDao.updateBatch(yhzss);
    }
    
    public static void saveRedis(){
    	YhzsJedis.clearCaches(saveDelete);
    	YhzsJedis.loadCaches(saveInsert);
    	YhzsJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhzs yhzs){
    	if (saveDelete.remove(yhzs))
			YhzsJedis.clearCache(yhzs);
		if (saveInsert.remove(yhzs))
			YhzsJedis.loadCache(yhzs);
		if (saveUpdate.remove(yhzs))
			YhzsJedis.loadCache(yhzs);
    }
    
    public static void saveRedis(List<Yhzs> yhzss){
    	if (saveDelete.removeAll(yhzss))
			YhzsJedis.clearCaches(yhzss);
		if (saveInsert.removeAll(yhzss))
			YhzsJedis.loadCaches(yhzss);
		if (saveUpdate.removeAll(yhzss))
			YhzsJedis.loadCaches(yhzss);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhzs yhzs){
   		saveDatabase(yhzs);
   		saveRedis(yhzs);
    }
    
    public static void saveAll(List<Yhzs> yhzss){
   		saveDatabase(yhzss);
   		saveRedis(yhzss);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhzs yhzs){
	
		loadCache(yhzs);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhzs> yhzss){
		for(Yhzs yhzs : yhzss){
			loadCacheCascade(yhzs);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhzs yhzs,int saveType){
	
		clearCache(yhzs,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhzs> yhzss,int saveType){
		for(Yhzs yhzs : yhzss){
			clearCacheCascade(yhzs,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhzss);
			break;
		case 2:
			saveRedis(yhzss);
			break;
		case 3:
			saveAll(yhzss);
			break;
		default:
			break;
		}
	}
}