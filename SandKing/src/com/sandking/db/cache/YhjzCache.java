package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.dao.YhjzDao;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.db.bean.Yhjz;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.jedis.YhjzJedis;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
/**
 * 用户建筑
 */
public class YhjzCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhjzCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhjz> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yhjz> saveDelete = new CopyOnWriteArrayList<Yhjz>();
	
	static final List<Yhjz> saveInsert = new CopyOnWriteArrayList<Yhjz>();
	
	static final List<Yhjz> saveUpdate = new CopyOnWriteArrayList<Yhjz>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhjz getById(int id){
		Yhjz yhjz = null;
		String key = SK_Plus.b(id).e();
		yhjz = idCache.get(key);
		
		if(yhjz==null){
			//查询数据库
			yhjz = YhjzJedis.getById(id);
			if(yhjz!=null){
				//加入缓存
				loadCache(yhjz);
			}
		}
		return yhjz;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhjz> getByYh_id(int yh_id){
		List<Yhjz> yhjzs = new ArrayList<Yhjz>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			Yhjz yhjz = null;
			for (String k : keys) {
				yhjz = getById(Integer.valueOf(k));
				if (yhjz == null) continue;
					yhjzs.add(yhjz);
			}
		}else{
			yhjzs = YhjzJedis.getByYh_id(yh_id);
			if(!yhjzs.isEmpty()){
				loadCaches(yhjzs);
			}
		}
		return yhjzs;
	}
	
	public static List<Yhjz> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhjz> yhjzs = getByYh_id(yh_id);
		yhjzs = SK_List.getPage(yhjzs, page, size, pageCount);
		return yhjzs;
	}
	
	public static List<Yhjz> getCacheAll(){
		return new ArrayList<Yhjz>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhjz> getAll(){
		List<Yhjz> yhjzs = new ArrayList<Yhjz>();
		if(!isLoadAll){
			yhjzs = YhjzJedis.getAll();
			loadCaches(yhjzs);
			isLoadAll = true;
		}else{
			yhjzs = new ArrayList<Yhjz>(idCache.values());
		}
		return yhjzs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhjz> getAllByPage(int page,int size,Integer pageCount){
		List<Yhjz> yhjzs = getAll();
		yhjzs = SK_List.getPage(yhjzs, page, size, pageCount);
		return yhjzs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhjz> yhjzs){
		for(Yhjz yhjz : yhjzs){
			loadCache(yhjz);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhjz yhjz){
		idCache.put(SK_Plus.b(yhjz.getId()).e(),yhjz);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhjz.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhjz.getId()));
		yh_idCache.put(SK_Plus.b(yhjz.getYh_id()).e(),yh_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhjz yhjz,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhjz.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhjz.getId()).e());
		}
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhjz.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhjz.getId()))) {
			yh_idset.remove(String.valueOf(yhjz.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhjz.getYh_id()).e(),yh_idset);
		switch (saveType) {
		case 1:
			saveDatabase(yhjz);
			break;
		case 2:
			saveRedis(yhjz);
			break;
		case 3:
			saveAll(yhjz);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhjz> yhjzs,int clearType,int saveType){
		for(Yhjz yhjz : yhjzs){
			clearCache(yhjz,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhjzs);
			break;
		case 2:
			saveRedis(yhjzs);
			break;
		case 3:
			saveAll(yhjzs);
			break;
		default:
			break;
		}
	}
	
	public static Yhjz insert(Yhjz yhjz){
		return insert(yhjz,false);
    }
    
    public static Yhjz update(Yhjz yhjz){
    	return update(yhjz,false);
    }
    
    public static boolean delete(Yhjz yhjz){
    	return delete(yhjz,false);
    }
    
    private static Yhjz insert(Yhjz yhjz,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhjz = YhjzJedis.insert(yhjz);
    		LASTID.set(yhjz.getId());
    	}else{
    		int _id = yhjz.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhjz.getId() > id) {
				LASTID.set(yhjz.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhjz);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhjz)) {
				saveInsert.add(yhjz);
			}
		}
    	return yhjz;
    }
    
    private static Yhjz update(Yhjz yhjz,boolean isFlush){
    	clearCache(yhjz,1,0);
    	loadCache(yhjz);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhjz)) {
				saveUpdate.add(yhjz);
			}
		}else{
			saveUpdate.remove(yhjz);
		}
    	return yhjz;
    }
    
    private static boolean delete(Yhjz yhjz,boolean isFlush){
    	clearCache(yhjz,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhjz)) {
				saveUpdate.remove(yhjz);
				saveInsert.remove(yhjz);
				saveDelete.add(yhjz);
			}
    	}else{
    		saveUpdate.remove(yhjz);
			saveInsert.remove(yhjz);
			saveDelete.remove(yhjz);
    	}
    	return false;
    }
    
    public static Yhjz updateAndFlush(Yhjz yhjz){
    	update(yhjz,true);
    	return YhjzJedis.update(yhjz);
    }
    
    public static Yhjz insertAndFlush(Yhjz yhjz){
    	int id = LASTID.get();
    	insert(yhjz,true);
    	if(id > 0){
    		yhjz = YhjzJedis.insert(yhjz);
    	}
    	return yhjz;
    }
    
    public static boolean deleteAndFlush(Yhjz yhjz){
    	delete(yhjz,true);
    	return YhjzJedis.delete(yhjz);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhjzDao.deleteBatch(saveDelete);
    	YhjzDao.insertBatch(saveInsert);
    	YhjzDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhjz yhjz){
    	if (saveDelete.remove(yhjz))
			YhjzDao.delete(yhjz);
		if (saveInsert.remove(yhjz))
			YhjzDao.insert(yhjz);
		if (saveUpdate.remove(yhjz))
			YhjzDao.update(yhjz);
    }
    
    public static void saveDatabase(List<Yhjz> yhjzs){
    	if (saveDelete.removeAll(yhjzs))
			YhjzDao.deleteBatch(yhjzs);
		if (saveInsert.removeAll(yhjzs))
			YhjzDao.insertBatch(yhjzs);
		if (saveUpdate.removeAll(yhjzs))
			YhjzDao.updateBatch(yhjzs);
    }
    
    public static void saveRedis(){
    	YhjzJedis.clearCaches(saveDelete);
    	YhjzJedis.loadCaches(saveInsert);
    	YhjzJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhjz yhjz){
    	if (saveDelete.remove(yhjz))
			YhjzJedis.clearCache(yhjz);
		if (saveInsert.remove(yhjz))
			YhjzJedis.loadCache(yhjz);
		if (saveUpdate.remove(yhjz))
			YhjzJedis.loadCache(yhjz);
    }
    
    public static void saveRedis(List<Yhjz> yhjzs){
    	if (saveDelete.removeAll(yhjzs))
			YhjzJedis.clearCaches(yhjzs);
		if (saveInsert.removeAll(yhjzs))
			YhjzJedis.loadCaches(yhjzs);
		if (saveUpdate.removeAll(yhjzs))
			YhjzJedis.loadCaches(yhjzs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhjz yhjz){
   		saveDatabase(yhjz);
   		saveRedis(yhjz);
    }
    
    public static void saveAll(List<Yhjz> yhjzs){
   		saveDatabase(yhjzs);
   		saveRedis(yhjzs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhjz yhjz){
	
		loadCache(yhjz);
		
		BydlCache.loadCachesCascade(BydlCache.getByYhjz_id(yhjz.getId()));
		JzdlCache.loadCachesCascade(JzdlCache.getByYhjz_id(yhjz.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhjz> yhjzs){
		for(Yhjz yhjz : yhjzs){
			loadCacheCascade(yhjz);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhjz yhjz,int saveType){
	
		clearCache(yhjz,3,saveType);
		
		BydlCache.clearCachesCascade(BydlCache.getByYhjz_id(yhjz.getId()),saveType);
		JzdlCache.clearCachesCascade(JzdlCache.getByYhjz_id(yhjz.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhjz> yhjzs,int saveType){
		for(Yhjz yhjz : yhjzs){
			clearCacheCascade(yhjz,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhjzs);
			break;
		case 2:
			saveRedis(yhjzs);
			break;
		case 3:
			saveAll(yhjzs);
			break;
		default:
			break;
		}
	}
}