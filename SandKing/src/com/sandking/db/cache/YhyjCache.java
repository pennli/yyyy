package com.sandking.db.cache;

import com.sandking.db.bean.Yhyj;
import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.jedis.YhyjJedis;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
import com.sandking.db.dao.YhyjDao;
/**
 * 用户邮件
 */
public class YhyjCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhyjCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhyj> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> fjridCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> jsridCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yhyj> saveDelete = new CopyOnWriteArrayList<Yhyj>();
	
	static final List<Yhyj> saveInsert = new CopyOnWriteArrayList<Yhyj>();
	
	static final List<Yhyj> saveUpdate = new CopyOnWriteArrayList<Yhyj>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyj getById(int id){
		Yhyj yhyj = null;
		String key = SK_Plus.b(id).e();
		yhyj = idCache.get(key);
		
		if(yhyj==null){
			//查询数据库
			yhyj = YhyjJedis.getById(id);
			if(yhyj!=null){
				//加入缓存
				loadCache(yhyj);
			}
		}
		return yhyj;
	}
	
	/**
	 * 根据( 发件人id ) 查询
	 */
	public static List<Yhyj> getByFjrid(int fjrid){
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		String key = SK_Plus.b(fjrid).e();
		Set<String> keys = fjridCache.get(key);
		if(keys != null){
			Yhyj yhyj = null;
			for (String k : keys) {
				yhyj = getById(Integer.valueOf(k));
				if (yhyj == null) continue;
					yhyjs.add(yhyj);
			}
		}else{
			yhyjs = YhyjJedis.getByFjrid(fjrid);
			if(!yhyjs.isEmpty()){
				loadCaches(yhyjs);
			}
		}
		return yhyjs;
	}
	
	public static List<Yhyj> getByPageFjrid(int fjrid,int page,int size,Integer pageCount){
		List<Yhyj> yhyjs = getByFjrid(fjrid);
		yhyjs = SK_List.getPage(yhyjs, page, size, pageCount);
		return yhyjs;
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Yhyj> getByJsrid(int jsrid){
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		String key = SK_Plus.b(jsrid).e();
		Set<String> keys = jsridCache.get(key);
		if(keys != null){
			Yhyj yhyj = null;
			for (String k : keys) {
				yhyj = getById(Integer.valueOf(k));
				if (yhyj == null) continue;
					yhyjs.add(yhyj);
			}
		}else{
			yhyjs = YhyjJedis.getByJsrid(jsrid);
			if(!yhyjs.isEmpty()){
				loadCaches(yhyjs);
			}
		}
		return yhyjs;
	}
	
	public static List<Yhyj> getByPageJsrid(int jsrid,int page,int size,Integer pageCount){
		List<Yhyj> yhyjs = getByJsrid(jsrid);
		yhyjs = SK_List.getPage(yhyjs, page, size, pageCount);
		return yhyjs;
	}
	
	public static List<Yhyj> getCacheAll(){
		return new ArrayList<Yhyj>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhyj> getAll(){
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		if(!isLoadAll){
			yhyjs = YhyjJedis.getAll();
			loadCaches(yhyjs);
			isLoadAll = true;
		}else{
			yhyjs = new ArrayList<Yhyj>(idCache.values());
		}
		return yhyjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhyj> getAllByPage(int page,int size,Integer pageCount){
		List<Yhyj> yhyjs = getAll();
		yhyjs = SK_List.getPage(yhyjs, page, size, pageCount);
		return yhyjs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhyj> yhyjs){
		for(Yhyj yhyj : yhyjs){
			loadCache(yhyj);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhyj yhyj){
		idCache.put(SK_Plus.b(yhyj.getId()).e(),yhyj);
		Set<String> fjridset = fjridCache.get(String.valueOf(SK_Plus.b(yhyj.getFjrid()).e()));
		if(fjridset == null){
			fjridset = new HashSet<String>();
		}
		fjridset.add(String.valueOf(yhyj.getId()));
		fjridCache.put(SK_Plus.b(yhyj.getFjrid()).e(),fjridset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(yhyj.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		jsridset.add(String.valueOf(yhyj.getId()));
		jsridCache.put(SK_Plus.b(yhyj.getJsrid()).e(),jsridset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhyj yhyj,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhyj.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhyj.getId()).e());
		}
		Set<String> fjridset = fjridCache.get(String.valueOf(SK_Plus.b(yhyj.getFjrid()).e()));
		if(fjridset == null){
			fjridset = new HashSet<String>();
		}
		if (fjridset.contains(String.valueOf(yhyj.getId()))) {
			fjridset.remove(String.valueOf(yhyj.getId()));
		}
		fjridCache.put(SK_Plus.b(yhyj.getFjrid()).e(),fjridset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(yhyj.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		if (jsridset.contains(String.valueOf(yhyj.getId()))) {
			jsridset.remove(String.valueOf(yhyj.getId()));
		}
		jsridCache.put(SK_Plus.b(yhyj.getJsrid()).e(),jsridset);
		switch (saveType) {
		case 1:
			saveDatabase(yhyj);
			break;
		case 2:
			saveRedis(yhyj);
			break;
		case 3:
			saveAll(yhyj);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhyj> yhyjs,int clearType,int saveType){
		for(Yhyj yhyj : yhyjs){
			clearCache(yhyj,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhyjs);
			break;
		case 2:
			saveRedis(yhyjs);
			break;
		case 3:
			saveAll(yhyjs);
			break;
		default:
			break;
		}
	}
	
	public static Yhyj insert(Yhyj yhyj){
		return insert(yhyj,false);
    }
    
    public static Yhyj update(Yhyj yhyj){
    	return update(yhyj,false);
    }
    
    public static boolean delete(Yhyj yhyj){
    	return delete(yhyj,false);
    }
    
    private static Yhyj insert(Yhyj yhyj,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhyj = YhyjJedis.insert(yhyj);
    		LASTID.set(yhyj.getId());
    	}else{
    		int _id = yhyj.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhyj.getId() > id) {
				LASTID.set(yhyj.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhyj);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhyj)) {
				saveInsert.add(yhyj);
			}
		}
    	return yhyj;
    }
    
    private static Yhyj update(Yhyj yhyj,boolean isFlush){
    	clearCache(yhyj,1,0);
    	loadCache(yhyj);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhyj)) {
				saveUpdate.add(yhyj);
			}
		}else{
			saveUpdate.remove(yhyj);
		}
    	return yhyj;
    }
    
    private static boolean delete(Yhyj yhyj,boolean isFlush){
    	clearCache(yhyj,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhyj)) {
				saveUpdate.remove(yhyj);
				saveInsert.remove(yhyj);
				saveDelete.add(yhyj);
			}
    	}else{
    		saveUpdate.remove(yhyj);
			saveInsert.remove(yhyj);
			saveDelete.remove(yhyj);
    	}
    	return false;
    }
    
    public static Yhyj updateAndFlush(Yhyj yhyj){
    	update(yhyj,true);
    	return YhyjJedis.update(yhyj);
    }
    
    public static Yhyj insertAndFlush(Yhyj yhyj){
    	int id = LASTID.get();
    	insert(yhyj,true);
    	if(id > 0){
    		yhyj = YhyjJedis.insert(yhyj);
    	}
    	return yhyj;
    }
    
    public static boolean deleteAndFlush(Yhyj yhyj){
    	delete(yhyj,true);
    	return YhyjJedis.delete(yhyj);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhyjDao.deleteBatch(saveDelete);
    	YhyjDao.insertBatch(saveInsert);
    	YhyjDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhyj yhyj){
    	if (saveDelete.remove(yhyj))
			YhyjDao.delete(yhyj);
		if (saveInsert.remove(yhyj))
			YhyjDao.insert(yhyj);
		if (saveUpdate.remove(yhyj))
			YhyjDao.update(yhyj);
    }
    
    public static void saveDatabase(List<Yhyj> yhyjs){
    	if (saveDelete.removeAll(yhyjs))
			YhyjDao.deleteBatch(yhyjs);
		if (saveInsert.removeAll(yhyjs))
			YhyjDao.insertBatch(yhyjs);
		if (saveUpdate.removeAll(yhyjs))
			YhyjDao.updateBatch(yhyjs);
    }
    
    public static void saveRedis(){
    	YhyjJedis.clearCaches(saveDelete);
    	YhyjJedis.loadCaches(saveInsert);
    	YhyjJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhyj yhyj){
    	if (saveDelete.remove(yhyj))
			YhyjJedis.clearCache(yhyj);
		if (saveInsert.remove(yhyj))
			YhyjJedis.loadCache(yhyj);
		if (saveUpdate.remove(yhyj))
			YhyjJedis.loadCache(yhyj);
    }
    
    public static void saveRedis(List<Yhyj> yhyjs){
    	if (saveDelete.removeAll(yhyjs))
			YhyjJedis.clearCaches(yhyjs);
		if (saveInsert.removeAll(yhyjs))
			YhyjJedis.loadCaches(yhyjs);
		if (saveUpdate.removeAll(yhyjs))
			YhyjJedis.loadCaches(yhyjs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhyj yhyj){
   		saveDatabase(yhyj);
   		saveRedis(yhyj);
    }
    
    public static void saveAll(List<Yhyj> yhyjs){
   		saveDatabase(yhyjs);
   		saveRedis(yhyjs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhyj yhyj){
	
		loadCache(yhyj);
		
		YjfjCache.loadCachesCascade(YjfjCache.getByYhyj_id(yhyj.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhyj> yhyjs){
		for(Yhyj yhyj : yhyjs){
			loadCacheCascade(yhyj);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhyj yhyj,int saveType){
	
		clearCache(yhyj,3,saveType);
		
		YjfjCache.clearCachesCascade(YjfjCache.getByYhyj_id(yhyj.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhyj> yhyjs,int saveType){
		for(Yhyj yhyj : yhyjs){
			clearCacheCascade(yhyj,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhyjs);
			break;
		case 2:
			saveRedis(yhyjs);
			break;
		case 3:
			saveAll(yhyjs);
			break;
		default:
			break;
		}
	}
}