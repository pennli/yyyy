package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.db.bean.Yhyx;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.jedis.YhyxJedis;
import java.util.Set;
import com.sandking.db.dao.YhyxDao;
/**
 * 用户英雄
 */
public class YhyxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhyxCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yhyx> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yh_idCache = new ConcurrentHashMap<String, Set<String>>();
	static final Map<String, Set<String>> yxztidCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Yhyx> saveDelete = new CopyOnWriteArrayList<Yhyx>();
	
	static final List<Yhyx> saveInsert = new CopyOnWriteArrayList<Yhyx>();
	
	static final List<Yhyx> saveUpdate = new CopyOnWriteArrayList<Yhyx>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyx getById(int id){
		Yhyx yhyx = null;
		String key = SK_Plus.b(id).e();
		yhyx = idCache.get(key);
		
		if(yhyx==null){
			//查询数据库
			yhyx = YhyxJedis.getById(id);
			if(yhyx!=null){
				//加入缓存
				loadCache(yhyx);
			}
		}
		return yhyx;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhyx> getByYh_id(int yh_id){
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			Yhyx yhyx = null;
			for (String k : keys) {
				yhyx = getById(Integer.valueOf(k));
				if (yhyx == null) continue;
					yhyxs.add(yhyx);
			}
		}else{
			yhyxs = YhyxJedis.getByYh_id(yh_id);
			if(!yhyxs.isEmpty()){
				loadCaches(yhyxs);
			}
		}
		return yhyxs;
	}
	
	public static List<Yhyx> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhyx> yhyxs = getByYh_id(yh_id);
		yhyxs = SK_List.getPage(yhyxs, page, size, pageCount);
		return yhyxs;
	}
	/**
	 * 根据( 英雄状态id ) 查询
	 */
	public static List<Yhyx> getByYxztid(int yxztid){
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		String key = SK_Plus.b(yxztid).e();
		Set<String> keys = yxztidCache.get(key);
		if(keys != null){
			Yhyx yhyx = null;
			for (String k : keys) {
				yhyx = getById(Integer.valueOf(k));
				if (yhyx == null) continue;
					yhyxs.add(yhyx);
			}
		}else{
			yhyxs = YhyxJedis.getByYxztid(yxztid);
			if(!yhyxs.isEmpty()){
				loadCaches(yhyxs);
			}
		}
		return yhyxs;
	}
	
	public static List<Yhyx> getByPageYxztid(int yxztid,int page,int size,Integer pageCount){
		List<Yhyx> yhyxs = getByYxztid(yxztid);
		yhyxs = SK_List.getPage(yhyxs, page, size, pageCount);
		return yhyxs;
	}
	
	public static List<Yhyx> getCacheAll(){
		return new ArrayList<Yhyx>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhyx> getAll(){
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		if(!isLoadAll){
			yhyxs = YhyxJedis.getAll();
			loadCaches(yhyxs);
			isLoadAll = true;
		}else{
			yhyxs = new ArrayList<Yhyx>(idCache.values());
		}
		return yhyxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhyx> getAllByPage(int page,int size,Integer pageCount){
		List<Yhyx> yhyxs = getAll();
		yhyxs = SK_List.getPage(yhyxs, page, size, pageCount);
		return yhyxs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhyx> yhyxs){
		for(Yhyx yhyx : yhyxs){
			loadCache(yhyx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhyx yhyx){
		idCache.put(SK_Plus.b(yhyx.getId()).e(),yhyx);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhyx.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhyx.getId()));
		yh_idCache.put(SK_Plus.b(yhyx.getYh_id()).e(),yh_idset);
		Set<String> yxztidset = yxztidCache.get(String.valueOf(SK_Plus.b(yhyx.getYxztid()).e()));
		if(yxztidset == null){
			yxztidset = new HashSet<String>();
		}
		yxztidset.add(String.valueOf(yhyx.getId()));
		yxztidCache.put(SK_Plus.b(yhyx.getYxztid()).e(),yxztidset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yhyx yhyx,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yhyx.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yhyx.getId()).e());
		}
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhyx.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhyx.getId()))) {
			yh_idset.remove(String.valueOf(yhyx.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhyx.getYh_id()).e(),yh_idset);
		Set<String> yxztidset = yxztidCache.get(String.valueOf(SK_Plus.b(yhyx.getYxztid()).e()));
		if(yxztidset == null){
			yxztidset = new HashSet<String>();
		}
		if (yxztidset.contains(String.valueOf(yhyx.getId()))) {
			yxztidset.remove(String.valueOf(yhyx.getId()));
		}
		yxztidCache.put(SK_Plus.b(yhyx.getYxztid()).e(),yxztidset);
		switch (saveType) {
		case 1:
			saveDatabase(yhyx);
			break;
		case 2:
			saveRedis(yhyx);
			break;
		case 3:
			saveAll(yhyx);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yhyx> yhyxs,int clearType,int saveType){
		for(Yhyx yhyx : yhyxs){
			clearCache(yhyx,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhyxs);
			break;
		case 2:
			saveRedis(yhyxs);
			break;
		case 3:
			saveAll(yhyxs);
			break;
		default:
			break;
		}
	}
	
	public static Yhyx insert(Yhyx yhyx){
		return insert(yhyx,false);
    }
    
    public static Yhyx update(Yhyx yhyx){
    	return update(yhyx,false);
    }
    
    public static boolean delete(Yhyx yhyx){
    	return delete(yhyx,false);
    }
    
    private static Yhyx insert(Yhyx yhyx,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yhyx = YhyxJedis.insert(yhyx);
    		LASTID.set(yhyx.getId());
    	}else{
    		int _id = yhyx.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yhyx.getId() > id) {
				LASTID.set(yhyx.getId());
			}
    		//加入定时器
    	}
    	loadCache(yhyx);
    	if(!isFlush){
	    	if (!saveInsert.contains(yhyx)) {
				saveInsert.add(yhyx);
			}
		}
    	return yhyx;
    }
    
    private static Yhyx update(Yhyx yhyx,boolean isFlush){
    	clearCache(yhyx,1,0);
    	loadCache(yhyx);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yhyx)) {
				saveUpdate.add(yhyx);
			}
		}else{
			saveUpdate.remove(yhyx);
		}
    	return yhyx;
    }
    
    private static boolean delete(Yhyx yhyx,boolean isFlush){
    	clearCache(yhyx,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yhyx)) {
				saveUpdate.remove(yhyx);
				saveInsert.remove(yhyx);
				saveDelete.add(yhyx);
			}
    	}else{
    		saveUpdate.remove(yhyx);
			saveInsert.remove(yhyx);
			saveDelete.remove(yhyx);
    	}
    	return false;
    }
    
    public static Yhyx updateAndFlush(Yhyx yhyx){
    	update(yhyx,true);
    	return YhyxJedis.update(yhyx);
    }
    
    public static Yhyx insertAndFlush(Yhyx yhyx){
    	int id = LASTID.get();
    	insert(yhyx,true);
    	if(id > 0){
    		yhyx = YhyxJedis.insert(yhyx);
    	}
    	return yhyx;
    }
    
    public static boolean deleteAndFlush(Yhyx yhyx){
    	delete(yhyx,true);
    	return YhyxJedis.delete(yhyx);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YhyxDao.deleteBatch(saveDelete);
    	YhyxDao.insertBatch(saveInsert);
    	YhyxDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yhyx yhyx){
    	if (saveDelete.remove(yhyx))
			YhyxDao.delete(yhyx);
		if (saveInsert.remove(yhyx))
			YhyxDao.insert(yhyx);
		if (saveUpdate.remove(yhyx))
			YhyxDao.update(yhyx);
    }
    
    public static void saveDatabase(List<Yhyx> yhyxs){
    	if (saveDelete.removeAll(yhyxs))
			YhyxDao.deleteBatch(yhyxs);
		if (saveInsert.removeAll(yhyxs))
			YhyxDao.insertBatch(yhyxs);
		if (saveUpdate.removeAll(yhyxs))
			YhyxDao.updateBatch(yhyxs);
    }
    
    public static void saveRedis(){
    	YhyxJedis.clearCaches(saveDelete);
    	YhyxJedis.loadCaches(saveInsert);
    	YhyxJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yhyx yhyx){
    	if (saveDelete.remove(yhyx))
			YhyxJedis.clearCache(yhyx);
		if (saveInsert.remove(yhyx))
			YhyxJedis.loadCache(yhyx);
		if (saveUpdate.remove(yhyx))
			YhyxJedis.loadCache(yhyx);
    }
    
    public static void saveRedis(List<Yhyx> yhyxs){
    	if (saveDelete.removeAll(yhyxs))
			YhyxJedis.clearCaches(yhyxs);
		if (saveInsert.removeAll(yhyxs))
			YhyxJedis.loadCaches(yhyxs);
		if (saveUpdate.removeAll(yhyxs))
			YhyxJedis.loadCaches(yhyxs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yhyx yhyx){
   		saveDatabase(yhyx);
   		saveRedis(yhyx);
    }
    
    public static void saveAll(List<Yhyx> yhyxs){
   		saveDatabase(yhyxs);
   		saveRedis(yhyxs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yhyx yhyx){
	
		loadCache(yhyx);
		
		YxjnCache.loadCachesCascade(YxjnCache.getByYhyx_id(yhyx.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yhyx> yhyxs){
		for(Yhyx yhyx : yhyxs){
			loadCacheCascade(yhyx);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yhyx yhyx,int saveType){
	
		clearCache(yhyx,3,saveType);
		
		YxjnCache.clearCachesCascade(YxjnCache.getByYhyx_id(yhyx.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yhyx> yhyxs,int saveType){
		for(Yhyx yhyx : yhyxs){
			clearCacheCascade(yhyx,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yhyxs);
			break;
		case 2:
			saveRedis(yhyxs);
			break;
		case 3:
			saveAll(yhyxs);
			break;
		default:
			break;
		}
	}
}