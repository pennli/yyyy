package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.jedis.YjlxJedis;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.db.dao.YjlxDao;
import com.sandking.db.bean.Yjlx;
import com.sandking.tools.SK_List;
/**
 * 邮件类型
 */
public class YjlxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YjlxCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Yjlx> idCache = SK_Collections.newSortedMap();
	
	static final List<Yjlx> saveDelete = new CopyOnWriteArrayList<Yjlx>();
	
	static final List<Yjlx> saveInsert = new CopyOnWriteArrayList<Yjlx>();
	
	static final List<Yjlx> saveUpdate = new CopyOnWriteArrayList<Yjlx>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yjlx getById(int id){
		Yjlx yjlx = null;
		String key = SK_Plus.b(id).e();
		yjlx = idCache.get(key);
		
		if(yjlx==null){
			//查询数据库
			yjlx = YjlxJedis.getById(id);
			if(yjlx!=null){
				//加入缓存
				loadCache(yjlx);
			}
		}
		return yjlx;
	}
	
	
	public static List<Yjlx> getCacheAll(){
		return new ArrayList<Yjlx>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yjlx> getAll(){
		List<Yjlx> yjlxs = new ArrayList<Yjlx>();
		if(!isLoadAll){
			yjlxs = YjlxJedis.getAll();
			loadCaches(yjlxs);
			isLoadAll = true;
		}else{
			yjlxs = new ArrayList<Yjlx>(idCache.values());
		}
		return yjlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yjlx> getAllByPage(int page,int size,Integer pageCount){
		List<Yjlx> yjlxs = getAll();
		yjlxs = SK_List.getPage(yjlxs, page, size, pageCount);
		return yjlxs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yjlx> yjlxs){
		for(Yjlx yjlx : yjlxs){
			loadCache(yjlx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yjlx yjlx){
		idCache.put(SK_Plus.b(yjlx.getId()).e(),yjlx);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Yjlx yjlx,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(yjlx.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(yjlx.getId()).e());
		}
		switch (saveType) {
		case 1:
			saveDatabase(yjlx);
			break;
		case 2:
			saveRedis(yjlx);
			break;
		case 3:
			saveAll(yjlx);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Yjlx> yjlxs,int clearType,int saveType){
		for(Yjlx yjlx : yjlxs){
			clearCache(yjlx,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yjlxs);
			break;
		case 2:
			saveRedis(yjlxs);
			break;
		case 3:
			saveAll(yjlxs);
			break;
		default:
			break;
		}
	}
	
	public static Yjlx insert(Yjlx yjlx){
		return insert(yjlx,false);
    }
    
    public static Yjlx update(Yjlx yjlx){
    	return update(yjlx,false);
    }
    
    public static boolean delete(Yjlx yjlx){
    	return delete(yjlx,false);
    }
    
    private static Yjlx insert(Yjlx yjlx,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		yjlx = YjlxJedis.insert(yjlx);
    		LASTID.set(yjlx.getId());
    	}else{
    		int _id = yjlx.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (yjlx.getId() > id) {
				LASTID.set(yjlx.getId());
			}
    		//加入定时器
    	}
    	loadCache(yjlx);
    	if(!isFlush){
	    	if (!saveInsert.contains(yjlx)) {
				saveInsert.add(yjlx);
			}
		}
    	return yjlx;
    }
    
    private static Yjlx update(Yjlx yjlx,boolean isFlush){
    	clearCache(yjlx,1,0);
    	loadCache(yjlx);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(yjlx)) {
				saveUpdate.add(yjlx);
			}
		}else{
			saveUpdate.remove(yjlx);
		}
    	return yjlx;
    }
    
    private static boolean delete(Yjlx yjlx,boolean isFlush){
    	clearCache(yjlx,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(yjlx)) {
				saveUpdate.remove(yjlx);
				saveInsert.remove(yjlx);
				saveDelete.add(yjlx);
			}
    	}else{
    		saveUpdate.remove(yjlx);
			saveInsert.remove(yjlx);
			saveDelete.remove(yjlx);
    	}
    	return false;
    }
    
    public static Yjlx updateAndFlush(Yjlx yjlx){
    	update(yjlx,true);
    	return YjlxJedis.update(yjlx);
    }
    
    public static Yjlx insertAndFlush(Yjlx yjlx){
    	int id = LASTID.get();
    	insert(yjlx,true);
    	if(id > 0){
    		yjlx = YjlxJedis.insert(yjlx);
    	}
    	return yjlx;
    }
    
    public static boolean deleteAndFlush(Yjlx yjlx){
    	delete(yjlx,true);
    	return YjlxJedis.delete(yjlx);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	YjlxDao.deleteBatch(saveDelete);
    	YjlxDao.insertBatch(saveInsert);
    	YjlxDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Yjlx yjlx){
    	if (saveDelete.remove(yjlx))
			YjlxDao.delete(yjlx);
		if (saveInsert.remove(yjlx))
			YjlxDao.insert(yjlx);
		if (saveUpdate.remove(yjlx))
			YjlxDao.update(yjlx);
    }
    
    public static void saveDatabase(List<Yjlx> yjlxs){
    	if (saveDelete.removeAll(yjlxs))
			YjlxDao.deleteBatch(yjlxs);
		if (saveInsert.removeAll(yjlxs))
			YjlxDao.insertBatch(yjlxs);
		if (saveUpdate.removeAll(yjlxs))
			YjlxDao.updateBatch(yjlxs);
    }
    
    public static void saveRedis(){
    	YjlxJedis.clearCaches(saveDelete);
    	YjlxJedis.loadCaches(saveInsert);
    	YjlxJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Yjlx yjlx){
    	if (saveDelete.remove(yjlx))
			YjlxJedis.clearCache(yjlx);
		if (saveInsert.remove(yjlx))
			YjlxJedis.loadCache(yjlx);
		if (saveUpdate.remove(yjlx))
			YjlxJedis.loadCache(yjlx);
    }
    
    public static void saveRedis(List<Yjlx> yjlxs){
    	if (saveDelete.removeAll(yjlxs))
			YjlxJedis.clearCaches(yjlxs);
		if (saveInsert.removeAll(yjlxs))
			YjlxJedis.loadCaches(yjlxs);
		if (saveUpdate.removeAll(yjlxs))
			YjlxJedis.loadCaches(yjlxs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Yjlx yjlx){
   		saveDatabase(yjlx);
   		saveRedis(yjlx);
    }
    
    public static void saveAll(List<Yjlx> yjlxs){
   		saveDatabase(yjlxs);
   		saveRedis(yjlxs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Yjlx yjlx){
	
		loadCache(yjlx);
		
		YjfjCache.loadCachesCascade(YjfjCache.getByYjlx_id(yjlx.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Yjlx> yjlxs){
		for(Yjlx yjlx : yjlxs){
			loadCacheCascade(yjlx);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Yjlx yjlx,int saveType){
	
		clearCache(yjlx,3,saveType);
		
		YjfjCache.clearCachesCascade(YjfjCache.getByYjlx_id(yjlx.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Yjlx> yjlxs,int saveType){
		for(Yjlx yjlx : yjlxs){
			clearCacheCascade(yjlx,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(yjlxs);
			break;
		case 2:
			saveRedis(yjlxs);
			break;
		case 3:
			saveAll(yjlxs);
			break;
		default:
			break;
		}
	}
}