package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.dao.FjlxDao;
import com.sandking.db.bean.Fjlx;
import java.util.concurrent.CopyOnWriteArrayList;
import com.sandking.tools.SK_List;
import com.sandking.db.jedis.FjlxJedis;
/**
 * 附件类型
 */
public class FjlxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		FjlxCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Fjlx> idCache = SK_Collections.newSortedMap();
	
	static final List<Fjlx> saveDelete = new CopyOnWriteArrayList<Fjlx>();
	
	static final List<Fjlx> saveInsert = new CopyOnWriteArrayList<Fjlx>();
	
	static final List<Fjlx> saveUpdate = new CopyOnWriteArrayList<Fjlx>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Fjlx getById(int id){
		Fjlx fjlx = null;
		String key = SK_Plus.b(id).e();
		fjlx = idCache.get(key);
		
		if(fjlx==null){
			//查询数据库
			fjlx = FjlxJedis.getById(id);
			if(fjlx!=null){
				//加入缓存
				loadCache(fjlx);
			}
		}
		return fjlx;
	}
	
	
	public static List<Fjlx> getCacheAll(){
		return new ArrayList<Fjlx>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Fjlx> getAll(){
		List<Fjlx> fjlxs = new ArrayList<Fjlx>();
		if(!isLoadAll){
			fjlxs = FjlxJedis.getAll();
			loadCaches(fjlxs);
			isLoadAll = true;
		}else{
			fjlxs = new ArrayList<Fjlx>(idCache.values());
		}
		return fjlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Fjlx> getAllByPage(int page,int size,Integer pageCount){
		List<Fjlx> fjlxs = getAll();
		fjlxs = SK_List.getPage(fjlxs, page, size, pageCount);
		return fjlxs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Fjlx> fjlxs){
		for(Fjlx fjlx : fjlxs){
			loadCache(fjlx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Fjlx fjlx){
		idCache.put(SK_Plus.b(fjlx.getId()).e(),fjlx);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Fjlx fjlx,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(fjlx.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(fjlx.getId()).e());
		}
		switch (saveType) {
		case 1:
			saveDatabase(fjlx);
			break;
		case 2:
			saveRedis(fjlx);
			break;
		case 3:
			saveAll(fjlx);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Fjlx> fjlxs,int clearType,int saveType){
		for(Fjlx fjlx : fjlxs){
			clearCache(fjlx,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(fjlxs);
			break;
		case 2:
			saveRedis(fjlxs);
			break;
		case 3:
			saveAll(fjlxs);
			break;
		default:
			break;
		}
	}
	
	public static Fjlx insert(Fjlx fjlx){
		return insert(fjlx,false);
    }
    
    public static Fjlx update(Fjlx fjlx){
    	return update(fjlx,false);
    }
    
    public static boolean delete(Fjlx fjlx){
    	return delete(fjlx,false);
    }
    
    private static Fjlx insert(Fjlx fjlx,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		fjlx = FjlxJedis.insert(fjlx);
    		LASTID.set(fjlx.getId());
    	}else{
    		int _id = fjlx.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (fjlx.getId() > id) {
				LASTID.set(fjlx.getId());
			}
    		//加入定时器
    	}
    	loadCache(fjlx);
    	if(!isFlush){
	    	if (!saveInsert.contains(fjlx)) {
				saveInsert.add(fjlx);
			}
		}
    	return fjlx;
    }
    
    private static Fjlx update(Fjlx fjlx,boolean isFlush){
    	clearCache(fjlx,1,0);
    	loadCache(fjlx);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(fjlx)) {
				saveUpdate.add(fjlx);
			}
		}else{
			saveUpdate.remove(fjlx);
		}
    	return fjlx;
    }
    
    private static boolean delete(Fjlx fjlx,boolean isFlush){
    	clearCache(fjlx,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(fjlx)) {
				saveUpdate.remove(fjlx);
				saveInsert.remove(fjlx);
				saveDelete.add(fjlx);
			}
    	}else{
    		saveUpdate.remove(fjlx);
			saveInsert.remove(fjlx);
			saveDelete.remove(fjlx);
    	}
    	return false;
    }
    
    public static Fjlx updateAndFlush(Fjlx fjlx){
    	update(fjlx,true);
    	return FjlxJedis.update(fjlx);
    }
    
    public static Fjlx insertAndFlush(Fjlx fjlx){
    	int id = LASTID.get();
    	insert(fjlx,true);
    	if(id > 0){
    		fjlx = FjlxJedis.insert(fjlx);
    	}
    	return fjlx;
    }
    
    public static boolean deleteAndFlush(Fjlx fjlx){
    	delete(fjlx,true);
    	return FjlxJedis.delete(fjlx);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	FjlxDao.deleteBatch(saveDelete);
    	FjlxDao.insertBatch(saveInsert);
    	FjlxDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Fjlx fjlx){
    	if (saveDelete.remove(fjlx))
			FjlxDao.delete(fjlx);
		if (saveInsert.remove(fjlx))
			FjlxDao.insert(fjlx);
		if (saveUpdate.remove(fjlx))
			FjlxDao.update(fjlx);
    }
    
    public static void saveDatabase(List<Fjlx> fjlxs){
    	if (saveDelete.removeAll(fjlxs))
			FjlxDao.deleteBatch(fjlxs);
		if (saveInsert.removeAll(fjlxs))
			FjlxDao.insertBatch(fjlxs);
		if (saveUpdate.removeAll(fjlxs))
			FjlxDao.updateBatch(fjlxs);
    }
    
    public static void saveRedis(){
    	FjlxJedis.clearCaches(saveDelete);
    	FjlxJedis.loadCaches(saveInsert);
    	FjlxJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Fjlx fjlx){
    	if (saveDelete.remove(fjlx))
			FjlxJedis.clearCache(fjlx);
		if (saveInsert.remove(fjlx))
			FjlxJedis.loadCache(fjlx);
		if (saveUpdate.remove(fjlx))
			FjlxJedis.loadCache(fjlx);
    }
    
    public static void saveRedis(List<Fjlx> fjlxs){
    	if (saveDelete.removeAll(fjlxs))
			FjlxJedis.clearCaches(fjlxs);
		if (saveInsert.removeAll(fjlxs))
			FjlxJedis.loadCaches(fjlxs);
		if (saveUpdate.removeAll(fjlxs))
			FjlxJedis.loadCaches(fjlxs);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Fjlx fjlx){
   		saveDatabase(fjlx);
   		saveRedis(fjlx);
    }
    
    public static void saveAll(List<Fjlx> fjlxs){
   		saveDatabase(fjlxs);
   		saveRedis(fjlxs);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Fjlx fjlx){
	
		loadCache(fjlx);
		
		YjfjCache.loadCachesCascade(YjfjCache.getByFjlx_id(fjlx.getId()));
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Fjlx> fjlxs){
		for(Fjlx fjlx : fjlxs){
			loadCacheCascade(fjlx);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Fjlx fjlx,int saveType){
	
		clearCache(fjlx,3,saveType);
		
		YjfjCache.clearCachesCascade(YjfjCache.getByFjlx_id(fjlx.getId()),saveType);
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Fjlx> fjlxs,int saveType){
		for(Fjlx fjlx : fjlxs){
			clearCacheCascade(fjlx,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(fjlxs);
			break;
		case 2:
			saveRedis(fjlxs);
			break;
		case 3:
			saveAll(fjlxs);
			break;
		default:
			break;
		}
	}
}