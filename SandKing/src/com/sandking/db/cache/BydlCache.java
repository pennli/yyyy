package com.sandking.db.cache;

import com.sandking.db.jedis.BydlJedis;
import java.util.ArrayList;
import com.sandking.db.dao.BydlDao;
import java.util.Map;
import com.sandking.tools.SK_Collections;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.bean.Bydl;
import com.sandking.tools.SK_List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.HashSet;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.Set;
/**
 * 兵营队列
 */
public class BydlCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		BydlCache.isLoadAll = isLoadAll;
	}
	//缓存
	static final Map<String, Bydl> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yhjz_idCache = new ConcurrentHashMap<String, Set<String>>();
	
	static final List<Bydl> saveDelete = new CopyOnWriteArrayList<Bydl>();
	
	static final List<Bydl> saveInsert = new CopyOnWriteArrayList<Bydl>();
	
	static final List<Bydl> saveUpdate = new CopyOnWriteArrayList<Bydl>();
	
	
	/**
	 * 根据( id ) 查询
	 */
	public static Bydl getById(int id){
		Bydl bydl = null;
		String key = SK_Plus.b(id).e();
		bydl = idCache.get(key);
		
		if(bydl==null){
			//查询数据库
			bydl = BydlJedis.getById(id);
			if(bydl!=null){
				//加入缓存
				loadCache(bydl);
			}
		}
		return bydl;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Bydl> getByYhjz_id(int yhjz_id){
		List<Bydl> bydls = new ArrayList<Bydl>();
		String key = SK_Plus.b(yhjz_id).e();
		Set<String> keys = yhjz_idCache.get(key);
		if(keys != null){
			Bydl bydl = null;
			for (String k : keys) {
				bydl = getById(Integer.valueOf(k));
				if (bydl == null) continue;
					bydls.add(bydl);
			}
		}else{
			bydls = BydlJedis.getByYhjz_id(yhjz_id);
			if(!bydls.isEmpty()){
				loadCaches(bydls);
			}
		}
		return bydls;
	}
	
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,int page,int size,Integer pageCount){
		List<Bydl> bydls = getByYhjz_id(yhjz_id);
		bydls = SK_List.getPage(bydls, page, size, pageCount);
		return bydls;
	}
	
	public static List<Bydl> getCacheAll(){
		return new ArrayList<Bydl>(idCache.values());
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Bydl> getAll(){
		List<Bydl> bydls = new ArrayList<Bydl>();
		if(!isLoadAll){
			bydls = BydlJedis.getAll();
			loadCaches(bydls);
			isLoadAll = true;
		}else{
			bydls = new ArrayList<Bydl>(idCache.values());
		}
		return bydls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Bydl> getAllByPage(int page,int size,Integer pageCount){
		List<Bydl> bydls = getAll();
		bydls = SK_List.getPage(bydls, page, size, pageCount);
		return bydls;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Bydl> bydls){
		for(Bydl bydl : bydls){
			loadCache(bydl);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Bydl bydl){
		idCache.put(SK_Plus.b(bydl.getId()).e(),bydl);
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(bydl.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		yhjz_idset.add(String.valueOf(bydl.getId()));
		yhjz_idCache.put(SK_Plus.b(bydl.getYhjz_id()).e(),yhjz_idset);
	}
	
	
	
	/**
	 * 清空缓存 
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCache(Bydl bydl,int clearType,int saveType){
		if(clearType == 2){
			idCache.remove(SK_Plus.b(bydl.getId()).e());
			return;
		}
		if(clearType == 3){
			idCache.remove(SK_Plus.b(bydl.getId()).e());
		}
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(bydl.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		if (yhjz_idset.contains(String.valueOf(bydl.getId()))) {
			yhjz_idset.remove(String.valueOf(bydl.getId()));
		}
		yhjz_idCache.put(SK_Plus.b(bydl.getYhjz_id()).e(),yhjz_idset);
		switch (saveType) {
		case 1:
			saveDatabase(bydl);
			break;
		case 2:
			saveRedis(bydl);
			break;
		case 3:
			saveAll(bydl);
			break;
		default:
			break;
		}
	}
	
	/**
	 * 清空缓存
	 * clearType 1:只清空缓存关系  2:只清空对象   3.全部清空
	 * saveType  0:不保存 1：保存database 2:保存redis 3:保存全部(redis+database)
	 */
	public static void clearCaches(List<Bydl> bydls,int clearType,int saveType){
		for(Bydl bydl : bydls){
			clearCache(bydl,clearType,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(bydls);
			break;
		case 2:
			saveRedis(bydls);
			break;
		case 3:
			saveAll(bydls);
			break;
		default:
			break;
		}
	}
	
	public static Bydl insert(Bydl bydl){
		return insert(bydl,false);
    }
    
    public static Bydl update(Bydl bydl){
    	return update(bydl,false);
    }
    
    public static boolean delete(Bydl bydl){
    	return delete(bydl,false);
    }
    
    private static Bydl insert(Bydl bydl,boolean isFlush){
		int id = LASTID.get();
    	if(id < 1){
    		bydl = BydlJedis.insert(bydl);
    		LASTID.set(bydl.getId());
    	}else{
    		int _id = bydl.getId();
			if (_id < 1) {
				LASTID.set(LASTID.incrementAndGet());
			} else if (bydl.getId() > id) {
				LASTID.set(bydl.getId());
			}
    		//加入定时器
    	}
    	loadCache(bydl);
    	if(!isFlush){
	    	if (!saveInsert.contains(bydl)) {
				saveInsert.add(bydl);
			}
		}
    	return bydl;
    }
    
    private static Bydl update(Bydl bydl,boolean isFlush){
    	clearCache(bydl,1,0);
    	loadCache(bydl);
    	//加入定时器
    	if(!isFlush){
    		if (!saveUpdate.contains(bydl)) {
				saveUpdate.add(bydl);
			}
		}else{
			saveUpdate.remove(bydl);
		}
    	return bydl;
    }
    
    private static boolean delete(Bydl bydl,boolean isFlush){
    	clearCache(bydl,3,0);
    	//加入定时器
    	if(!isFlush){
    		if (!saveDelete.contains(bydl)) {
				saveUpdate.remove(bydl);
				saveInsert.remove(bydl);
				saveDelete.add(bydl);
			}
    	}else{
    		saveUpdate.remove(bydl);
			saveInsert.remove(bydl);
			saveDelete.remove(bydl);
    	}
    	return false;
    }
    
    public static Bydl updateAndFlush(Bydl bydl){
    	update(bydl,true);
    	return BydlJedis.update(bydl);
    }
    
    public static Bydl insertAndFlush(Bydl bydl){
    	int id = LASTID.get();
    	insert(bydl,true);
    	if(id > 0){
    		bydl = BydlJedis.insert(bydl);
    	}
    	return bydl;
    }
    
    public static boolean deleteAndFlush(Bydl bydl){
    	delete(bydl,true);
    	return BydlJedis.delete(bydl);
    }
    
    
    
    // ******************************** 持久化操作 ********************************
    public static void saveDatabase(){
    	BydlDao.deleteBatch(saveDelete);
    	BydlDao.insertBatch(saveInsert);
    	BydlDao.updateBatch(saveUpdate);
    	clearSave();
    }
    
    public static void saveDatabase(Bydl bydl){
    	if (saveDelete.remove(bydl))
			BydlDao.delete(bydl);
		if (saveInsert.remove(bydl))
			BydlDao.insert(bydl);
		if (saveUpdate.remove(bydl))
			BydlDao.update(bydl);
    }
    
    public static void saveDatabase(List<Bydl> bydls){
    	if (saveDelete.removeAll(bydls))
			BydlDao.deleteBatch(bydls);
		if (saveInsert.removeAll(bydls))
			BydlDao.insertBatch(bydls);
		if (saveUpdate.removeAll(bydls))
			BydlDao.updateBatch(bydls);
    }
    
    public static void saveRedis(){
    	BydlJedis.clearCaches(saveDelete);
    	BydlJedis.loadCaches(saveInsert);
    	BydlJedis.loadCaches(saveUpdate);
    	clearSave();
    }
    
    public static void saveRedis(Bydl bydl){
    	if (saveDelete.remove(bydl))
			BydlJedis.clearCache(bydl);
		if (saveInsert.remove(bydl))
			BydlJedis.loadCache(bydl);
		if (saveUpdate.remove(bydl))
			BydlJedis.loadCache(bydl);
    }
    
    public static void saveRedis(List<Bydl> bydls){
    	if (saveDelete.removeAll(bydls))
			BydlJedis.clearCaches(bydls);
		if (saveInsert.removeAll(bydls))
			BydlJedis.loadCaches(bydls);
		if (saveUpdate.removeAll(bydls))
			BydlJedis.loadCaches(bydls);
    }
    
    public static void saveAll(){
   		saveDatabase();
   		saveRedis();
   		clearSave();
    }
    
    public static void saveAll(Bydl bydl){
   		saveDatabase(bydl);
   		saveRedis(bydl);
    }
    
    public static void saveAll(List<Bydl> bydls){
   		saveDatabase(bydls);
   		saveRedis(bydls);
    }
    
    private static void clearSave(){
    	saveDelete.clear();
    	saveInsert.clear();
    	saveUpdate.clear();
    }
    
    // ******************************** 级联操作 ********************************
    
    /**
	 * 级联加载缓存
	 */
	public static void loadCacheCascade(Bydl bydl){
	
		loadCache(bydl);
		
	}
	
	/**
	 * 级联加载缓存
	 */
	public static void loadCachesCascade(List<Bydl> bydls){
		for(Bydl bydl : bydls){
			loadCacheCascade(bydl);
		}
	}
	
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCacheCascade(Bydl bydl,int saveType){
	
		clearCache(bydl,3,saveType);
		
	}
	
	/**
	 * 级联清除缓存
	 */
	public static void clearCachesCascade(List<Bydl> bydls,int saveType){
		for(Bydl bydl : bydls){
			clearCacheCascade(bydl,0);
		}
		switch (saveType) {
		case 1:
			saveDatabase(bydls);
			break;
		case 2:
			saveRedis(bydls);
			break;
		case 3:
			saveAll(bydls);
			break;
		default:
			break;
		}
	}
}