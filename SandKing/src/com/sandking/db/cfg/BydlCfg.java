package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.util.HashSet;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.util.Set;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 兵营队列
 */
public class BydlCfg {

	public static final String TABLENAME = "兵营队列";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		BydlCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, BydlCfg> idCache = SK_Collections.newSortedMap();
	static final Map<String, Set<String>> yhjz_idCache = SK_Collections.newMap();

	/** id */
	private int id;
	
	/** 用户建筑_id */
	private int yhjz_id;
	
	
	public BydlCfg() {
		super();
	}
	
	public BydlCfg(int id, int yhjz_id) {
		super();
		this.id = id;
		this.yhjz_id = yhjz_id;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public int getYhjz_id() {
		return yhjz_id;
	}
	
	public void setYhjz_id(int yhjz_id) {
		this.yhjz_id = yhjz_id;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<BydlCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<BydlCfg> bydlCfgs = new ArrayList<BydlCfg>();
		for (Map<String, Object> map : list) {
			bydlCfgs.add(createForColumnNameMap(map));
		}
		return bydlCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static BydlCfg createForColumnNameMap(Map<String, Object> map){
    	BydlCfg obj = new BydlCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhjz_id = SK_Map.getInt("用户建筑_id", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeInt(out,this.yhjz_id);
    }
    
     public static BydlCfg forStream(ByteArrayInputStream in) throws Exception {
     	BydlCfg bydlCfg = new BydlCfg();
	    bydlCfg.id = SK_InputStream.readInt(in,null);
	    bydlCfg.yhjz_id = SK_InputStream.readInt(in,null);
	    return bydlCfg;
     }
    
    public static List<BydlCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<BydlCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,BydlCfg.TABLENAME);
	}
	
	public static List<BydlCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<BydlCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<BydlCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户建筑_id FROM " + tableName;
		List<BydlCfg> bydlCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			bydlCfgs = BydlCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return bydlCfgs;
	}
	
	public static List<BydlCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<BydlCfg> bydlCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, bydlCfgs.size());
			for(BydlCfg bydlCfg : bydlCfgs){
				bydlCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(BydlCfg bydlCfg){
		idCache.put(SK_Plus.b(bydlCfg.getId()).e(),bydlCfg);
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(bydlCfg.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		yhjz_idset.add(String.valueOf(bydlCfg.getId()));
		yhjz_idCache.put(SK_Plus.b(bydlCfg.getYhjz_id()).e(),yhjz_idset);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static BydlCfg getById(int id){
		BydlCfg bydlCfg = null;
		String key = SK_Plus.b(id).e();
		bydlCfg = idCache.get(key);
		
		return bydlCfg;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<BydlCfg> getByYhjz_id(int yhjz_id){
		List<BydlCfg> bydlCfgs = new ArrayList<BydlCfg>();
		String key = SK_Plus.b(yhjz_id).e();
		Set<String> keys = yhjz_idCache.get(key);
		if(keys != null){
			BydlCfg bydlCfg = null;
			for (String k : keys) {
				bydlCfg = getById(Integer.valueOf(k));
				if (bydlCfg == null) continue;
					bydlCfgs.add(bydlCfg);
			}
		}
		return bydlCfgs;
	}
	
	public static List<BydlCfg> getByPageYhjz_id(int yhjz_id,int page,int size,Integer pageCount){
		List<BydlCfg> bydlCfgs = getByYhjz_id(yhjz_id);
		bydlCfgs = SK_List.getPage(bydlCfgs, page, size, pageCount);
		return bydlCfgs;
	}
	
	public static List<BydlCfg> getAll(){
		return new ArrayList<BydlCfg>(idCache.values());
	}
	
	public static List<BydlCfg> getAll(int page,int size,Integer pageCount){
		List<BydlCfg> bydlCfgs = getAll();
		bydlCfgs = SK_List.getPage(bydlCfgs, page, size, pageCount);
		return bydlCfgs;
	}
}