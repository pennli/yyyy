package com.sandking.db.cfg;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import javax.sql.DataSource;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import com.sandking.tools.SK_Collections;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.tools.SK_List;
import java.sql.Connection;
import java.lang.Exception;
import com.sandking.io.SK_InputStream;
import org.apache.commons.dbutils.DbUtils;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * 附件类型
 */
public class FjlxCfg {

	public static final String TABLENAME = "附件类型";
	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		FjlxCfg.isLoadAll = isLoadAll;
	}
	
	//缓存
	static final Map<String, FjlxCfg> idCache = SK_Collections.newSortedMap();

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	
	public FjlxCfg() {
		super();
	}
	
	public FjlxCfg(int id, String mc) {
		super();
		this.id = id;
		this.mc = mc;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
	}
	
	 /**
     * 根据list创建对象
     */
    public static List<FjlxCfg> createForColumnNameList(List<Map<String, Object>> list){
    	List<FjlxCfg> fjlxCfgs = new ArrayList<FjlxCfg>();
		for (Map<String, Object> map : list) {
			fjlxCfgs.add(createForColumnNameMap(map));
		}
		return fjlxCfgs;
    }
    
    /**
     * 根据map创建对象
     */
    public static FjlxCfg createForColumnNameMap(Map<String, Object> map){
    	FjlxCfg obj = new FjlxCfg();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
        return obj;
    }
    
    public void toStream(ByteArrayOutputStream out) throws Exception {
	    SK_OutputStream.writeInt(out,this.id);
	    SK_OutputStream.writeString(out,this.mc);
    }
    
     public static FjlxCfg forStream(ByteArrayInputStream in) throws Exception {
     	FjlxCfg fjlxCfg = new FjlxCfg();
	    fjlxCfg.id = SK_InputStream.readInt(in,null);
	    fjlxCfg.mc = SK_InputStream.readString(in,null);
	    return fjlxCfg;
     }
    
    public static List<FjlxCfg> loadDatabase(){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn);
	}
	
	public static List<FjlxCfg> loadDatabase(Connection conn){
		return loadDatabase(conn,FjlxCfg.TABLENAME);
	}
	
	public static List<FjlxCfg> loadDatabase(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<FjlxCfg> loadDatabase(String tableName){
		Connection conn = SK_Config.getConnection();
		return loadDatabase(conn,tableName);
	}
	
	public static List<FjlxCfg> loadDatabase(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName;
		List<FjlxCfg> fjlxCfgs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			fjlxCfgs = FjlxCfg.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return fjlxCfgs;
	}
	
	public static List<FjlxCfg> loadDatabase(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return loadDatabase(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static void writeFile(String path) throws Exception{
		try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
			List<FjlxCfg> fjlxCfgs = loadDatabase();
			SK_OutputStream.writeInt(out, fjlxCfgs.size());
			for(FjlxCfg fjlxCfg : fjlxCfgs){
				fjlxCfg.toStream(out);
			}
			FileUtils.writeByteArrayToFile(new File(path), out.toByteArray());
		}catch (Exception e) {
            throw e;
        }
	}
	
	public static void loadFile(String path) throws Exception{
		byte[] data = FileUtils.readFileToByteArray(new File(path));
		try (ByteArrayInputStream in = new ByteArrayInputStream(data);) {
			int size = SK_InputStream.readInt(in, null);
			for (int i = 0; i < size; i++) {
				loadCache(forStream(in));
			}
			isLoadAll = true;
		}catch (Exception e) {
            throw e;
        }
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(FjlxCfg fjlxCfg){
		idCache.put(SK_Plus.b(fjlxCfg.getId()).e(),fjlxCfg);
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static FjlxCfg getById(int id){
		FjlxCfg fjlxCfg = null;
		String key = SK_Plus.b(id).e();
		fjlxCfg = idCache.get(key);
		
		return fjlxCfg;
	}
	
	
	public static List<FjlxCfg> getAll(){
		return new ArrayList<FjlxCfg>(idCache.values());
	}
	
	public static List<FjlxCfg> getAll(int page,int size,Integer pageCount){
		List<FjlxCfg> fjlxCfgs = getAll();
		fjlxCfgs = SK_List.getPage(fjlxCfgs, page, size, pageCount);
		return fjlxCfgs;
	}
}