package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.YjfjDao;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import com.sandking.db.bean.Yjfj;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 邮件附件
 */
public class YjfjJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YjfjJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yjfj getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yjfj yjfj = null;
		try{
			String key = "Yjfj_Object";
			String field = SK_Plus.b("id:",id).e();
			yjfj = Yjfj.createForJson(jedis.hget(key,field));
			if(yjfj == null){
				yjfj = YjfjDao.getById(id);
				if(yjfj!=null){
					loadCache(yjfj);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yjfj;
	}
	
	/**
	 * 根据( 邮件类型_id ) 查询
	 */
	public static List<Yjfj> getByYjlx_id(int yjlx_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		try{
			String key = "Yjfj_Index";
			String field = SK_Plus.b(key,"yjlx_id:",yjlx_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yjfj_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yjfjs = Yjfj.createForJson(jsons);
			}else{
				yjfjs = YjfjDao.getByYjlx_id(yjlx_id);
				if(yjfjs!=null && !yjfjs.isEmpty()){
					loadCaches(yjfjs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yjfjs;
	}
	
	
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,int page,int size,Integer pageCount){
		List<Yjfj> yjfjs = getByYjlx_id(yjlx_id);
		yjfjs = SK_List.getPage(yjfjs, page, size, pageCount);
		return yjfjs;
	}
	
	/**
	 * 根据( 用户邮件_id ) 查询
	 */
	public static List<Yjfj> getByYhyj_id(int yhyj_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		try{
			String key = "Yjfj_Index";
			String field = SK_Plus.b(key,"yhyj_id:",yhyj_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yjfj_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yjfjs = Yjfj.createForJson(jsons);
			}else{
				yjfjs = YjfjDao.getByYhyj_id(yhyj_id);
				if(yjfjs!=null && !yjfjs.isEmpty()){
					loadCaches(yjfjs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yjfjs;
	}
	
	
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,int page,int size,Integer pageCount){
		List<Yjfj> yjfjs = getByYhyj_id(yhyj_id);
		yjfjs = SK_List.getPage(yjfjs, page, size, pageCount);
		return yjfjs;
	}
	
	/**
	 * 根据( 附件类型_id ) 查询
	 */
	public static List<Yjfj> getByFjlx_id(int fjlx_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		try{
			String key = "Yjfj_Index";
			String field = SK_Plus.b(key,"fjlx_id:",fjlx_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yjfj_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yjfjs = Yjfj.createForJson(jsons);
			}else{
				yjfjs = YjfjDao.getByFjlx_id(fjlx_id);
				if(yjfjs!=null && !yjfjs.isEmpty()){
					loadCaches(yjfjs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yjfjs;
	}
	
	
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,int page,int size,Integer pageCount){
		List<Yjfj> yjfjs = getByFjlx_id(fjlx_id);
		yjfjs = SK_List.getPage(yjfjs, page, size, pageCount);
		return yjfjs;
	}
	

	public static void loadCache(Yjfj yjfj){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yjfj,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yjfj yjfj,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yjfj_Object";
		String field = SK_Plus.b("id:",yjfj.getId()).e();
		String data = yjfj.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yjfj.getId());
		key = "Yjfj_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yjlx_id:",yjfj.getYjlx_id()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"yhyj_id:",yjfj.getYhyj_id()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"fjlx_id:",yjfj.getFjlx_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yjfj> yjfjs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yjfj yjfj : yjfjs){
				loadCache(yjfj,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yjfj yjfj){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yjfj,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yjfj yjfj,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yjfj_Object";
		String field = SK_Plus.b("id:",yjfj.getId()).e();
		p.hdel(key,field);
		
		key = "Yjfj_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yjfj.getId());
		field = SK_Plus.b(key,"yjlx_id:",yjfj.getYjlx_id()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"yhyj_id:",yjfj.getYhyj_id()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"fjlx_id:",yjfj.getFjlx_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yjfj> yjfjs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yjfj yjfj : yjfjs){
				clearCache(yjfj,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yjfj insert(Yjfj yjfj){
		yjfj = YjfjDao.insert(yjfj);
		if(yjfj!=null){
			loadCache(yjfj);
		}
    	return yjfj;
    }
    
    public static Yjfj update(Yjfj yjfj){
    	yjfj = YjfjDao.update(yjfj);
    	if(yjfj!=null){
    		clearCache(yjfj);
			loadCache(yjfj);
		}
    	return yjfj;
    }
    
    public static boolean delete(Yjfj yjfj){
    	boolean bool = YjfjDao.delete(yjfj);
    	if(bool){
    		clearCache(yjfj);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yjfj> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		try{
			String key = "Yjfj_Object";
			List<String> jsons = jedis.hvals(key);
			yjfjs = Yjfj.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yjfjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yjfj> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		try{
			if(!isLoadAll){
				yjfjs = YjfjDao.getAll();
				loadCaches(yjfjs);
				isLoadAll = true;
			}else{
				String key = "Yjfj_Object";
				List<String> jsons = jedis.hvals(key);
				yjfjs = Yjfj.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yjfjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yjfj> getAllByPage(int page,int size,Integer pageCount){
		List<Yjfj> yjfjs = getAll();
		yjfjs = SK_List.getPage(yjfjs, page, size, pageCount);
		return yjfjs;
	}
}