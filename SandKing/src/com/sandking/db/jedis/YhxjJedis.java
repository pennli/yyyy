package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.bean.Yhxj;
import com.sandking.db.dao.YhxjDao;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 用户陷阱
 */
public class YhxjJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhxjJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhxj getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhxj yhxj = null;
		try{
			String key = "Yhxj_Object";
			String field = SK_Plus.b("id:",id).e();
			yhxj = Yhxj.createForJson(jedis.hget(key,field));
			if(yhxj == null){
				yhxj = YhxjDao.getById(id);
				if(yhxj!=null){
					loadCache(yhxj);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhxj;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhxj> getByYh_id(int yh_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhxj> yhxjs = new ArrayList<Yhxj>();
		try{
			String key = "Yhxj_Index";
			String field = SK_Plus.b(key,"yh_id:",yh_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhxj_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhxjs = Yhxj.createForJson(jsons);
			}else{
				yhxjs = YhxjDao.getByYh_id(yh_id);
				if(yhxjs!=null && !yhxjs.isEmpty()){
					loadCaches(yhxjs);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhxjs;
	}
	
	
	public static List<Yhxj> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhxj> yhxjs = getByYh_id(yh_id);
		yhxjs = SK_List.getPage(yhxjs, page, size, pageCount);
		return yhxjs;
	}
	

	public static void loadCache(Yhxj yhxj){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhxj,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhxj yhxj,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhxj_Object";
		String field = SK_Plus.b("id:",yhxj.getId()).e();
		String data = yhxj.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yhxj.getId());
		key = "Yhxj_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yh_id:",yhxj.getYh_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhxj> yhxjs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhxj yhxj : yhxjs){
				loadCache(yhxj,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhxj yhxj){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhxj,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhxj yhxj,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhxj_Object";
		String field = SK_Plus.b("id:",yhxj.getId()).e();
		p.hdel(key,field);
		
		key = "Yhxj_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yhxj.getId());
		field = SK_Plus.b(key,"yh_id:",yhxj.getYh_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhxj> yhxjs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhxj yhxj : yhxjs){
				clearCache(yhxj,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhxj insert(Yhxj yhxj){
		yhxj = YhxjDao.insert(yhxj);
		if(yhxj!=null){
			loadCache(yhxj);
		}
    	return yhxj;
    }
    
    public static Yhxj update(Yhxj yhxj){
    	yhxj = YhxjDao.update(yhxj);
    	if(yhxj!=null){
    		clearCache(yhxj);
			loadCache(yhxj);
		}
    	return yhxj;
    }
    
    public static boolean delete(Yhxj yhxj){
    	boolean bool = YhxjDao.delete(yhxj);
    	if(bool){
    		clearCache(yhxj);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhxj> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhxj> yhxjs = new ArrayList<Yhxj>();
		try{
			String key = "Yhxj_Object";
			List<String> jsons = jedis.hvals(key);
			yhxjs = Yhxj.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhxjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhxj> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhxj> yhxjs = new ArrayList<Yhxj>();
		try{
			if(!isLoadAll){
				yhxjs = YhxjDao.getAll();
				loadCaches(yhxjs);
				isLoadAll = true;
			}else{
				String key = "Yhxj_Object";
				List<String> jsons = jedis.hvals(key);
				yhxjs = Yhxj.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhxjs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhxj> getAllByPage(int page,int size,Integer pageCount){
		List<Yhxj> yhxjs = getAll();
		yhxjs = SK_List.getPage(yhxjs, page, size, pageCount);
		return yhxjs;
	}
}