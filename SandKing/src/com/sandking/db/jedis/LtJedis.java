package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.bean.Lt;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.dao.LtDao;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 聊天
 */
public class LtJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		LtJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Lt getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Lt lt = null;
		try{
			String key = "Lt_Object";
			String field = SK_Plus.b("id:",id).e();
			lt = Lt.createForJson(jedis.hget(key,field));
			if(lt == null){
				lt = LtDao.getById(id);
				if(lt!=null){
					loadCache(lt);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return lt;
	}
	
	/**
	 * 根据( 聊天类型_id ) 查询
	 */
	public static List<Lt> getByLtlx_id(int ltlx_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lt> lts = new ArrayList<Lt>();
		try{
			String key = "Lt_Index";
			String field = SK_Plus.b(key,"ltlx_id:",ltlx_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Lt_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				lts = Lt.createForJson(jsons);
			}else{
				lts = LtDao.getByLtlx_id(ltlx_id);
				if(lts!=null && !lts.isEmpty()){
					loadCaches(lts);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lts;
	}
	
	
	public static List<Lt> getByPageLtlx_id(int ltlx_id,int page,int size,Integer pageCount){
		List<Lt> lts = getByLtlx_id(ltlx_id);
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Lt> getByLm_id(int lm_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lt> lts = new ArrayList<Lt>();
		try{
			String key = "Lt_Index";
			String field = SK_Plus.b(key,"lm_id:",lm_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Lt_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				lts = Lt.createForJson(jsons);
			}else{
				lts = LtDao.getByLm_id(lm_id);
				if(lts!=null && !lts.isEmpty()){
					loadCaches(lts);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lts;
	}
	
	
	public static List<Lt> getByPageLm_id(int lm_id,int page,int size,Integer pageCount){
		List<Lt> lts = getByLm_id(lm_id);
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Lt> getByJsrid(int jsrid){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lt> lts = new ArrayList<Lt>();
		try{
			String key = "Lt_Index";
			String field = SK_Plus.b(key,"jsrid:",jsrid).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Lt_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				lts = Lt.createForJson(jsons);
			}else{
				lts = LtDao.getByJsrid(jsrid);
				if(lts!=null && !lts.isEmpty()){
					loadCaches(lts);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lts;
	}
	
	
	public static List<Lt> getByPageJsrid(int jsrid,int page,int size,Integer pageCount){
		List<Lt> lts = getByJsrid(jsrid);
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	
	/**
	 * 根据( 发言人id ) 查询
	 */
	public static List<Lt> getByFyrid(int fyrid){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lt> lts = new ArrayList<Lt>();
		try{
			String key = "Lt_Index";
			String field = SK_Plus.b(key,"fyrid:",fyrid).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Lt_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				lts = Lt.createForJson(jsons);
			}else{
				lts = LtDao.getByFyrid(fyrid);
				if(lts!=null && !lts.isEmpty()){
					loadCaches(lts);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lts;
	}
	
	
	public static List<Lt> getByPageFyrid(int fyrid,int page,int size,Integer pageCount){
		List<Lt> lts = getByFyrid(fyrid);
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
	

	public static void loadCache(Lt lt){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(lt,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Lt lt,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Lt_Object";
		String field = SK_Plus.b("id:",lt.getId()).e();
		String data = lt.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(lt.getId());
		key = "Lt_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"ltlx_id:",lt.getLtlx_id()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"lm_id:",lt.getLm_id()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"jsrid:",lt.getJsrid()).e();
		p.sadd(field, primaryKey);
		
		field = SK_Plus.b(key,"fyrid:",lt.getFyrid()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Lt> lts){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Lt lt : lts){
				loadCache(lt,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Lt lt){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(lt,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Lt lt,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Lt_Object";
		String field = SK_Plus.b("id:",lt.getId()).e();
		p.hdel(key,field);
		
		key = "Lt_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(lt.getId());
		field = SK_Plus.b(key,"ltlx_id:",lt.getLtlx_id()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"lm_id:",lt.getLm_id()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"jsrid:",lt.getJsrid()).e();
		p.srem(field, primaryKey);
		
		field = SK_Plus.b(key,"fyrid:",lt.getFyrid()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Lt> lts){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Lt lt : lts){
				clearCache(lt,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Lt insert(Lt lt){
		lt = LtDao.insert(lt);
		if(lt!=null){
			loadCache(lt);
		}
    	return lt;
    }
    
    public static Lt update(Lt lt){
    	lt = LtDao.update(lt);
    	if(lt!=null){
    		clearCache(lt);
			loadCache(lt);
		}
    	return lt;
    }
    
    public static boolean delete(Lt lt){
    	boolean bool = LtDao.delete(lt);
    	if(bool){
    		clearCache(lt);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Lt> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lt> lts = new ArrayList<Lt>();
		try{
			String key = "Lt_Object";
			List<String> jsons = jedis.hvals(key);
			lts = Lt.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lts;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Lt> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Lt> lts = new ArrayList<Lt>();
		try{
			if(!isLoadAll){
				lts = LtDao.getAll();
				loadCaches(lts);
				isLoadAll = true;
			}else{
				String key = "Lt_Object";
				List<String> jsons = jedis.hvals(key);
				lts = Lt.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return lts;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Lt> getAllByPage(int page,int size,Integer pageCount){
		List<Lt> lts = getAll();
		lts = SK_List.getPage(lts, page, size, pageCount);
		return lts;
	}
}