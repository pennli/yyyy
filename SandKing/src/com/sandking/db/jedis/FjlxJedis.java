package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.dao.FjlxDao;
import com.sandking.db.bean.Fjlx;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 附件类型
 */
public class FjlxJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		FjlxJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Fjlx getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Fjlx fjlx = null;
		try{
			String key = "Fjlx_Object";
			String field = SK_Plus.b("id:",id).e();
			fjlx = Fjlx.createForJson(jedis.hget(key,field));
			if(fjlx == null){
				fjlx = FjlxDao.getById(id);
				if(fjlx!=null){
					loadCache(fjlx);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return fjlx;
	}
	

	public static void loadCache(Fjlx fjlx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(fjlx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Fjlx fjlx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Fjlx_Object";
		String field = SK_Plus.b("id:",fjlx.getId()).e();
		String data = fjlx.toJson();
		p.hset(key,field,data);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Fjlx> fjlxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Fjlx fjlx : fjlxs){
				loadCache(fjlx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Fjlx fjlx){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(fjlx,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Fjlx fjlx,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Fjlx_Object";
		String field = SK_Plus.b("id:",fjlx.getId()).e();
		p.hdel(key,field);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Fjlx> fjlxs){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Fjlx fjlx : fjlxs){
				clearCache(fjlx,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Fjlx insert(Fjlx fjlx){
		fjlx = FjlxDao.insert(fjlx);
		if(fjlx!=null){
			loadCache(fjlx);
		}
    	return fjlx;
    }
    
    public static Fjlx update(Fjlx fjlx){
    	fjlx = FjlxDao.update(fjlx);
    	if(fjlx!=null){
    		clearCache(fjlx);
			loadCache(fjlx);
		}
    	return fjlx;
    }
    
    public static boolean delete(Fjlx fjlx){
    	boolean bool = FjlxDao.delete(fjlx);
    	if(bool){
    		clearCache(fjlx);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Fjlx> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Fjlx> fjlxs = new ArrayList<Fjlx>();
		try{
			String key = "Fjlx_Object";
			List<String> jsons = jedis.hvals(key);
			fjlxs = Fjlx.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return fjlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Fjlx> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Fjlx> fjlxs = new ArrayList<Fjlx>();
		try{
			if(!isLoadAll){
				fjlxs = FjlxDao.getAll();
				loadCaches(fjlxs);
				isLoadAll = true;
			}else{
				String key = "Fjlx_Object";
				List<String> jsons = jedis.hvals(key);
				fjlxs = Fjlx.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return fjlxs;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Fjlx> getAllByPage(int page,int size,Integer pageCount){
		List<Fjlx> fjlxs = getAll();
		fjlxs = SK_List.getPage(fjlxs, page, size, pageCount);
		return fjlxs;
	}
}