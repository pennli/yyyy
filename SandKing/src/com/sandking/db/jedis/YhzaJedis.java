package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import com.sandking.db.bean.Yhza;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
import com.sandking.db.dao.YhzaDao;
/**
 * 用户障碍
 */
public class YhzaJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		YhzaJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhza getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Yhza yhza = null;
		try{
			String key = "Yhza_Object";
			String field = SK_Plus.b("id:",id).e();
			yhza = Yhza.createForJson(jedis.hget(key,field));
			if(yhza == null){
				yhza = YhzaDao.getById(id);
				if(yhza!=null){
					loadCache(yhza);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return yhza;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhza> getByYh_id(int yh_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhza> yhzas = new ArrayList<Yhza>();
		try{
			String key = "Yhza_Index";
			String field = SK_Plus.b(key,"yh_id:",yh_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Yhza_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				yhzas = Yhza.createForJson(jsons);
			}else{
				yhzas = YhzaDao.getByYh_id(yh_id);
				if(yhzas!=null && !yhzas.isEmpty()){
					loadCaches(yhzas);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhzas;
	}
	
	
	public static List<Yhza> getByPageYh_id(int yh_id,int page,int size,Integer pageCount){
		List<Yhza> yhzas = getByYh_id(yh_id);
		yhzas = SK_List.getPage(yhzas, page, size, pageCount);
		return yhzas;
	}
	

	public static void loadCache(Yhza yhza){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(yhza,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Yhza yhza,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhza_Object";
		String field = SK_Plus.b("id:",yhza.getId()).e();
		String data = yhza.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(yhza.getId());
		key = "Yhza_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yh_id:",yhza.getYh_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Yhza> yhzas){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhza yhza : yhzas){
				loadCache(yhza,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Yhza yhza){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(yhza,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Yhza yhza,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Yhza_Object";
		String field = SK_Plus.b("id:",yhza.getId()).e();
		p.hdel(key,field);
		
		key = "Yhza_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(yhza.getId());
		field = SK_Plus.b(key,"yh_id:",yhza.getYh_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Yhza> yhzas){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Yhza yhza : yhzas){
				clearCache(yhza,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Yhza insert(Yhza yhza){
		yhza = YhzaDao.insert(yhza);
		if(yhza!=null){
			loadCache(yhza);
		}
    	return yhza;
    }
    
    public static Yhza update(Yhza yhza){
    	yhza = YhzaDao.update(yhza);
    	if(yhza!=null){
    		clearCache(yhza);
			loadCache(yhza);
		}
    	return yhza;
    }
    
    public static boolean delete(Yhza yhza){
    	boolean bool = YhzaDao.delete(yhza);
    	if(bool){
    		clearCache(yhza);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Yhza> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhza> yhzas = new ArrayList<Yhza>();
		try{
			String key = "Yhza_Object";
			List<String> jsons = jedis.hvals(key);
			yhzas = Yhza.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhzas;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhza> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Yhza> yhzas = new ArrayList<Yhza>();
		try{
			if(!isLoadAll){
				yhzas = YhzaDao.getAll();
				loadCaches(yhzas);
				isLoadAll = true;
			}else{
				String key = "Yhza_Object";
				List<String> jsons = jedis.hvals(key);
				yhzas = Yhza.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return yhzas;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Yhza> getAllByPage(int page,int size,Integer pageCount){
		List<Yhza> yhzas = getAll();
		yhzas = SK_List.getPage(yhzas, page, size, pageCount);
		return yhzas;
	}
}