package com.sandking.db.jedis;

import java.util.ArrayList;
import com.sandking.db.dao.ZadlDao;
import com.sandking.tools.SK_Plus;
import com.sandking.tools.SK_Jedis;
import java.util.List;
import com.sandking.db.bean.Zadl;
import java.util.Set;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.Jedis;
import com.sandking.tools.SK_List;
/**
 * 障碍队列
 */
public class ZadlJedis{

	private static boolean isLoadAll = false;
	
	public static boolean isLoadAll() {
		return isLoadAll;
	}

	public static void setLoadAll(boolean isLoadAll) {
		ZadlJedis.isLoadAll = isLoadAll;
	}
	
	/**
	 * 根据( id ) 查询
	 */
	public static Zadl getById(int id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		Zadl zadl = null;
		try{
			String key = "Zadl_Object";
			String field = SK_Plus.b("id:",id).e();
			zadl = Zadl.createForJson(jedis.hget(key,field));
			if(zadl == null){
				zadl = ZadlDao.getById(id);
				if(zadl!=null){
					loadCache(zadl);
				}
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		
		return zadl;
	}
	
	/**
	 * 根据( 用户障碍_id ) 查询
	 */
	public static List<Zadl> getByYhza_id(int yhza_id){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Zadl> zadls = new ArrayList<Zadl>();
		try{
			String key = "Zadl_Index";
			String field = SK_Plus.b(key,"yhza_id:",yhza_id).e();
			Set<String> setStr = jedis.smembers(field);
			if(setStr!=null){
				key = "Zadl_Object";
				String[] fieldArray = (String[]) setStr.toArray();
				List<String> jsons = jedis.hmget(key,fieldArray);
				zadls = Zadl.createForJson(jsons);
			}else{
				zadls = ZadlDao.getByYhza_id(yhza_id);
				if(zadls!=null && !zadls.isEmpty()){
					loadCaches(zadls);
				}
			}
			
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return zadls;
	}
	
	
	public static List<Zadl> getByPageYhza_id(int yhza_id,int page,int size,Integer pageCount){
		List<Zadl> zadls = getByYhza_id(yhza_id);
		zadls = SK_List.getPage(zadls, page, size, pageCount);
		return zadls;
	}
	

	public static void loadCache(Zadl zadl){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			loadCache(zadl,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 加载一个缓存
	 */
	public static void loadCache(Zadl zadl,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Zadl_Object";
		String field = SK_Plus.b("id:",zadl.getId()).e();
		String data = zadl.toJson();
		p.hset(key,field,data);
		
		String primaryKey = String.valueOf(zadl.getId());
		key = "Zadl_Index";
		// ---------------------- 聚集索引 ----------------------
		field = SK_Plus.b(key,"yhza_id:",zadl.getYhza_id()).e();
		p.sadd(field, primaryKey);
		
	}
	
	/**
	 * 加载一组缓存
	 */
	public static void loadCaches(List<Zadl> zadls){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Zadl zadl : zadls){
				loadCache(zadl,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static void clearCache(Zadl zadl){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			clearCache(zadl,p);
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	/**
	 * 清空一个缓存
	 */
	public static void clearCache(Zadl zadl,Pipeline p){
		// ---------------------- 主键索引 ----------------------
		String key = "Zadl_Object";
		String field = SK_Plus.b("id:",zadl.getId()).e();
		p.hdel(key,field);
		
		key = "Zadl_Index";
		// ---------------------- 聚集索引 ----------------------
		String primaryKey = String.valueOf(zadl.getId());
		field = SK_Plus.b(key,"yhza_id:",zadl.getYhza_id()).e();
		p.srem(field, primaryKey);
		
	}
	/**
	 * 清空一组缓存
	 */
	public static void clearCaches(List<Zadl> zadls){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try{
			Pipeline p = jedis.pipelined();
			for(Zadl zadl : zadls){
				clearCache(zadl,p);
			}
			p.sync();
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
	}
	
	public static Zadl insert(Zadl zadl){
		zadl = ZadlDao.insert(zadl);
		if(zadl!=null){
			loadCache(zadl);
		}
    	return zadl;
    }
    
    public static Zadl update(Zadl zadl){
    	zadl = ZadlDao.update(zadl);
    	if(zadl!=null){
    		clearCache(zadl);
			loadCache(zadl);
		}
    	return zadl;
    }
    
    public static boolean delete(Zadl zadl){
    	boolean bool = ZadlDao.delete(zadl);
    	if(bool){
    		clearCache(zadl);
    	}
    	return bool;
    }
    
    /**
	 * 全部加载进内存(慎用)
	 */
    public static List<Zadl> getCacheAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Zadl> zadls = new ArrayList<Zadl>();
		try{
			String key = "Zadl_Object";
			List<String> jsons = jedis.hvals(key);
			zadls = Zadl.createForJson(jsons);
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return zadls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Zadl> getAll(){
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		List<Zadl> zadls = new ArrayList<Zadl>();
		try{
			if(!isLoadAll){
				zadls = ZadlDao.getAll();
				loadCaches(zadls);
				isLoadAll = true;
			}else{
				String key = "Zadl_Object";
				List<String> jsons = jedis.hvals(key);
				zadls = Zadl.createForJson(jsons);			
			}
		}catch (Exception e) {
			
		}finally{
			 SK_Jedis.getInstance().returnJedis(jedis);
		}
		return zadls;
	}
	
	/**
	 * 全部加载进内存(慎用)
	 */
	public static List<Zadl> getAllByPage(int page,int size,Integer pageCount){
		List<Zadl> zadls = getAll();
		zadls = SK_List.getPage(zadls, page, size, pageCount);
		return zadls;
	}
}