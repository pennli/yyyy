package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.db.bean.Yhyx;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
/**
 * 用户英雄
 */
public class YhyxDao {
	public static Yhyx insert(Yhyx yhyx){
		Connection conn = SK_Config.getConnection();
		return insert(yhyx,conn);
	}
	
	public static Yhyx insert(Yhyx yhyx,Connection conn){
		return insert(yhyx,conn,Yhyx.TABLENAME);
	}
	
	public static Yhyx insert(Yhyx yhyx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yhyx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhyx insert(Yhyx yhyx,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yhyx,conn,tableName);
	}
	
	public static Yhyx insert(Yhyx yhyx,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数) VALUES (?,?,?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yhyx.getId(),yhyx.getYh_id(),yhyx.getYxid(),yhyx.getYxlvl(),yhyx.getYxexp(),yhyx.getYxztid(),yhyx.getBs());
			if(yhyx.getId()==0){
				yhyx.setId(i);
			}
			return i > 0 ? yhyx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhyx insert(Yhyx yhyx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yhyx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yhyx> yhyxs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhyxs,conn);
	}
	
	public static int[] insertBatch(List<Yhyx> yhyxs,Connection conn){
		return insertBatch(yhyxs,conn,Yhyx.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yhyx> yhyxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhyxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yhyx> yhyxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhyxs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yhyx> yhyxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数) VALUES (?,?,?,?,?,?,?)";
		try {
			int columnSize = 7;
			int size = yhyxs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhyxs.get(i).getId();
				params[i][1] =yhyxs.get(i).getYh_id();
				params[i][2] =yhyxs.get(i).getYxid();
				params[i][3] =yhyxs.get(i).getYxlvl();
				params[i][4] =yhyxs.get(i).getYxexp();
				params[i][5] =yhyxs.get(i).getYxztid();
				params[i][6] =yhyxs.get(i).getBs();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yhyx> yhyxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhyxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yhyx update(Yhyx yhyx){
		Connection conn = SK_Config.getConnection();
		return update(yhyx,conn);
	}
	
	public static Yhyx update(Yhyx yhyx,Connection conn){
		return update(yhyx,conn,Yhyx.TABLENAME);
	}
	
	public static Yhyx update(Yhyx yhyx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yhyx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhyx update(Yhyx yhyx,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yhyx,conn,tableName);
	}
	
	public static Yhyx update(Yhyx yhyx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yhyx.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yhyx;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yhyx.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yhyx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yhyx.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhyx update(Yhyx yhyx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yhyx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhyx> yhyxs){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhyxs,conn);
	}
	
	public static int[] updateBatch(List<Yhyx> yhyxs,Connection conn){
		return updateBatch(yhyxs,conn,Yhyx.TABLENAME);
	}
	
	public static int[] updateBatch(List<Yhyx> yhyxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhyxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhyx> yhyxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhyxs,conn,tableName);
	}
	
	public static int[] updateBatch(List<Yhyx> yhyxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,用户_id = ?,英雄id = ?,英雄lvl = ?,英雄exp = ?,英雄状态id = ?,兵数 = ? WHERE id = ?";
		try {
			int columnSize = 7;
			int size = yhyxs.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhyxs.get(i).getId();
				params[i][1] =yhyxs.get(i).getYh_id();
				params[i][2] =yhyxs.get(i).getYxid();
				params[i][3] =yhyxs.get(i).getYxlvl();
				params[i][4] =yhyxs.get(i).getYxexp();
				params[i][5] =yhyxs.get(i).getYxztid();
				params[i][6] =yhyxs.get(i).getBs();
				params[i][columnSize] =yhyxs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Yhyx> yhyxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhyxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Yhyx yhyx){
		Connection conn = SK_Config.getConnection();
		return delete(yhyx,conn);
	}
	
	public static boolean delete(Yhyx yhyx,Connection conn){
		return delete(yhyx,conn,Yhyx.TABLENAME);
	}
	
	public static boolean delete(Yhyx yhyx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yhyx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yhyx yhyx,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yhyx,conn,tableName);
	}
	
	public static boolean delete(Yhyx yhyx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yhyx.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yhyx yhyx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yhyx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yhyx> yhyxs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhyxs,conn);
	}
	
	public static boolean deleteBatch(List<Yhyx> yhyxs,Connection conn){
		return deleteBatch(yhyxs,conn,Yhyx.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yhyx> yhyxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhyxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yhyx> yhyxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhyxs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yhyx> yhyxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhyxs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhyxs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yhyx> yhyxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhyxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yhyx getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yhyx getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhyx> getByYh_id(int yh_id){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn);
	}
	
	public static List<Yhyx> getByYh_id(int yh_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn,tableName);
	}
	
	public static List<Yhyx> getByPageYh_id(int yh_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,page,pageSize);
	}
	
	public static List<Yhyx> getByPageYh_id(int yh_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,tableName,page,pageSize);
	}
	/**
	 * 根据( 英雄状态id ) 查询
	 */
	public static List<Yhyx> getByYxztid(int yxztid){
		Connection conn = SK_Config.getConnection();
		return getByYxztid(yxztid, conn);
	}
	
	public static List<Yhyx> getByYxztid(int yxztid,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYxztid(yxztid, conn,tableName);
	}
	
	public static List<Yhyx> getByPageYxztid(int yxztid,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYxztid(yxztid, conn,page,pageSize);
	}
	
	public static List<Yhyx> getByPageYxztid(int yxztid,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYxztid(yxztid, conn,tableName,page,pageSize);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyx getById(int id,Connection conn){
		return getById(id,conn,Yhyx.TABLENAME);
	}
	
	public static Yhyx getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数 FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Yhyx yhyx = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yhyx = Yhyx.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyx;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhyx> getByYh_id(int yh_id,Connection conn){
		return getByYh_id(yh_id,conn,Yhyx.TABLENAME);
	}
	
	public static List<Yhyx> getByYh_id(int yh_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数 FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC";
		List<Yhyx> yhyxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhyxs = Yhyx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyxs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhyx> getByPageYh_id(int yh_id,Connection conn,int page,int pageSize){
		return getByPageYh_id(yh_id,conn,Yhyx.TABLENAME,page,pageSize);
	}
	
	public static List<Yhyx> getByPageYh_id(int yh_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数 FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhyx> yhyxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhyxs = Yhyx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyxs;
	}
	/**
	 * 根据( 英雄状态id ) 查询
	 */
	public static List<Yhyx> getByYxztid(int yxztid,Connection conn){
		return getByYxztid(yxztid,conn,Yhyx.TABLENAME);
	}
	
	public static List<Yhyx> getByYxztid(int yxztid,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数 FROM " + tableName + " WHERE " + "yxztid = ? ORDER BY id ASC";
		List<Yhyx> yhyxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yxztid);
			yhyxs = Yhyx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyxs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhyx> getByPageYxztid(int yxztid,Connection conn,int page,int pageSize){
		return getByPageYxztid(yxztid,conn,Yhyx.TABLENAME,page,pageSize);
	}
	
	public static List<Yhyx> getByPageYxztid(int yxztid,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数 FROM " + tableName + " WHERE " + "yxztid = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhyx> yhyxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yxztid);
			yhyxs = Yhyx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyxs;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyx getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhyx getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhyx> getByYh_id(int yh_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyx> getByYh_id(int yh_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhyx> getByPageYh_id(int yh_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyx> getByPageYh_id(int yh_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 英雄状态id ) 查询
	 */
	public static List<Yhyx> getByYxztid(int yxztid,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYxztid(yxztid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyx> getByYxztid(int yxztid,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYxztid(yxztid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhyx> getByPageYxztid(int yxztid,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYxztid(yxztid, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyx> getByPageYxztid(int yxztid,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYxztid(yxztid, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yhyx> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yhyx> getAll(Connection conn){
		return getAll(conn,Yhyx.TABLENAME);
	}
	
	public static List<Yhyx> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyx> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yhyx> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数 FROM " + tableName + " ORDER BY id ASC";
		List<Yhyx> yhyxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhyxs = Yhyx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyxs;
	}
	
	public static List<Yhyx> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yhyx> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Yhyx> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Yhyx.TABLENAME,page,pageSize);
	}
	
	public static List<Yhyx> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhyx> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Yhyx> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,用户_id,英雄id,英雄lvl,英雄exp,英雄状态id,兵数 FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhyx> yhyxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhyxs = Yhyx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhyxs;
	}
	
	public static List<Yhyx> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Yhyx.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Yhyx.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Yhyx.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `用户_id` INT(10) NOT NULL,");	
		plus.a("  `英雄id` INT(10) NOT NULL,");	
		plus.a("  `英雄lvl` INT(10) NOT NULL,");	
		plus.a("  `英雄exp` INT(10) NOT NULL,");	
		plus.a("  `英雄状态id` INT(10) NOT NULL,");	
		plus.a("  `兵数` INT(10) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}