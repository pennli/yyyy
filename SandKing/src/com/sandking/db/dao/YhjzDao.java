package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import com.sandking.db.bean.Yhjz;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
/**
 * 用户建筑
 */
public class YhjzDao {
	public static Yhjz insert(Yhjz yhjz){
		Connection conn = SK_Config.getConnection();
		return insert(yhjz,conn);
	}
	
	public static Yhjz insert(Yhjz yhjz,Connection conn){
		return insert(yhjz,conn,Yhjz.TABLENAME);
	}
	
	public static Yhjz insert(Yhjz yhjz,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yhjz,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhjz insert(Yhjz yhjz,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yhjz,conn,tableName);
	}
	
	public static Yhjz insert(Yhjz yhjz,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,建筑id,建筑lvl,x,y,用户_id) VALUES (?,?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yhjz.getId(),yhjz.getJzid(),yhjz.getJzlvl(),yhjz.getX(),yhjz.getY(),yhjz.getYh_id());
			if(yhjz.getId()==0){
				yhjz.setId(i);
			}
			return i > 0 ? yhjz : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhjz insert(Yhjz yhjz,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yhjz,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yhjz> yhjzs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhjzs,conn);
	}
	
	public static int[] insertBatch(List<Yhjz> yhjzs,Connection conn){
		return insertBatch(yhjzs,conn,Yhjz.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yhjz> yhjzs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhjzs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yhjz> yhjzs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhjzs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yhjz> yhjzs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,建筑id,建筑lvl,x,y,用户_id) VALUES (?,?,?,?,?,?)";
		try {
			int columnSize = 6;
			int size = yhjzs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhjzs.get(i).getId();
				params[i][1] =yhjzs.get(i).getJzid();
				params[i][2] =yhjzs.get(i).getJzlvl();
				params[i][3] =yhjzs.get(i).getX();
				params[i][4] =yhjzs.get(i).getY();
				params[i][5] =yhjzs.get(i).getYh_id();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yhjz> yhjzs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhjzs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yhjz update(Yhjz yhjz){
		Connection conn = SK_Config.getConnection();
		return update(yhjz,conn);
	}
	
	public static Yhjz update(Yhjz yhjz,Connection conn){
		return update(yhjz,conn,Yhjz.TABLENAME);
	}
	
	public static Yhjz update(Yhjz yhjz,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yhjz,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhjz update(Yhjz yhjz,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yhjz,conn,tableName);
	}
	
	public static Yhjz update(Yhjz yhjz,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yhjz.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yhjz;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yhjz.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yhjz : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yhjz.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhjz update(Yhjz yhjz,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yhjz,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhjz> yhjzs){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhjzs,conn);
	}
	
	public static int[] updateBatch(List<Yhjz> yhjzs,Connection conn){
		return updateBatch(yhjzs,conn,Yhjz.TABLENAME);
	}
	
	public static int[] updateBatch(List<Yhjz> yhjzs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhjzs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhjz> yhjzs,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhjzs,conn,tableName);
	}
	
	public static int[] updateBatch(List<Yhjz> yhjzs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,建筑id = ?,建筑lvl = ?,x = ?,y = ?,用户_id = ? WHERE id = ?";
		try {
			int columnSize = 6;
			int size = yhjzs.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhjzs.get(i).getId();
				params[i][1] =yhjzs.get(i).getJzid();
				params[i][2] =yhjzs.get(i).getJzlvl();
				params[i][3] =yhjzs.get(i).getX();
				params[i][4] =yhjzs.get(i).getY();
				params[i][5] =yhjzs.get(i).getYh_id();
				params[i][columnSize] =yhjzs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Yhjz> yhjzs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhjzs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Yhjz yhjz){
		Connection conn = SK_Config.getConnection();
		return delete(yhjz,conn);
	}
	
	public static boolean delete(Yhjz yhjz,Connection conn){
		return delete(yhjz,conn,Yhjz.TABLENAME);
	}
	
	public static boolean delete(Yhjz yhjz,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yhjz,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yhjz yhjz,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yhjz,conn,tableName);
	}
	
	public static boolean delete(Yhjz yhjz,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yhjz.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yhjz yhjz,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yhjz,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yhjz> yhjzs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhjzs,conn);
	}
	
	public static boolean deleteBatch(List<Yhjz> yhjzs,Connection conn){
		return deleteBatch(yhjzs,conn,Yhjz.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yhjz> yhjzs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhjzs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yhjz> yhjzs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhjzs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yhjz> yhjzs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhjzs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhjzs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yhjz> yhjzs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhjzs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yhjz getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yhjz getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhjz> getByYh_id(int yh_id){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn);
	}
	
	public static List<Yhjz> getByYh_id(int yh_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn,tableName);
	}
	
	public static List<Yhjz> getByPageYh_id(int yh_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,page,pageSize);
	}
	
	public static List<Yhjz> getByPageYh_id(int yh_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,tableName,page,pageSize);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yhjz getById(int id,Connection conn){
		return getById(id,conn,Yhjz.TABLENAME);
	}
	
	public static Yhjz getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,建筑id,建筑lvl,x,y,用户_id FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Yhjz yhjz = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yhjz = Yhjz.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhjz;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhjz> getByYh_id(int yh_id,Connection conn){
		return getByYh_id(yh_id,conn,Yhjz.TABLENAME);
	}
	
	public static List<Yhjz> getByYh_id(int yh_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,建筑id,建筑lvl,x,y,用户_id FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC";
		List<Yhjz> yhjzs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhjzs = Yhjz.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhjzs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhjz> getByPageYh_id(int yh_id,Connection conn,int page,int pageSize){
		return getByPageYh_id(yh_id,conn,Yhjz.TABLENAME,page,pageSize);
	}
	
	public static List<Yhjz> getByPageYh_id(int yh_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,建筑id,建筑lvl,x,y,用户_id FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhjz> yhjzs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhjzs = Yhjz.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhjzs;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yhjz getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhjz getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhjz> getByYh_id(int yh_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhjz> getByYh_id(int yh_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhjz> getByPageYh_id(int yh_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhjz> getByPageYh_id(int yh_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yhjz> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yhjz> getAll(Connection conn){
		return getAll(conn,Yhjz.TABLENAME);
	}
	
	public static List<Yhjz> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhjz> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yhjz> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,建筑id,建筑lvl,x,y,用户_id FROM " + tableName + " ORDER BY id ASC";
		List<Yhjz> yhjzs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhjzs = Yhjz.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhjzs;
	}
	
	public static List<Yhjz> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yhjz> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Yhjz> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Yhjz.TABLENAME,page,pageSize);
	}
	
	public static List<Yhjz> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhjz> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Yhjz> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,建筑id,建筑lvl,x,y,用户_id FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhjz> yhjzs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhjzs = Yhjz.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhjzs;
	}
	
	public static List<Yhjz> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Yhjz.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Yhjz.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Yhjz.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `建筑id` INT(10) NOT NULL,");	
		plus.a("  `建筑lvl` INT(10) NOT NULL,");	
		plus.a("  `x` INT(10) NOT NULL,");	
		plus.a("  `y` INT(10) NOT NULL,");	
		plus.a("  `用户_id` INT(10) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}