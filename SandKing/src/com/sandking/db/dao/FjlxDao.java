package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import com.sandking.db.bean.Fjlx;
import org.apache.commons.dbutils.handlers.MapListHandler;
/**
 * 附件类型
 */
public class FjlxDao {
	public static Fjlx insert(Fjlx fjlx){
		Connection conn = SK_Config.getConnection();
		return insert(fjlx,conn);
	}
	
	public static Fjlx insert(Fjlx fjlx,Connection conn){
		return insert(fjlx,conn,Fjlx.TABLENAME);
	}
	
	public static Fjlx insert(Fjlx fjlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(fjlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Fjlx insert(Fjlx fjlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(fjlx,conn,tableName);
	}
	
	public static Fjlx insert(Fjlx fjlx,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int i = (int)sq.insert(conn,sql,fjlx.getId(),fjlx.getMc());
			if(fjlx.getId()==0){
				fjlx.setId(i);
			}
			return i > 0 ? fjlx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Fjlx insert(Fjlx fjlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(fjlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Fjlx> fjlxs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(fjlxs,conn);
	}
	
	public static int[] insertBatch(List<Fjlx> fjlxs,Connection conn){
		return insertBatch(fjlxs,conn,Fjlx.TABLENAME);
	}
	
	public static int[] insertBatch(List<Fjlx> fjlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(fjlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Fjlx> fjlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(fjlxs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Fjlx> fjlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int columnSize = 2;
			int size = fjlxs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =fjlxs.get(i).getId();
				params[i][1] =fjlxs.get(i).getMc();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Fjlx> fjlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(fjlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Fjlx update(Fjlx fjlx){
		Connection conn = SK_Config.getConnection();
		return update(fjlx,conn);
	}
	
	public static Fjlx update(Fjlx fjlx,Connection conn){
		return update(fjlx,conn,Fjlx.TABLENAME);
	}
	
	public static Fjlx update(Fjlx fjlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(fjlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Fjlx update(Fjlx fjlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(fjlx,conn,tableName);
	}
	
	public static Fjlx update(Fjlx fjlx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = fjlx.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return fjlx;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = fjlx.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? fjlx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				fjlx.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Fjlx update(Fjlx fjlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(fjlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Fjlx> fjlxs){
		Connection conn = SK_Config.getConnection();
		return updateBatch(fjlxs,conn);
	}
	
	public static int[] updateBatch(List<Fjlx> fjlxs,Connection conn){
		return updateBatch(fjlxs,conn,Fjlx.TABLENAME);
	}
	
	public static int[] updateBatch(List<Fjlx> fjlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(fjlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Fjlx> fjlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(fjlxs,conn,tableName);
	}
	
	public static int[] updateBatch(List<Fjlx> fjlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,名称 = ? WHERE id = ?";
		try {
			int columnSize = 2;
			int size = fjlxs.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =fjlxs.get(i).getId();
				params[i][1] =fjlxs.get(i).getMc();
				params[i][columnSize] =fjlxs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Fjlx> fjlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(fjlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Fjlx fjlx){
		Connection conn = SK_Config.getConnection();
		return delete(fjlx,conn);
	}
	
	public static boolean delete(Fjlx fjlx,Connection conn){
		return delete(fjlx,conn,Fjlx.TABLENAME);
	}
	
	public static boolean delete(Fjlx fjlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(fjlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Fjlx fjlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(fjlx,conn,tableName);
	}
	
	public static boolean delete(Fjlx fjlx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, fjlx.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Fjlx fjlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(fjlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Fjlx> fjlxs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(fjlxs,conn);
	}
	
	public static boolean deleteBatch(List<Fjlx> fjlxs,Connection conn){
		return deleteBatch(fjlxs,conn,Fjlx.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Fjlx> fjlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(fjlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Fjlx> fjlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(fjlxs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Fjlx> fjlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = fjlxs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = fjlxs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Fjlx> fjlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(fjlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Fjlx getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Fjlx getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Fjlx getById(int id,Connection conn){
		return getById(id,conn,Fjlx.TABLENAME);
	}
	
	public static Fjlx getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Fjlx fjlx = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			fjlx = Fjlx.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return fjlx;
	}
	
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Fjlx getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Fjlx getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public static List<Fjlx> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Fjlx> getAll(Connection conn){
		return getAll(conn,Fjlx.TABLENAME);
	}
	
	public static List<Fjlx> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Fjlx> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Fjlx> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName + " ORDER BY id ASC";
		List<Fjlx> fjlxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			fjlxs = Fjlx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return fjlxs;
	}
	
	public static List<Fjlx> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Fjlx> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Fjlx> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Fjlx.TABLENAME,page,pageSize);
	}
	
	public static List<Fjlx> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Fjlx> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Fjlx> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,名称 FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Fjlx> fjlxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			fjlxs = Fjlx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return fjlxs;
	}
	
	public static List<Fjlx> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Fjlx.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Fjlx.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Fjlx.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `名称` VARCHAR(12) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}