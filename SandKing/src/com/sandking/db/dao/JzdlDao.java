package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.db.bean.Jzdl;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
/**
 * 建筑队列
 */
public class JzdlDao {
	public static Jzdl insert(Jzdl jzdl){
		Connection conn = SK_Config.getConnection();
		return insert(jzdl,conn);
	}
	
	public static Jzdl insert(Jzdl jzdl,Connection conn){
		return insert(jzdl,conn,Jzdl.TABLENAME);
	}
	
	public static Jzdl insert(Jzdl jzdl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(jzdl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Jzdl insert(Jzdl jzdl,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(jzdl,conn,tableName);
	}
	
	public static Jzdl insert(Jzdl jzdl,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,用户建筑_id) VALUES (?,?)";
		try {
			int i = (int)sq.insert(conn,sql,jzdl.getId(),jzdl.getYhjz_id());
			if(jzdl.getId()==0){
				jzdl.setId(i);
			}
			return i > 0 ? jzdl : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Jzdl insert(Jzdl jzdl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(jzdl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Jzdl> jzdls){
		Connection conn = SK_Config.getConnection();
		return insertBatch(jzdls,conn);
	}
	
	public static int[] insertBatch(List<Jzdl> jzdls,Connection conn){
		return insertBatch(jzdls,conn,Jzdl.TABLENAME);
	}
	
	public static int[] insertBatch(List<Jzdl> jzdls,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(jzdls,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Jzdl> jzdls,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(jzdls,conn,tableName);
	}
	
	public static int[] insertBatch(List<Jzdl> jzdls,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,用户建筑_id) VALUES (?,?)";
		try {
			int columnSize = 2;
			int size = jzdls.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =jzdls.get(i).getId();
				params[i][1] =jzdls.get(i).getYhjz_id();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Jzdl> jzdls,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(jzdls,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Jzdl update(Jzdl jzdl){
		Connection conn = SK_Config.getConnection();
		return update(jzdl,conn);
	}
	
	public static Jzdl update(Jzdl jzdl,Connection conn){
		return update(jzdl,conn,Jzdl.TABLENAME);
	}
	
	public static Jzdl update(Jzdl jzdl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(jzdl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Jzdl update(Jzdl jzdl,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(jzdl,conn,tableName);
	}
	
	public static Jzdl update(Jzdl jzdl,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = jzdl.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return jzdl;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = jzdl.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? jzdl : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				jzdl.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Jzdl update(Jzdl jzdl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(jzdl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Jzdl> jzdls){
		Connection conn = SK_Config.getConnection();
		return updateBatch(jzdls,conn);
	}
	
	public static int[] updateBatch(List<Jzdl> jzdls,Connection conn){
		return updateBatch(jzdls,conn,Jzdl.TABLENAME);
	}
	
	public static int[] updateBatch(List<Jzdl> jzdls,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(jzdls,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Jzdl> jzdls,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(jzdls,conn,tableName);
	}
	
	public static int[] updateBatch(List<Jzdl> jzdls,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,用户建筑_id = ? WHERE id = ?";
		try {
			int columnSize = 2;
			int size = jzdls.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =jzdls.get(i).getId();
				params[i][1] =jzdls.get(i).getYhjz_id();
				params[i][columnSize] =jzdls.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Jzdl> jzdls,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(jzdls,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Jzdl jzdl){
		Connection conn = SK_Config.getConnection();
		return delete(jzdl,conn);
	}
	
	public static boolean delete(Jzdl jzdl,Connection conn){
		return delete(jzdl,conn,Jzdl.TABLENAME);
	}
	
	public static boolean delete(Jzdl jzdl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(jzdl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Jzdl jzdl,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(jzdl,conn,tableName);
	}
	
	public static boolean delete(Jzdl jzdl,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, jzdl.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Jzdl jzdl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(jzdl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Jzdl> jzdls){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(jzdls,conn);
	}
	
	public static boolean deleteBatch(List<Jzdl> jzdls,Connection conn){
		return deleteBatch(jzdls,conn,Jzdl.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Jzdl> jzdls,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(jzdls,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Jzdl> jzdls,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(jzdls,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Jzdl> jzdls,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = jzdls.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = jzdls.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Jzdl> jzdls,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(jzdls,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Jzdl getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Jzdl getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Jzdl> getByYhjz_id(int yhjz_id){
		Connection conn = SK_Config.getConnection();
		return getByYhjz_id(yhjz_id, conn);
	}
	
	public static List<Jzdl> getByYhjz_id(int yhjz_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYhjz_id(yhjz_id, conn,tableName);
	}
	
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYhjz_id(yhjz_id, conn,page,pageSize);
	}
	
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYhjz_id(yhjz_id, conn,tableName,page,pageSize);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Jzdl getById(int id,Connection conn){
		return getById(id,conn,Jzdl.TABLENAME);
	}
	
	public static Jzdl getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Jzdl jzdl = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			jzdl = Jzdl.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return jzdl;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Jzdl> getByYhjz_id(int yhjz_id,Connection conn){
		return getByYhjz_id(yhjz_id,conn,Jzdl.TABLENAME);
	}
	
	public static List<Jzdl> getByYhjz_id(int yhjz_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " WHERE " + "yhjz_id = ? ORDER BY id ASC";
		List<Jzdl> jzdls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhjz_id);
			jzdls = Jzdl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return jzdls;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,Connection conn,int page,int pageSize){
		return getByPageYhjz_id(yhjz_id,conn,Jzdl.TABLENAME,page,pageSize);
	}
	
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " WHERE " + "yhjz_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Jzdl> jzdls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhjz_id);
			jzdls = Jzdl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return jzdls;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Jzdl getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Jzdl getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Jzdl> getByYhjz_id(int yhjz_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYhjz_id(yhjz_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Jzdl> getByYhjz_id(int yhjz_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYhjz_id(yhjz_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhjz_id(yhjz_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhjz_id(yhjz_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Jzdl> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Jzdl> getAll(Connection conn){
		return getAll(conn,Jzdl.TABLENAME);
	}
	
	public static List<Jzdl> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Jzdl> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Jzdl> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " ORDER BY id ASC";
		List<Jzdl> jzdls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			jzdls = Jzdl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return jzdls;
	}
	
	public static List<Jzdl> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Jzdl> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Jzdl> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Jzdl.TABLENAME,page,pageSize);
	}
	
	public static List<Jzdl> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Jzdl> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Jzdl> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Jzdl> jzdls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			jzdls = Jzdl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return jzdls;
	}
	
	public static List<Jzdl> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Jzdl.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Jzdl.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Jzdl.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `用户建筑_id` INT(10) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}