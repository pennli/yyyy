package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import java.util.List;
import com.sandking.db.bean.Yhbb;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
/**
 * 用户背包
 */
public class YhbbDao {
	public static Yhbb insert(Yhbb yhbb){
		Connection conn = SK_Config.getConnection();
		return insert(yhbb,conn);
	}
	
	public static Yhbb insert(Yhbb yhbb,Connection conn){
		return insert(yhbb,conn,Yhbb.TABLENAME);
	}
	
	public static Yhbb insert(Yhbb yhbb,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yhbb,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhbb insert(Yhbb yhbb,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yhbb,conn,tableName);
	}
	
	public static Yhbb insert(Yhbb yhbb,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,用户_id,道具id,数量) VALUES (?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yhbb.getId(),yhbb.getYh_id(),yhbb.getDjid(),yhbb.getSl());
			if(yhbb.getId()==0){
				yhbb.setId(i);
			}
			return i > 0 ? yhbb : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhbb insert(Yhbb yhbb,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yhbb,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yhbb> yhbbs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhbbs,conn);
	}
	
	public static int[] insertBatch(List<Yhbb> yhbbs,Connection conn){
		return insertBatch(yhbbs,conn,Yhbb.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yhbb> yhbbs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhbbs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yhbb> yhbbs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhbbs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yhbb> yhbbs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,用户_id,道具id,数量) VALUES (?,?,?,?)";
		try {
			int columnSize = 4;
			int size = yhbbs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhbbs.get(i).getId();
				params[i][1] =yhbbs.get(i).getYh_id();
				params[i][2] =yhbbs.get(i).getDjid();
				params[i][3] =yhbbs.get(i).getSl();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yhbb> yhbbs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhbbs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yhbb update(Yhbb yhbb){
		Connection conn = SK_Config.getConnection();
		return update(yhbb,conn);
	}
	
	public static Yhbb update(Yhbb yhbb,Connection conn){
		return update(yhbb,conn,Yhbb.TABLENAME);
	}
	
	public static Yhbb update(Yhbb yhbb,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yhbb,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhbb update(Yhbb yhbb,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yhbb,conn,tableName);
	}
	
	public static Yhbb update(Yhbb yhbb,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yhbb.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yhbb;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yhbb.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yhbb : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yhbb.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhbb update(Yhbb yhbb,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yhbb,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhbb> yhbbs){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhbbs,conn);
	}
	
	public static int[] updateBatch(List<Yhbb> yhbbs,Connection conn){
		return updateBatch(yhbbs,conn,Yhbb.TABLENAME);
	}
	
	public static int[] updateBatch(List<Yhbb> yhbbs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhbbs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] updateBatch(List<Yhbb> yhbbs,String tableName){
		Connection conn = SK_Config.getConnection();
		return updateBatch(yhbbs,conn,tableName);
	}
	
	public static int[] updateBatch(List<Yhbb> yhbbs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "UPDATE " +tableName+ " SET id = ?,用户_id = ?,道具id = ?,数量 = ? WHERE id = ?";
		try {
			int columnSize = 4;
			int size = yhbbs.size();
			Object[][] params = new Object[size][columnSize + 1];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhbbs.get(i).getId();
				params[i][1] =yhbbs.get(i).getYh_id();
				params[i][2] =yhbbs.get(i).getDjid();
				params[i][3] =yhbbs.get(i).getSl();
				params[i][columnSize] =yhbbs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] updateBatch(List<Yhbb> yhbbs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return updateBatch(yhbbs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean delete(Yhbb yhbb){
		Connection conn = SK_Config.getConnection();
		return delete(yhbb,conn);
	}
	
	public static boolean delete(Yhbb yhbb,Connection conn){
		return delete(yhbb,conn,Yhbb.TABLENAME);
	}
	
	public static boolean delete(Yhbb yhbb,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yhbb,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yhbb yhbb,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yhbb,conn,tableName);
	}
	
	public static boolean delete(Yhbb yhbb,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yhbb.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yhbb yhbb,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yhbb,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yhbb> yhbbs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhbbs,conn);
	}
	
	public static boolean deleteBatch(List<Yhbb> yhbbs,Connection conn){
		return deleteBatch(yhbbs,conn,Yhbb.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yhbb> yhbbs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhbbs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yhbb> yhbbs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhbbs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yhbb> yhbbs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhbbs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhbbs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yhbb> yhbbs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhbbs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yhbb getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yhbb getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhbb> getByYh_id(int yh_id){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn);
	}
	
	public static List<Yhbb> getByYh_id(int yh_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn,tableName);
	}
	
	public static List<Yhbb> getByPageYh_id(int yh_id,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,page,pageSize);
	}
	
	public static List<Yhbb> getByPageYh_id(int yh_id,String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,tableName,page,pageSize);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yhbb getById(int id,Connection conn){
		return getById(id,conn,Yhbb.TABLENAME);
	}
	
	public static Yhbb getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,道具id,数量 FROM " + tableName + " WHERE " + "id = ? ORDER BY id ASC";
		Yhbb yhbb = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yhbb = Yhbb.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhbb;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhbb> getByYh_id(int yh_id,Connection conn){
		return getByYh_id(yh_id,conn,Yhbb.TABLENAME);
	}
	
	public static List<Yhbb> getByYh_id(int yh_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,道具id,数量 FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC";
		List<Yhbb> yhbbs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhbbs = Yhbb.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhbbs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhbb> getByPageYh_id(int yh_id,Connection conn,int page,int pageSize){
		return getByPageYh_id(yh_id,conn,Yhbb.TABLENAME,page,pageSize);
	}
	
	public static List<Yhbb> getByPageYh_id(int yh_id,Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,用户_id,道具id,数量 FROM " + tableName + " WHERE " + "yh_id = ? ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhbb> yhbbs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhbbs = Yhbb.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhbbs;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yhbb getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhbb getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhbb> getByYh_id(int yh_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhbb> getByYh_id(int yh_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhbb> getByPageYh_id(int yh_id,DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhbb> getByPageYh_id(int yh_id,DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yhbb> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yhbb> getAll(Connection conn){
		return getAll(conn,Yhbb.TABLENAME);
	}
	
	public static List<Yhbb> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhbb> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yhbb> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户_id,道具id,数量 FROM " + tableName + " ORDER BY id ASC";
		List<Yhbb> yhbbs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhbbs = Yhbb.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhbbs;
	}
	
	public static List<Yhbb> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yhbb> getAllPage(int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageSize);
	}
	
	public static List<Yhbb> getAllPage(Connection conn,int page,int pageSize){
		return getAllPage(conn,Yhbb.TABLENAME,page,pageSize);
	}
	
	public static List<Yhbb> getAllPage(DataSource ds,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhbb> getAllPage(String tableName,int page,int pageSize){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageSize);
	}
	
	public static List<Yhbb> getAllPage(Connection conn,String tableName,int page,int pageSize){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageSize);
		String sql = "SELECT id,用户_id,道具id,数量 FROM " + tableName + " ORDER BY id ASC LIMIT " + page + " , " +pageSize;
		List<Yhbb> yhbbs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhbbs = Yhbb.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhbbs;
	}
	
	public static List<Yhbb> getAllPage(DataSource ds,String tableName,int page,int pageSize){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageSize);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static boolean truncate(){
		Connection conn = SK_Config.getConnection();
		return truncate(conn);
	}
	
	public static boolean truncate(Connection conn){
		return truncate(conn,Yhbb.TABLENAME);
	}
	
	public static boolean truncate(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean truncate(String tableName){
		Connection conn = SK_Config.getConnection();
		return truncate(conn,tableName);
	}
	
	public static boolean truncate(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "TRUNCATE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean truncate(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return truncate(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	
	//Drop Table
	public static boolean drop(){
		Connection conn = SK_Config.getConnection();
		return drop(conn);
	}
	
	public static boolean drop(Connection conn){
		return drop(conn,Yhbb.TABLENAME);
	}
	
	public static boolean drop(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return drop(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean drop(String tableName){
		Connection conn = SK_Config.getConnection();
		return drop(conn,tableName);
	}
	
	public static boolean drop(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DROP TABLE " + tableName;
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean drop(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return drop(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	//create
	public static boolean createTable(){
		Connection conn = SK_Config.getConnection();
		return createTable(conn);
	}
	
	public static boolean createTable(Connection conn){
		return createTable(conn,Yhbb.TABLENAME);
	}
	
	public static boolean createTable(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean createTable(String tableName){
		Connection conn = SK_Config.getConnection();
		return createTable(conn,tableName);
	}
	
	public static boolean createTable(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		SK_Plus plus = SK_Plus.b("CREATE TABLE IF NOT EXISTS `", tableName,"` (");
		plus.a("  `id` INT(10) NOT NULL AUTO_INCREMENT,");	
		plus.a("  `用户_id` INT(10) NOT NULL,");	
		plus.a("  `道具id` INT(10) NOT NULL,");	
		plus.a("  `数量` VARCHAR(45) NOT NULL,");	
		plus.a("  PRIMARY KEY (`id`)");
		plus.a(") ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;");
		String sql = plus.e();
		try {
			run.update(conn, sql);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				DbUtils.close(conn);
			} catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean createTable(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return createTable(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}