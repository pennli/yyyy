package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import com.sandking.db.cache.ZadlCache;
import java.io.ByteArrayOutputStream;
import com.sandking.db.cache.YhzaCache;

/**
 * 用户障碍
 */
public class Yhza {

	public static final String TABLENAME = "用户障碍";
	public static final String CLASSNAME = "Yhza"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 障碍id */
	private int zaid;
	
	/** x */
	private int x;
	
	/** y */
	private int y;
	
	/** 用户_id */
	private int yh_id;
	
	
	public Yhza() {
		super();
	}
	
	public Yhza(int id, int zaid, int x, int y, int yh_id) {
		super();
		this.id = id;
		this.zaid = zaid;
		this.x = x;
		this.y = y;
		this.yh_id = yh_id;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getZaid() {
		return zaid;
	}
	
	public void setZaid(int zaid) {
		this.zaid = zaid;
		addUpdateColumn("障碍id",zaid);
	}
	
	public void changeZaidWith(int zaid){
		this.zaid += zaid;
		setZaid(this.zaid);
	}
	
	public void changeZaidWithMin(int zaid,int min){
		this.zaid += zaid;
		this.zaid = this.zaid < min ? min : this.zaid;
		setZaid(this.zaid);
	}
	
	public void changeZaidWithMax(int zaid,int max){
		this.zaid += zaid;
		this.zaid = this.zaid > max ? max : this.zaid;
		setZaid(this.zaid);
	}
	
	public void changeZaidWithMaxMin(int zaid,int max,int min){
		this.zaid += zaid;
		this.zaid = this.zaid < min ? min : this.zaid;
		this.zaid = this.zaid > max ? max : this.zaid;
		setZaid(this.zaid);
	}	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
		addUpdateColumn("x",x);
	}
	
	public void changeXWith(int x){
		this.x += x;
		setX(this.x);
	}
	
	public void changeXWithMin(int x,int min){
		this.x += x;
		this.x = this.x < min ? min : this.x;
		setX(this.x);
	}
	
	public void changeXWithMax(int x,int max){
		this.x += x;
		this.x = this.x > max ? max : this.x;
		setX(this.x);
	}
	
	public void changeXWithMaxMin(int x,int max,int min){
		this.x += x;
		this.x = this.x < min ? min : this.x;
		this.x = this.x > max ? max : this.x;
		setX(this.x);
	}	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
		addUpdateColumn("y",y);
	}
	
	public void changeYWith(int y){
		this.y += y;
		setY(this.y);
	}
	
	public void changeYWithMin(int y,int min){
		this.y += y;
		this.y = this.y < min ? min : this.y;
		setY(this.y);
	}
	
	public void changeYWithMax(int y,int max){
		this.y += y;
		this.y = this.y > max ? max : this.y;
		setY(this.y);
	}
	
	public void changeYWithMaxMin(int y,int max,int min){
		this.y += y;
		this.y = this.y < min ? min : this.y;
		this.y = this.y > max ? max : this.y;
		setY(this.y);
	}	
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
		addUpdateColumn("用户_id",yh_id);
	}
	
	public void changeYh_idWith(int yh_id){
		this.yh_id += yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMin(int yh_id,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMax(int yh_id,int max){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMaxMin(int yh_id,int max,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}	
	
	//id
	public List<Zadl> getZadlsFkYhza_id(){
		return ZadlCache.getByYhza_id(id);
	}
	
	//用户_id
	public Yh getYhPkYh_id(){
		return YhCache.getById(yh_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("zaid", this.zaid);
        result.put("x", this.x);
        result.put("y", this.y);
        result.put("yh_id", this.yh_id);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("障碍id", this.zaid);
        result.put("x", this.x);
        result.put("y", this.y);
        result.put("用户_id", this.yh_id);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.zaid);
		    SK_OutputStream.writeInt(out,this.x);
		    SK_OutputStream.writeInt(out,this.y);
		    SK_OutputStream.writeInt(out,this.yh_id);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Yhza createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yhza yhza = new Yhza();
		    yhza.id = SK_InputStream.readInt(in,null);
		    yhza.zaid = SK_InputStream.readInt(in,null);
		    yhza.x = SK_InputStream.readInt(in,null);
		    yhza.y = SK_InputStream.readInt(in,null);
		    yhza.yh_id = SK_InputStream.readInt(in,null);
		    return yhza;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Yhza createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Yhza yhza = Yhza.createForMap(map);
		    return yhza;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhza> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yhza> yhzas = new ArrayList<Yhza>();
		for (Map<String, Object> map : list) {
			yhzas.add(createForColumnNameMap(map));
		}
		return yhzas;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhza createForColumnNameMap(Map<String, Object> map){
    	Yhza obj = new Yhza();
	    obj.id = SK_Map.getInt("id", map);
	    obj.zaid = SK_Map.getInt("障碍id", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhza> createForList(List<Map<String, Object>> list){
    	List<Yhza> yhzas = new ArrayList<Yhza>();
		for (Map<String, Object> map : list) {
			yhzas.add(createForColumnNameMap(map));
		}
		return yhzas;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhza createForMap(Map<String, Object> map){
    	Yhza obj = new Yhza();
	    obj.id = SK_Map.getInt("id", map);
	    obj.zaid = SK_Map.getInt("zaid", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("yh_id", map);
        return obj;
    }
    
    public static Yhza createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Yhza> createForJson(List<String> jsons){
    	List<Yhza> yhzas = new ArrayList<Yhza>();
    	for(String json : jsons){
    		yhzas.add(createForJson(json));
    	}
    	return yhzas;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Yhza insert(){
    	return YhzaCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yhza update(){
    	return YhzaCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhzaCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yhza insertAndFlush(){
    	return YhzaCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yhza updateAndFlush(){
    	return YhzaCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhzaCache.deleteAndFlush(this);
    }
}