package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import com.sandking.db.cache.ZadlCache;
import java.io.ByteArrayOutputStream;
import com.sandking.db.cache.YhzaCache;

/**
 * 障碍队列
 */
public class Zadl {

	public static final String TABLENAME = "障碍队列";
	public static final String CLASSNAME = "Zadl"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 用户障碍_id */
	private int yhza_id;
	
	
	public Zadl() {
		super();
	}
	
	public Zadl(int id, int yhza_id) {
		super();
		this.id = id;
		this.yhza_id = yhza_id;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getYhza_id() {
		return yhza_id;
	}
	
	public void setYhza_id(int yhza_id) {
		this.yhza_id = yhza_id;
		addUpdateColumn("用户障碍_id",yhza_id);
	}
	
	public void changeYhza_idWith(int yhza_id){
		this.yhza_id += yhza_id;
		setYhza_id(this.yhza_id);
	}
	
	public void changeYhza_idWithMin(int yhza_id,int min){
		this.yhza_id += yhza_id;
		this.yhza_id = this.yhza_id < min ? min : this.yhza_id;
		setYhza_id(this.yhza_id);
	}
	
	public void changeYhza_idWithMax(int yhza_id,int max){
		this.yhza_id += yhza_id;
		this.yhza_id = this.yhza_id > max ? max : this.yhza_id;
		setYhza_id(this.yhza_id);
	}
	
	public void changeYhza_idWithMaxMin(int yhza_id,int max,int min){
		this.yhza_id += yhza_id;
		this.yhza_id = this.yhza_id < min ? min : this.yhza_id;
		this.yhza_id = this.yhza_id > max ? max : this.yhza_id;
		setYhza_id(this.yhza_id);
	}	
	
	//用户障碍_id
	public Yhza getYhzaPkYhza_id(){
		return YhzaCache.getById(yhza_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("yhza_id", this.yhza_id);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("用户障碍_id", this.yhza_id);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.yhza_id);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Zadl createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Zadl zadl = new Zadl();
		    zadl.id = SK_InputStream.readInt(in,null);
		    zadl.yhza_id = SK_InputStream.readInt(in,null);
		    return zadl;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Zadl createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Zadl zadl = Zadl.createForMap(map);
		    return zadl;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Zadl> createForColumnNameList(List<Map<String, Object>> list){
    	List<Zadl> zadls = new ArrayList<Zadl>();
		for (Map<String, Object> map : list) {
			zadls.add(createForColumnNameMap(map));
		}
		return zadls;
    }
    
    /**
     * 根据map创建对象
     */
    public static Zadl createForColumnNameMap(Map<String, Object> map){
    	Zadl obj = new Zadl();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhza_id = SK_Map.getInt("用户障碍_id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Zadl> createForList(List<Map<String, Object>> list){
    	List<Zadl> zadls = new ArrayList<Zadl>();
		for (Map<String, Object> map : list) {
			zadls.add(createForColumnNameMap(map));
		}
		return zadls;
    }
    
    /**
     * 根据map创建对象
     */
    public static Zadl createForMap(Map<String, Object> map){
    	Zadl obj = new Zadl();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhza_id = SK_Map.getInt("yhza_id", map);
        return obj;
    }
    
    public static Zadl createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Zadl> createForJson(List<String> jsons){
    	List<Zadl> zadls = new ArrayList<Zadl>();
    	for(String json : jsons){
    		zadls.add(createForJson(json));
    	}
    	return zadls;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Zadl insert(){
    	return ZadlCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Zadl update(){
    	return ZadlCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return ZadlCache.delete(this);
    }
    /** 即时插入数据库 */
    public Zadl insertAndFlush(){
    	return ZadlCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Zadl updateAndFlush(){
    	return ZadlCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return ZadlCache.deleteAndFlush(this);
    }
}