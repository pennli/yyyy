package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.BydlCache;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import com.sandking.db.cache.YhjzCache;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 兵营队列
 */
public class Bydl {

	public static final String TABLENAME = "兵营队列";
	public static final String CLASSNAME = "Bydl"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 用户建筑_id */
	private int yhjz_id;
	
	
	public Bydl() {
		super();
	}
	
	public Bydl(int id, int yhjz_id) {
		super();
		this.id = id;
		this.yhjz_id = yhjz_id;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getYhjz_id() {
		return yhjz_id;
	}
	
	public void setYhjz_id(int yhjz_id) {
		this.yhjz_id = yhjz_id;
		addUpdateColumn("用户建筑_id",yhjz_id);
	}
	
	public void changeYhjz_idWith(int yhjz_id){
		this.yhjz_id += yhjz_id;
		setYhjz_id(this.yhjz_id);
	}
	
	public void changeYhjz_idWithMin(int yhjz_id,int min){
		this.yhjz_id += yhjz_id;
		this.yhjz_id = this.yhjz_id < min ? min : this.yhjz_id;
		setYhjz_id(this.yhjz_id);
	}
	
	public void changeYhjz_idWithMax(int yhjz_id,int max){
		this.yhjz_id += yhjz_id;
		this.yhjz_id = this.yhjz_id > max ? max : this.yhjz_id;
		setYhjz_id(this.yhjz_id);
	}
	
	public void changeYhjz_idWithMaxMin(int yhjz_id,int max,int min){
		this.yhjz_id += yhjz_id;
		this.yhjz_id = this.yhjz_id < min ? min : this.yhjz_id;
		this.yhjz_id = this.yhjz_id > max ? max : this.yhjz_id;
		setYhjz_id(this.yhjz_id);
	}	
	
	//用户建筑_id
	public Yhjz getYhjzPkYhjz_id(){
		return YhjzCache.getById(yhjz_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("yhjz_id", this.yhjz_id);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("用户建筑_id", this.yhjz_id);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.yhjz_id);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Bydl createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Bydl bydl = new Bydl();
		    bydl.id = SK_InputStream.readInt(in,null);
		    bydl.yhjz_id = SK_InputStream.readInt(in,null);
		    return bydl;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Bydl createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Bydl bydl = Bydl.createForMap(map);
		    return bydl;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Bydl> createForColumnNameList(List<Map<String, Object>> list){
    	List<Bydl> bydls = new ArrayList<Bydl>();
		for (Map<String, Object> map : list) {
			bydls.add(createForColumnNameMap(map));
		}
		return bydls;
    }
    
    /**
     * 根据map创建对象
     */
    public static Bydl createForColumnNameMap(Map<String, Object> map){
    	Bydl obj = new Bydl();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhjz_id = SK_Map.getInt("用户建筑_id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Bydl> createForList(List<Map<String, Object>> list){
    	List<Bydl> bydls = new ArrayList<Bydl>();
		for (Map<String, Object> map : list) {
			bydls.add(createForColumnNameMap(map));
		}
		return bydls;
    }
    
    /**
     * 根据map创建对象
     */
    public static Bydl createForMap(Map<String, Object> map){
    	Bydl obj = new Bydl();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhjz_id = SK_Map.getInt("yhjz_id", map);
        return obj;
    }
    
    public static Bydl createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Bydl> createForJson(List<String> jsons){
    	List<Bydl> bydls = new ArrayList<Bydl>();
    	for(String json : jsons){
    		bydls.add(createForJson(json));
    	}
    	return bydls;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Bydl insert(){
    	return BydlCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Bydl update(){
    	return BydlCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return BydlCache.delete(this);
    }
    /** 即时插入数据库 */
    public Bydl insertAndFlush(){
    	return BydlCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Bydl updateAndFlush(){
    	return BydlCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return BydlCache.deleteAndFlush(this);
    }
}