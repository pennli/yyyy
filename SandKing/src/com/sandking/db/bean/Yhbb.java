package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YhbbCache;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 用户背包
 */
public class Yhbb {

	public static final String TABLENAME = "用户背包";
	public static final String CLASSNAME = "Yhbb"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 用户_id */
	private int yh_id;
	
	/** 道具id */
	private int djid;
	
	/** 数量 */
	private String sl;
	
	
	public Yhbb() {
		super();
	}
	
	public Yhbb(int id, int yh_id, int djid, String sl) {
		super();
		this.id = id;
		this.yh_id = yh_id;
		this.djid = djid;
		this.sl = sl;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
		addUpdateColumn("用户_id",yh_id);
	}
	
	public void changeYh_idWith(int yh_id){
		this.yh_id += yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMin(int yh_id,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMax(int yh_id,int max){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMaxMin(int yh_id,int max,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}	
	public int getDjid() {
		return djid;
	}
	
	public void setDjid(int djid) {
		this.djid = djid;
		addUpdateColumn("道具id",djid);
	}
	
	public void changeDjidWith(int djid){
		this.djid += djid;
		setDjid(this.djid);
	}
	
	public void changeDjidWithMin(int djid,int min){
		this.djid += djid;
		this.djid = this.djid < min ? min : this.djid;
		setDjid(this.djid);
	}
	
	public void changeDjidWithMax(int djid,int max){
		this.djid += djid;
		this.djid = this.djid > max ? max : this.djid;
		setDjid(this.djid);
	}
	
	public void changeDjidWithMaxMin(int djid,int max,int min){
		this.djid += djid;
		this.djid = this.djid < min ? min : this.djid;
		this.djid = this.djid > max ? max : this.djid;
		setDjid(this.djid);
	}	
	public String getSl() {
		return sl;
	}
	
	public void setSl(String sl) {
		this.sl = sl;
		addUpdateColumn("数量",sl);
	}
	
	
	//用户_id
	public Yh getYhPkYh_id(){
		return YhCache.getById(yh_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("yh_id", this.yh_id);
        result.put("djid", this.djid);
        result.put("sl", this.sl);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("用户_id", this.yh_id);
        result.put("道具id", this.djid);
        result.put("数量", this.sl);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.yh_id);
		    SK_OutputStream.writeInt(out,this.djid);
		    SK_OutputStream.writeString(out,this.sl);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Yhbb createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yhbb yhbb = new Yhbb();
		    yhbb.id = SK_InputStream.readInt(in,null);
		    yhbb.yh_id = SK_InputStream.readInt(in,null);
		    yhbb.djid = SK_InputStream.readInt(in,null);
		    yhbb.sl = SK_InputStream.readString(in,null);
		    return yhbb;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Yhbb createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Yhbb yhbb = Yhbb.createForMap(map);
		    return yhbb;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhbb> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yhbb> yhbbs = new ArrayList<Yhbb>();
		for (Map<String, Object> map : list) {
			yhbbs.add(createForColumnNameMap(map));
		}
		return yhbbs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhbb createForColumnNameMap(Map<String, Object> map){
    	Yhbb obj = new Yhbb();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
	    obj.djid = SK_Map.getInt("道具id", map);
	    obj.sl = SK_Map.getString("数量", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhbb> createForList(List<Map<String, Object>> list){
    	List<Yhbb> yhbbs = new ArrayList<Yhbb>();
		for (Map<String, Object> map : list) {
			yhbbs.add(createForColumnNameMap(map));
		}
		return yhbbs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhbb createForMap(Map<String, Object> map){
    	Yhbb obj = new Yhbb();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yh_id = SK_Map.getInt("yh_id", map);
	    obj.djid = SK_Map.getInt("djid", map);
	    obj.sl = SK_Map.getString("sl", map);
        return obj;
    }
    
    public static Yhbb createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Yhbb> createForJson(List<String> jsons){
    	List<Yhbb> yhbbs = new ArrayList<Yhbb>();
    	for(String json : jsons){
    		yhbbs.add(createForJson(json));
    	}
    	return yhbbs;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Yhbb insert(){
    	return YhbbCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yhbb update(){
    	return YhbbCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhbbCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yhbb insertAndFlush(){
    	return YhbbCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yhbb updateAndFlush(){
    	return YhbbCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhbbCache.deleteAndFlush(this);
    }
}