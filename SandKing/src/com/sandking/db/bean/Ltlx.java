package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.LtCache;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.LtlxCache;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 聊天类型
 */
public class Ltlx {

	public static final String TABLENAME = "聊天类型";
	public static final String CLASSNAME = "Ltlx"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	
	public Ltlx() {
		super();
	}
	
	public Ltlx(int id, String mc) {
		super();
		this.id = id;
		this.mc = mc;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
		addUpdateColumn("名称",mc);
	}
	
	
	//id
	public List<Lt> getLtsFkLtlx_id(){
		return LtCache.getByLtlx_id(id);
	}
	
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("mc", this.mc);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("名称", this.mc);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.mc);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Ltlx createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Ltlx ltlx = new Ltlx();
		    ltlx.id = SK_InputStream.readInt(in,null);
		    ltlx.mc = SK_InputStream.readString(in,null);
		    return ltlx;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Ltlx createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Ltlx ltlx = Ltlx.createForMap(map);
		    return ltlx;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Ltlx> createForColumnNameList(List<Map<String, Object>> list){
    	List<Ltlx> ltlxs = new ArrayList<Ltlx>();
		for (Map<String, Object> map : list) {
			ltlxs.add(createForColumnNameMap(map));
		}
		return ltlxs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Ltlx createForColumnNameMap(Map<String, Object> map){
    	Ltlx obj = new Ltlx();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Ltlx> createForList(List<Map<String, Object>> list){
    	List<Ltlx> ltlxs = new ArrayList<Ltlx>();
		for (Map<String, Object> map : list) {
			ltlxs.add(createForColumnNameMap(map));
		}
		return ltlxs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Ltlx createForMap(Map<String, Object> map){
    	Ltlx obj = new Ltlx();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("mc", map);
        return obj;
    }
    
    public static Ltlx createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Ltlx> createForJson(List<String> jsons){
    	List<Ltlx> ltlxs = new ArrayList<Ltlx>();
    	for(String json : jsons){
    		ltlxs.add(createForJson(json));
    	}
    	return ltlxs;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Ltlx insert(){
    	return LtlxCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Ltlx update(){
    	return LtlxCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return LtlxCache.delete(this);
    }
    /** 即时插入数据库 */
    public Ltlx insertAndFlush(){
    	return LtlxCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Ltlx updateAndFlush(){
    	return LtlxCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return LtlxCache.deleteAndFlush(this);
    }
}