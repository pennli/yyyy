package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.JzdlCache;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import com.sandking.db.cache.YhjzCache;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 建筑队列
 */
public class Jzdl {

	public static final String TABLENAME = "建筑队列";
	public static final String CLASSNAME = "Jzdl"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 用户建筑_id */
	private int yhjz_id;
	
	
	public Jzdl() {
		super();
	}
	
	public Jzdl(int id, int yhjz_id) {
		super();
		this.id = id;
		this.yhjz_id = yhjz_id;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getYhjz_id() {
		return yhjz_id;
	}
	
	public void setYhjz_id(int yhjz_id) {
		this.yhjz_id = yhjz_id;
		addUpdateColumn("用户建筑_id",yhjz_id);
	}
	
	public void changeYhjz_idWith(int yhjz_id){
		this.yhjz_id += yhjz_id;
		setYhjz_id(this.yhjz_id);
	}
	
	public void changeYhjz_idWithMin(int yhjz_id,int min){
		this.yhjz_id += yhjz_id;
		this.yhjz_id = this.yhjz_id < min ? min : this.yhjz_id;
		setYhjz_id(this.yhjz_id);
	}
	
	public void changeYhjz_idWithMax(int yhjz_id,int max){
		this.yhjz_id += yhjz_id;
		this.yhjz_id = this.yhjz_id > max ? max : this.yhjz_id;
		setYhjz_id(this.yhjz_id);
	}
	
	public void changeYhjz_idWithMaxMin(int yhjz_id,int max,int min){
		this.yhjz_id += yhjz_id;
		this.yhjz_id = this.yhjz_id < min ? min : this.yhjz_id;
		this.yhjz_id = this.yhjz_id > max ? max : this.yhjz_id;
		setYhjz_id(this.yhjz_id);
	}	
	
	//用户建筑_id
	public Yhjz getYhjzPkYhjz_id(){
		return YhjzCache.getById(yhjz_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("yhjz_id", this.yhjz_id);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("用户建筑_id", this.yhjz_id);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.yhjz_id);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Jzdl createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Jzdl jzdl = new Jzdl();
		    jzdl.id = SK_InputStream.readInt(in,null);
		    jzdl.yhjz_id = SK_InputStream.readInt(in,null);
		    return jzdl;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Jzdl createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Jzdl jzdl = Jzdl.createForMap(map);
		    return jzdl;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Jzdl> createForColumnNameList(List<Map<String, Object>> list){
    	List<Jzdl> jzdls = new ArrayList<Jzdl>();
		for (Map<String, Object> map : list) {
			jzdls.add(createForColumnNameMap(map));
		}
		return jzdls;
    }
    
    /**
     * 根据map创建对象
     */
    public static Jzdl createForColumnNameMap(Map<String, Object> map){
    	Jzdl obj = new Jzdl();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhjz_id = SK_Map.getInt("用户建筑_id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Jzdl> createForList(List<Map<String, Object>> list){
    	List<Jzdl> jzdls = new ArrayList<Jzdl>();
		for (Map<String, Object> map : list) {
			jzdls.add(createForColumnNameMap(map));
		}
		return jzdls;
    }
    
    /**
     * 根据map创建对象
     */
    public static Jzdl createForMap(Map<String, Object> map){
    	Jzdl obj = new Jzdl();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhjz_id = SK_Map.getInt("yhjz_id", map);
        return obj;
    }
    
    public static Jzdl createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Jzdl> createForJson(List<String> jsons){
    	List<Jzdl> jzdls = new ArrayList<Jzdl>();
    	for(String json : jsons){
    		jzdls.add(createForJson(json));
    	}
    	return jzdls;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Jzdl insert(){
    	return JzdlCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Jzdl update(){
    	return JzdlCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return JzdlCache.delete(this);
    }
    /** 即时插入数据库 */
    public Jzdl insertAndFlush(){
    	return JzdlCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Jzdl updateAndFlush(){
    	return JzdlCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return JzdlCache.deleteAndFlush(this);
    }
}