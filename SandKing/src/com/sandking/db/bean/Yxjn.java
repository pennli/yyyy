package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.alibaba.fastjson.JSON;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YxjnCache;
import com.sandking.db.cache.YhyxCache;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 英雄技能
 */
public class Yxjn {

	public static final String TABLENAME = "英雄技能";
	public static final String CLASSNAME = "Yxjn"; 
	//Cache 中的索引
	
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 用户英雄_id */
	private int yhyx_id;
	
	/** 技能id */
	private int jnid;
	
	/** 技能lvl */
	private int jnlvl;
	
	
	public Yxjn() {
		super();
	}
	
	public Yxjn(int id, int yhyx_id, int jnid, int jnlvl) {
		super();
		this.id = id;
		this.yhyx_id = yhyx_id;
		this.jnid = jnid;
		this.jnlvl = jnlvl;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getYhyx_id() {
		return yhyx_id;
	}
	
	public void setYhyx_id(int yhyx_id) {
		this.yhyx_id = yhyx_id;
		addUpdateColumn("用户英雄_id",yhyx_id);
	}
	
	public void changeYhyx_idWith(int yhyx_id){
		this.yhyx_id += yhyx_id;
		setYhyx_id(this.yhyx_id);
	}
	
	public void changeYhyx_idWithMin(int yhyx_id,int min){
		this.yhyx_id += yhyx_id;
		this.yhyx_id = this.yhyx_id < min ? min : this.yhyx_id;
		setYhyx_id(this.yhyx_id);
	}
	
	public void changeYhyx_idWithMax(int yhyx_id,int max){
		this.yhyx_id += yhyx_id;
		this.yhyx_id = this.yhyx_id > max ? max : this.yhyx_id;
		setYhyx_id(this.yhyx_id);
	}
	
	public void changeYhyx_idWithMaxMin(int yhyx_id,int max,int min){
		this.yhyx_id += yhyx_id;
		this.yhyx_id = this.yhyx_id < min ? min : this.yhyx_id;
		this.yhyx_id = this.yhyx_id > max ? max : this.yhyx_id;
		setYhyx_id(this.yhyx_id);
	}	
	public int getJnid() {
		return jnid;
	}
	
	public void setJnid(int jnid) {
		this.jnid = jnid;
		addUpdateColumn("技能id",jnid);
	}
	
	public void changeJnidWith(int jnid){
		this.jnid += jnid;
		setJnid(this.jnid);
	}
	
	public void changeJnidWithMin(int jnid,int min){
		this.jnid += jnid;
		this.jnid = this.jnid < min ? min : this.jnid;
		setJnid(this.jnid);
	}
	
	public void changeJnidWithMax(int jnid,int max){
		this.jnid += jnid;
		this.jnid = this.jnid > max ? max : this.jnid;
		setJnid(this.jnid);
	}
	
	public void changeJnidWithMaxMin(int jnid,int max,int min){
		this.jnid += jnid;
		this.jnid = this.jnid < min ? min : this.jnid;
		this.jnid = this.jnid > max ? max : this.jnid;
		setJnid(this.jnid);
	}	
	public int getJnlvl() {
		return jnlvl;
	}
	
	public void setJnlvl(int jnlvl) {
		this.jnlvl = jnlvl;
		addUpdateColumn("技能lvl",jnlvl);
	}
	
	public void changeJnlvlWith(int jnlvl){
		this.jnlvl += jnlvl;
		setJnlvl(this.jnlvl);
	}
	
	public void changeJnlvlWithMin(int jnlvl,int min){
		this.jnlvl += jnlvl;
		this.jnlvl = this.jnlvl < min ? min : this.jnlvl;
		setJnlvl(this.jnlvl);
	}
	
	public void changeJnlvlWithMax(int jnlvl,int max){
		this.jnlvl += jnlvl;
		this.jnlvl = this.jnlvl > max ? max : this.jnlvl;
		setJnlvl(this.jnlvl);
	}
	
	public void changeJnlvlWithMaxMin(int jnlvl,int max,int min){
		this.jnlvl += jnlvl;
		this.jnlvl = this.jnlvl < min ? min : this.jnlvl;
		this.jnlvl = this.jnlvl > max ? max : this.jnlvl;
		setJnlvl(this.jnlvl);
	}	
	
	//用户英雄_id
	public Yhyx getYhyxPkYhyx_id(){
		return YhyxCache.getById(yhyx_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("yhyx_id", this.yhyx_id);
        result.put("jnid", this.jnid);
        result.put("jnlvl", this.jnlvl);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public String toJson(){
    	return JSON.toJSONString(toMap());
    }
    
    /**
     * 数据库源字段Map
     */
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("用户英雄_id", this.yhyx_id);
        result.put("技能id", this.jnid);
        result.put("技能lvl", this.jnlvl);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.yhyx_id);
		    SK_OutputStream.writeInt(out,this.jnid);
		    SK_OutputStream.writeInt(out,this.jnlvl);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
    public byte[] toSKBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeMap(out,toMap());
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public static Yxjn createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yxjn yxjn = new Yxjn();
		    yxjn.id = SK_InputStream.readInt(in,null);
		    yxjn.yhyx_id = SK_InputStream.readInt(in,null);
		    yxjn.jnid = SK_InputStream.readInt(in,null);
		    yxjn.jnlvl = SK_InputStream.readInt(in,null);
		    return yxjn;
    	}catch (Exception e) {
            throw e;
        }
     }
     
     public static Yxjn createForSKBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
     		@SuppressWarnings("unchecked")
     		Map<String,Object> map = SK_InputStream.readMap(in,null);
	     	Yxjn yxjn = Yxjn.createForMap(map);
		    return yxjn;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yxjn> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yxjn> yxjns = new ArrayList<Yxjn>();
		for (Map<String, Object> map : list) {
			yxjns.add(createForColumnNameMap(map));
		}
		return yxjns;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yxjn createForColumnNameMap(Map<String, Object> map){
    	Yxjn obj = new Yxjn();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhyx_id = SK_Map.getInt("用户英雄_id", map);
	    obj.jnid = SK_Map.getInt("技能id", map);
	    obj.jnlvl = SK_Map.getInt("技能lvl", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yxjn> createForList(List<Map<String, Object>> list){
    	List<Yxjn> yxjns = new ArrayList<Yxjn>();
		for (Map<String, Object> map : list) {
			yxjns.add(createForColumnNameMap(map));
		}
		return yxjns;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yxjn createForMap(Map<String, Object> map){
    	Yxjn obj = new Yxjn();
	    obj.id = SK_Map.getInt("id", map);
	    obj.yhyx_id = SK_Map.getInt("yhyx_id", map);
	    obj.jnid = SK_Map.getInt("jnid", map);
	    obj.jnlvl = SK_Map.getInt("jnlvl", map);
        return obj;
    }
    
    public static Yxjn createForJson(String json){
    	Map<String, Object> map = JSON.parseObject(json);
    	return createForMap(map);
    }
    
    public static List<Yxjn> createForJson(List<String> jsons){
    	List<Yxjn> yxjns = new ArrayList<Yxjn>();
    	for(String json : jsons){
    		yxjns.add(createForJson(json));
    	}
    	return yxjns;
    }
    
    /** 级联删除(延迟入库) */
    public boolean deleteAndSon(){
    	return false;
    }
    
    /** 级联删除(及时入库) */
    public boolean deleteAndSonAndFlush(){
    	return false;
    }
    
    /** 延迟插入数据库 */
    public Yxjn insert(){
    	return YxjnCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yxjn update(){
    	return YxjnCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YxjnCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yxjn insertAndFlush(){
    	return YxjnCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yxjn updateAndFlush(){
    	return YxjnCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YxjnCache.deleteAndFlush(this);
    }
}